# Create PSR Angular app 

## Prerequisites
### for MAC OS 
   1. Open the Terminal app and type brew install node.
   2. Sit back and wait. Homebrew downloads some files and installs them. And that’s it.  
     To make sure you have Node and NPM installed, run two simple commands to see what version of each is installed:  
     $ **node -v**  
     This should print the version number:- **v8.9.4**  
     $ **npm -v**  
     This should print the version number:- **6.1.0**
#### How to Update Node and NPM
   1. Make sure Homebrew has the latest version of the Node package. In Terminal type  
     $ **brew update**  
     $ **brew upgrade node**
### for Windows  
  1. Open the official page for Node.js downloads and download Node.js for Windows by clicking the "Windows Installer" option  
  2. Run the downloaded Node.js .msi Installer - including accepting the license, selecting the destination, and authenticating for the install.
  3. This requires Administrator privileges, and you may need to authenticate. To ensure Node.js has been installed, Open your terminal and type this command
  4. **c:\>node -v**  
  5. Update your version of npm with   
  6. **c:\>npm install npm --global** 
  This requires Administrator privileges, and you may need to authenticate  
  Congratulations - you've now got Node.js installed, and are ready to start building!  
  7. **c:\>npm -v**  
  8. c:\>npm install -g @angular/cli  
  9. c:\>npm install -g @angular-devkit/core

## Getting Started for installation PSR

### Installation
```sh  
cd direcotrypath  
git clone https://vikashpkl@bitbucket.org/igtcounsulting/psr.git  
cd psr
rm -rf node_modules
rm -rf package-lock.json
npm install --save @angular/cli
npm install --save @angular-devkit/core
npm install --save path-platform
npm install
npm start
```
Then open [http://localhost:4200/](http://localhost:4200/) to see your app.<br>
When you’re ready to deploy to production, create a minified bundle with **`npm run build`**.  

After than goopen fileto **src/app/app-routing.module.ts**
and some changes done automatically compiled and run

It will create a directory called `psr` inside the current folder.<br>
Inside that directory, it will generate the initial project structure and install the transitive dependencies:

```
psr
├── e2e
├── node_modules
├── src
├── angular.json
├── package.json
├── README.md
├── tsconfig.json
├── tslint.json
```

Subfolders are not shown in order to keep it short.<br>
Once the installation is done, you can run some commands inside the project folder:

### `npm start`
Builds the app in development mode and starts a web server. <br />
Open [http://localhost:4200](http://localhost:4200) to view it in the browser. <br />
The page will reload if you make edits.

**Note: If you’re getting any issue related this project installlation or run contact to administrator.** 

### `npm run build`
Builds the app and stores it in the `dist/` directory.