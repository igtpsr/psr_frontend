// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://base.url',
  apiUrl: 'http://localhost:8080/psr/rest',
  //imagePath:'http://localhost:9080/Images/',

  //excelPath:'http://localhost/phpproject/dataexport.php' 
  //imagePath:'http://psrwin.infogloballink.com:8182/Images/',
  imagePath:'http://localhost:9080/Images/',
  smartTag:'./assets/images/tip.gif',
  
  // Local Excel PHP Path
 // excelPath:'http://tssdev02web01:9999/phpproject/dataexport.php'
 //excelPath: 'http://localhost:9080/phpproject/test.php',

 // Local Excel Download Path
 //excelDownloadPath: 'http://localhost:9080/phpproject/psr.xlsx'
  //apiUrl: 'http://psrwin.infogloballink.com:8181/psr/rest',
   // MGF Excel PHP Path
 excelPath: 'http://localhost:9080/phpproject/test.php',

 // MGF Excel Download Path
 excelDownloadPath: 'http://localhost:9080/phpproject/psr.xlsx'  ,
  //  psr ui relative path
 psrui: "/psr",
   //  psr ui login relative path
 login: "/login",
 error: "/error"
 
  
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
