export const environment = {
  production: true,
  apiUrl: '/psr-combined/rest',
  imagePath: '/Images/',
  excelPath: '/phpproject/test.php',
  excelDownloadPath: '/phpproject/psr.xlsx',
  psrui: '/psrtest/psr',
  login: '/psrtest/login',
  error: '/psrtest/error',
  smartTag:'./assets/images/tip.gif',
};
