import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { ToastrManager } from 'ng6-toastr-notifications';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json',
    'authentication': localStorage.getItem('authentication'),
    'Cache-Control':  'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
    'Pragma': 'no-cache',
    'Expires': '0' })
};

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    doRecalc(orderIds): any {
        orderIds = JSON.stringify(orderIds);
        let loginUserId: string= localStorage.getItem('loginUserId');
        return this.http.post(environment.apiUrl + '/recalculate?loginUserId=' + loginUserId, orderIds, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    constructor(private http: HttpClient, public toastr: ToastrManager, private router: Router) { }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
            this.toastr.errorToastr('An error occurred:' + error.error.message, 'Oops!');
            if (error.status == 417) {
                this.toastr.errorToastr('An error occurred:' + error.error.message, 'Oops!');
            }
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,4

            console.error(
                'Backend returned code ' + error.status +
                ', Error was in back end: ', error.error);
            if (error.status == 417) {
                this.toastr.errorToastr('An error occurred:' + error.error.message, 'Oops!');
            }

        }
        // if( error.status === 0 || error.status === 503){
        //     // this.toastr.errorToastr('Your Backend session expired please Re-Login and continue .....!','ERROR!');
        //     // console.log("here in session time out");
        //     localStorage.clear();
        //     //this.router.navigate(['http://localhost:4203/login']);
        //     location.reload();

        //     return throwError('backend not available');
        //   }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };

    private extractData(res: Response) {
        //console.log(res);
        let body = res;
        return body || {};
    }

    updateOrderAndEventsData(data, loginUserId, roleName): Observable<any> {
        data = JSON.stringify(data);
        return this.http.post(environment.apiUrl + '/updateOrders?loginUserId=' + loginUserId + '&roleName=' + roleName + '&searchTs=' + localStorage.getItem('searchTs'), data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    updateModelData(data, modelName): Observable<any> {
        data = JSON.stringify(data);
        console.log('DATA------->' + data)
        return this.http.post(environment.apiUrl + '/addEventModelsToCriteria/' + modelName, data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    };

    addeventmodel(data, modelName): Observable<any> {
        data = JSON.stringify(data);
        return this.http.post(environment.apiUrl + '/addeventmodel?modelName='+modelName+ '&loginUserId=' + localStorage.getItem('loginUserId'), data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    };

    addCustView(queryname, selectedCheckbox, checkboxValue, columnReorder): Observable<any> {
        return this.http.post(environment.apiUrl + '/addCustomizedView?queryName=' + queryname + '&checkboxValue=' + checkboxValue
            +'&selectedCheckbox='+selectedCheckbox+ '&loginUserId=' + localStorage.getItem('loginUserId'),columnReorder, httpOptions)
            .pipe(
                catchError(this.handleError)
            );

    }

    deleteCustView(availableviewid): Observable<any> {

        return this.http.delete(environment.apiUrl + '/deleteAvailableView?queryId=' + availableviewid + '&authentication=' + localStorage.getItem('authentication'), httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    updateCustView(availableid, selectedcheckbox, checkboxValue, columnReorder): Observable<any> {
        //console.log(columnReorder);
        return this.http.post(environment.apiUrl + '/updateAvailableView?queryId=' + availableid + '&checkBoxValue=' + checkboxValue + '&selectedcheckbox=' + selectedcheckbox + '&loginUserId=' + localStorage.getItem('loginUserId'), columnReorder, httpOptions).pipe(
            catchError(this.handleError)
        );
    }

    getCustomisedViews(user: string): Observable<any> {
        return this.http.get(environment.apiUrl + '/customisedViews?loginUserId=' + user, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    getSelectedColumn(value: string): Observable<any> {
        return this.http.get(environment.apiUrl + '/selectedColumnView?queryName=' + value
            + '&loginUserId=' + localStorage.getItem('loginUserId'), httpOptions)
            .pipe(
                map(this.extractData),
                catchError(this.handleError));
    }

    getOrderColumn(value: string): Observable<any> {
        return this.http.get(environment.apiUrl + '/reOrderColumn?queryName=' + value
            + '&loginUserId=' + localStorage.getItem('loginUserId'), httpOptions)
            .pipe(
                map(this.extractData),
                catchError(this.handleError));
    }

    getEventMileStones(user: string): Observable<any> {
        return this.http.get(environment.apiUrl + '/eventMileStone?brUserId=' + localStorage.getItem('brUserId') + '&loginUserId=' + user + '&roleName=' + localStorage.getItem('roleName'), httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    getChangeTracking(user: string, orderIds, since, searchKeyword, fromDate, toDate): Observable<any> {
        return this.http.get(environment.apiUrl + '/changeTrackDetails?loginUserId=' + user + '&roleName=' + localStorage.getItem('roleName')
            + '&psrOrderIds=' + orderIds+ '&from=' + since+ '&searchKeyword=' + searchKeyword+ '&fromDate=' + fromDate+ '&toDate=' + toDate, httpOptions)
            .pipe(map(this.extractData),
                catchError(this.handleError));
    }

   
   
    getAllGriddataforExcel(user: string, searchFiledValues, queryId, milstoneList, holdCheckedOrderId): Observable<HttpResponse<Blob>> {

        return this.http.get<Blob>(environment.apiUrl + '/downloadExcel?' + searchFiledValues + '&queryId=' + queryId+'&milstoneList='+milstoneList+'&checkedOrderId='+holdCheckedOrderId, {
            observe: 'response',
            responseType: 'blob' as 'json',
       //     headers: { 'authentication': localStorage.getItem('authentication') }
            headers : {
                'Cache-Control':  'no-cache, no-store, must-revalidate, post-check=0, pre-check=0',
                'Pragma': 'no-cache',
                'Expires': '0',
                'authentication': localStorage.getItem('authentication')
            }
        });
    }


    getEventDescriptions(user: string): Observable<any> {
        return this.http.get(environment.apiUrl + '/eventDescriptions?brUserId=' + localStorage.getItem('brUserId') + '&loginUserId=' + user + '&roleName=' + localStorage.getItem('roleName'), httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    deleteEventModel(modelName: string) {
        return this.http.delete(environment.apiUrl + '/deleteEventModel?modelName=' + modelName, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    generateXML(data): Observable<any> {
        data = JSON.stringify(data);
        return this.http.post(environment.apiUrl + '/posttobamboorose', data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    downloadReport(data): void {
        data = JSON.stringify(data);
        const httpOptions_new = {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' })
        };
        let url = environment.excelPath;
        this.http.post(url, data, httpOptions_new).pipe(map(res => {
            return {
                filename: res
            };
        })).subscribe(res => {
            // alert('dff');
            //  console.log('start download:', res);
            //  url = window.URL.createObjectURL(res.data);
            let a = document.createElement('a');
            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            // a.href = res.filename;
            a.href = environment.excelDownloadPath;
            //    a.download = res.filename;
            a.click();
            // window.URL.revokeObjectURL(url);
            // a.remove(); // remove the element
        }, error => {
            console.log('download error:', JSON.stringify(error));
        }, () => {
            console.log('Completed file download.');
        });;
    }

    updateDefaultView(queryName: string, existingQryNm: string): any {
        return this.http.put(environment.apiUrl + '/updateDefaultView?queryName=' + queryName + '&loginUserId=' + localStorage.getItem('loginUserId') + '&existingQryNm=' + existingQryNm, '', httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    addRoleName(data) {
        data = JSON.stringify(data);
        return this.http.post<any>(environment.apiUrl + '/saveRoleProfile?loginUserId=' + localStorage.getItem('loginUserId'), data, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));

    }

    getRoleDetails(roleId: string): Observable<any> {
        return this.http.get(environment.apiUrl + '/getRoleProfileDetails?roleId=' + roleId, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    };

    updateRoleName(roleId: string, data) {
        data = JSON.stringify(data);
        return this.http.put<any>(environment.apiUrl + '/updateRoleProfile?roleId=' + roleId + '&loginUserId=' + localStorage.getItem('loginUserId'), data, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));

    }

    deleteRole(roleId: string) {
        return this.http.put(environment.apiUrl + '/deleteRoleProfile?roleId=' + roleId + '&authentication=' + localStorage.getItem('authentication'), httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    deleteUser(userId: string) {
        return this.http.delete(environment.apiUrl + '/deleteUserProfile?userId=' + userId, httpOptions).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }

    getRoles(disableInput) {
        return this.http.get(environment.apiUrl + '/getRoleProfile?roleName=' + '&disable=' + disableInput + '&authentication=' + localStorage.getItem('authentication')
        ).pipe(catchError(this.handleError));
    }

    createUpdateUser(data, loginuserid) {
        data = JSON.stringify(data);
        return this.http.post(environment.apiUrl + '/saveUserPrfile?loginUserId=' + loginuserid + '&authentication=' + localStorage.getItem('authentication'), data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    getUserDetails(userId) {
        return this.http.get(environment.apiUrl + '/getUserDetails?userId=' + userId + '&authentication=' + localStorage.getItem('authentication'))
            .pipe(
                catchError(this.handleError)
            );
    }
    getBrUserDetails(brUserId) {
        return this.http.get(environment.apiUrl + '/getBrUserProfile?brUserId=' + brUserId + '&authentication=' + localStorage.getItem('authentication'), httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }
    updateUserProfile(data, username, loginuserid) {
        data = JSON.stringify(data);
        return this.http.put(environment.apiUrl + '/updateUserProfile?loginUserId=' + loginuserid + '&UserId=' + username + '&authentication=' + localStorage.getItem('authentication'), data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    updateAcknowledgement(psrOrderIds) {
        let data: any = psrOrderIds.toString();
        return this.http.put(environment.apiUrl + '/updateAcknowledge?authentication=' + localStorage.getItem('authentication'), data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }
    //added by Thulasidhar
    getReportUrls() {
        return this.http.get(environment.apiUrl + '/getReportUrls', httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    sendBrowserCloseStatus() {
        let xhr = new XMLHttpRequest();
        let params = localStorage.getItem('authentication');
        xhr.open("POST", environment.apiUrl + '/browserClosed');
        return xhr.send(params);
    }

    checkTokenAvailable() {
        return this.http.post(environment.apiUrl + '/isTokenAvailable', { tokenId: localStorage.getItem('authentication') }, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    uploadFile(file: File) {
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        formdata.append('loginUserId', localStorage.getItem('loginUserId'));
        formdata.append('roleName', localStorage.getItem('roleName'));
        return this.http.post(environment.apiUrl + '/importExcel?authentication=' + localStorage.getItem('authentication'), formdata).pipe(
            catchError(this.handleError)
        );
    }

    getFileTrackList(from, fromDate, toDate): Observable<any> {
        return this.http.get(environment.apiUrl + '/fileTrack?loginUserId=' + localStorage.getItem('loginUserId') + '&roleName=' + localStorage.getItem('roleName')+'&from='+from+'&fromDate='+fromDate+'&toDate='+toDate, httpOptions)
            .pipe(map(this.extractData),
                catchError(this.handleError));
    }
	
	    saveSearchData(data, loginuserid): Observable<any> {
        data = JSON.stringify(data);
        return this.http.post(environment.apiUrl + '/saveSearch?loginUserId=' + loginuserid, data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    savedSearchData(loginuserid): Observable<any> {
        return this.http.get(environment.apiUrl + '/saveSearch?loginUserId=' + loginuserid , httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }
    
    /**new approach for importing excel bia json */
    updateOrdersByImportExcel(loginUserId, roleName,sheetName,orderAvailableFields, data): Observable<any> {
        data = JSON.stringify(data);
        return this.http.post(environment.apiUrl + '/updateOrdersByImportExcel?loginUserId=' + loginUserId + '&roleName=' + roleName+ '&sheetName='+sheetName+ '&orderAvailableFields='+orderAvailableFields, data, httpOptions)
            .pipe(
                catchError(this.handleError)
            );
    }

    /**Recalc Model Name */
    recalcModel(modelName:string)    {
        let user = localStorage.getItem('loginUserId')+'_MS'
        return this.http.post(environment.apiUrl + '/recalcModel?modelName=' + modelName+ '&loginUserId='+user,'',httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }
    
    getEventCodeList():Observable<any> {
        return this.http.get( environment.apiUrl + '/eventMaster?eventcode='+ '&eventcodeselect="like"'+ '&desc=' + '&descSelect= "like"' + '&eventcategory=' + '&eventcategorySelect= "like"' + '&authentication=' + localStorage.getItem('authentication'), httpOptions)
                .pipe(map(this.extractData),
                    catchError(this.handleError));
    }

    getModelNames():Observable<any> {
        return this.http.get( environment.apiUrl + '/getModelNames?modelName=&authentication=' + localStorage.getItem('authentication'),httpOptions)
        .pipe(map(this.extractData),
            catchError(this.handleError));
    }

    disableModel(disableStatus:boolean,modelName:string )    {
        let user = localStorage.getItem('loginUserId');
        return this.http.post(environment.apiUrl + '/disableModel?modelName=' + modelName+'&status='+disableStatus+ '&loginUserId='+user,'',httpOptions)
        .pipe(
            catchError(this.handleError)
        );
    }

    getSmartTagRecords(requestNo:string,  style:string, colorWashName:string, profomaPo:string):Observable<any> {
        return this.http.get( environment.apiUrl + '/getSmartTagRecords?requestNo='+requestNo+'&style='+style+'&colorWashName='+colorWashName+'&profomaPo='+profomaPo+'&authentication=' + localStorage.getItem('authentication'),httpOptions)
        .pipe(map(this.extractData),
            catchError(this.handleError));
    }

}





