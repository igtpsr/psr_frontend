import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { MatExpansionModule } from '@angular/material/expansion';
import { SelectModule } from 'ng2-select';
import { MyDatePickerModule } from 'mydatepicker';
import { DialogComponent } from './dialog/dialog.component';



@NgModule({
  imports: [
    CommonModule,
    MatExpansionModule,
    SelectModule,
    MyDatePickerModule
  ],
  declarations: [
    jqxGridComponent,
    DialogComponent
  ],
  exports:[
    jqxGridComponent,
    MatExpansionModule,
    SelectModule,
    MyDatePickerModule,
    DialogComponent
  ]
})
export class SharedModule { }
