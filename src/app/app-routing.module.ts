import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { StaticpsrComponent } from './_psr/staticpsr/staticpsr.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PsrReportsComponent } from './_layout/psr-reports/psr-reports.component';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'psr',
    component: StaticpsrComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'psr-report/:reportId',
    component: PsrReportsComponent,
    canActivate: [AuthGuard]
  },
 
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'error',
    component: ErrorComponent
  },
  {
    path:'user', loadChildren:'./_userHome/user.module#UserModule'
  },
  {
    path:'event', loadChildren:'./_eventHome/event.module#EventModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
