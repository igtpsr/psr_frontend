import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {jqxGridComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { EventsService } from '../eventcode-model-add/events.service';
import { ApiService } from '../../api.service';
import { Observable, of, throwError } from 'rxjs';
import { NgProgress } from '@ngx-progressbar/core';
import { Globals } from '../../Globals';
import { environment } from '../../../environments/environment';
import { ToastrManager } from 'ng6-toastr-notifications';
export interface PeriodicElement {
  modelname: string;
  select: number;
}

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css'],
  providers: [Globals]
})
export class EventsComponent implements OnInit {
  @ViewChild('eventsGrid') public eventsGrid: jqxGridComponent;
  addNewModel = true;
  model: any = {};
  ELEMENT_DATA: PeriodicElement[];
  modelOperators = [
    { value: 'Like', label: 'Like' },
    { value: 'Not like', label: 'Not like' },
    { value: 'Equal to', label: 'Equal to' },
    { value: 'Not equal to', label: 'Not equal to' },
    { value: 'In the list', label: 'In the list' },
    { value: 'Not in the list', label: 'Not in the list' },
    { value: 'Starts with', label: 'Starts with' },
    { value: 'Ends with', label: 'Ends with' },
    { value: 'Equal to null', label: 'Equal to null' },
    { value: 'Is not null', label: 'Is not null' },
  ];

  passValue: string;
  displayedColumns: string[] = [ 'modelname'];
  source: any =
    {
      datatype: 'json',
      datafields:
        [
          { name: 'modelname', type: 'string' },
          { name: 'disable', type: 'boolean' },
        ],
      unboundmode: true,
      id: 'modelname',
      // url: 'http://psrwin.infogloballink.com:8080/psr/rest/getModelNames?modelName=',
      url: environment.apiUrl + '/getModelNames?modelName=&authentication=' + localStorage.getItem('authentication'),
      //  url: '../assets/model5.txt',
      autorowheight: true,
      autoheight: false,
      autoshowloadelement: false
    };

  dataAdapter: any = new jqx.dataAdapter(this.source);
  cellbeginedit = (row, datafield, columntype, value): boolean => {
    if(value === '' || value === undefined || value === null)
      return false;
  };
  cellsrenderer =  (row, columnfield, value, defaulthtml, columnproperties): string => {
    return '<div class="event-code-alignment">' + value + '</div>';
      
  };
  copycellsrenderer =  (row, columnfield, value, defaulthtml, columnproperties): string => {
    return '<div style="text-align:center;font-size:15px;line-height:25px;"><a href="event/event-model-details/copy/' + this.eventsGrid.getcellvalue(row,'modelname') + '"><i class="fa fa-copy"></i></a></div>';
 };
 updatecellsrenderer =  (row, columnfield, value, defaulthtml, columnproperties): string => {
    return '<div style="text-align:center;font-size:15px;line-height:25px;"><a href="event/event-model-details/update/' + this.eventsGrid.getcellvalue(row,'modelname') + '"><i class="fa fa-edit"></i></a></div>';
  };

  cellclass = (row, columnfield, value, defaulthtml, columnproperties): string => {
    let disableEvent = this.eventsGrid.getrowdata(row);
    if(disableEvent.disable === true) {
        return 'editable-cell-gray';
    }
    return '';
  }

  disableModelEdit = (row, datafield, columntype, value): boolean => {
    let confirmUpdate:boolean = confirm("Are you sure you want to disable Model?");
    if(!confirmUpdate) {
        return false;
    }
  }; 
  
  cellvaluechanging = (row, datafield, columntype, oldvalue, newvalue) => {

    let disableEvent = this.eventsGrid.getrowdata(row);
    this.disableModel(newvalue, disableEvent.modelname);

  }

  columns = [
    { text: 'Model Name',align: "center", datafield: 'modelname', width: 'auto', cellsrenderer: this.cellsrenderer, cellbeginedit: this.cellbeginedit, editable: false, cellclassname: this.cellclass},
    { text: 'Copy Model',align: "center",  width: '20%', cellsrenderer: this.copycellsrenderer, cellbeginedit: this.cellbeginedit, editable: false, cellclassname: this.cellclass},
    { text: 'Update Model',align: "center",  width: '20%', cellsrenderer: this.updatecellsrenderer, cellbeginedit: this.cellbeginedit, editable: false, cellclassname: this.cellclass},
    { text: 'Disable Model', datafield: 'disable', width: 100, type: 'boolean', editable: true, columntype: 'checkbox', cellsformat: '', cellclassname: this.cellclass, cellbeginedit:this.disableModelEdit, cellvaluechanging:this.cellvaluechanging},

  ];

  constructor(private  api: ApiService, private eventsService: EventsService,  public progress: NgProgress, public _global: Globals, public toastr: ToastrManager) {

  }
  ngOnInit() {
   //console.log(' jtsw events component .....');
    let roleName = localStorage.getItem('roleName')
    if(roleName !== '' && roleName !== null && roleName !== 'null'){
      if(roleName !== 'PSR_ADMIN' && roleName !== 'PTL_ADMIN'){
          this.toastr.warningToastr("You are not authorized to access this page","Oops!",{dismiss:'click',showCloseButton:true });

          window.location.href = '/welcome';
      }
    }
  };
  getModelNames() {
    this.progress.start();
    this._global.elementDisabled = true;
    const eventModelSearch = <HTMLInputElement>document.getElementById('modeloperator');
      this.source.url = environment.apiUrl +'/getModelNames?modelName=' + this.model.modelName
      + '&eventModelSearch=' + eventModelSearch.value + '&authentication=' + localStorage.getItem('authentication');
      // this.source.url ='http://psrwin.infogloballink.com:8080/psr/rest/getModelNames?modelName='+this.model.modelName,
      console.log(this.source.url);
      this.eventsGrid.updatebounddata('cells');
      this.eventsGrid.refreshdata();
    this.progress.complete();
    this._global.elementDisabled = false;
  }

  handlekeyboardnavigation = (event): boolean => {
    const currentCell: any = this.eventsGrid.getselectedcell();
    if (currentCell !== null) {
        const key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
        if (currentCell.datafield === 'modelname') {
            if (key === 86) {
                this.toastr.warningToastr("ModelName is a non editable field","Alert!",{dismiss:'click',showCloseButton:true });
                event.preventDefault();
                return true;
            }
        }
      }
    }

  public onChange(event): void {
    const modeloperator = <HTMLInputElement>document.getElementById('modeloperator');
    const modelName = <HTMLInputElement>document.getElementById('modelName');
    
    if (modeloperator.value === 'Is not null' || modeloperator.value === 'Equal to null') {
      modelName.disabled = true;
      this.model.modelName = '';
      modelName.value = null;
        } else {
          modelName.disabled = false;
      }
  }
  resetForm(): void {
    var modelName = <HTMLInputElement>document.getElementById("modelName");
    const modeloperator = <HTMLInputElement>document.getElementById('modeloperator');
    if (modeloperator.value === 'Is not null' || modeloperator.value === 'Equal to null') {
      modelName.disabled = false;
    }
  }

  disableModel(status:boolean, modelName:string):void {
     
    this.progress.start();
      this.api.disableModel(status, modelName).subscribe(res=>{
        let data:any = res;
        this.progress.complete();
        if(data.type.toUpperCase() ==='SUCCESS') {
          this.toastr.successToastr("Disable Model Saved Successfully", 'Success!');
        } else {
          this.toastr.errorToastr(data.message,"Error!");
        }
      },error=>{
        this.toastr.errorToastr("Error occured while disabling the model, please contact the Administrator","Error!");
        this.progress.complete();
      })
  }
}
