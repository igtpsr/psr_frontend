import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {jqxGridComponent} from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { EventsService } from '../eventcode-model-add/events.service';
import { ApiService } from '../../api.service';
import { EventSearchForm } from './eventSearchForm';
import { NgProgress } from '@ngx-progressbar/core';
import { Globals } from '../../Globals';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
export interface PeriodicElement {
  eventcode: string;
  select: number;
}

@Component({
  selector: 'app-eventcode',
  templateUrl: './eventcode.component.html',
  styleUrls: ['./eventcode.component.css'],
  providers: [Globals]
})
export class EventcodeComponent implements OnInit {
  eventSearchModel = new EventSearchForm('', '');
  @ViewChild('eventsGrid') public eventsGrid: jqxGridComponent;
  addNewModel = true;
  model: any = {};
  ELEMENT_DATA: PeriodicElement[];
  modelOperators = [
    { value: 'Like', label: 'Like' },
    { value: 'Not like', label: 'Not like' },
    { value: 'Equal to', label: 'Equal to' },
    { value: 'Not equal to', label: 'Not equal to' },
    { value: 'In the list', label: 'In the list' },
    { value: 'Not in the list', label: 'Not in the list' },
    { value: 'Starts with', label: 'Starts with' },
    { value: 'Ends with', label: 'Ends with' },
    { value: 'Equal to null', label: 'Equal to null' },
    { value: 'Is not null', label: 'Is not null' },
  ];

  displayedColumns: string[] = ['eventcode', 'desc', 'eventcategory'];
  source: any =
      {
        datatype: 'json',
        datafields: [{ name: 'eventcode', type: 'string' },
              { name: 'desc', type: 'string' },
              { name: 'eventcategory', type: 'string' },
            ],
        unboundmode: true,
        id: 'eventcode',
       // url: 'http://psrwin.infogloballink.com:8080/psr/rest/eventMaster?eventCode=',
        url: environment.apiUrl +'/eventMaster?eventcode=' + '&authentication=' + localStorage.getItem('authentication'),
       // url: '../assets/model6.txt',
        autorowheight: true,
        autoheight: false,
        autoshowloadelement: false
      };

  dataAdapter: any = new jqx.dataAdapter(this.source);
    cellbeginedit = (row, datafield, columntype, value): boolean => {
        if(value === '' || value === undefined || value === null)
            return false;
    };
  cellsrenderer =  (row, columnfield, value, defaulthtml, columnproperties): string => {
    return '<div class="event-code-alignment"><a href="event/eventcode-model-add/' + value + '">' + value + '</a></div>';
};
  columns = [{ text: 'Event Code', datafield: 'eventcode', width: '150', cellsrenderer: this.cellsrenderer,editable: false},
        { text: 'Descprition', datafield: 'desc', width: '150',editable: false},
        { text: 'Event Category', datafield: 'eventcategory', width: '180',editable: false}];

  constructor(private  api: ApiService, private eventsService:EventsService, public progress: NgProgress, public _global: Globals, private router: Router , public toastr: ToastrManager ) {
    this.eventSearchModel.eventcodeselect = 'Like';
  }
  ngOnInit() {
    let roleName = localStorage.getItem('roleName')
      if(roleName !== '' && roleName !== null && roleName !== 'null'){
          if(roleName !== 'PSR_ADMIN' && roleName !== 'PTL_ADMIN'){
              this.toastr.warningToastr("You are not authorized to access this page","Oops!",{dismiss:'click',showCloseButton:true });
              window.location.href = '/welcome';
          }
      }
  };
   getEventCode(): void {
       this.progress.start();
       this._global.elementDisabled = true;
       const eventcode = <HTMLInputElement>document.getElementById('eventcode');
        const eventcodeselect = <HTMLInputElement>document.getElementById('modeloperator');

    this.source.url = environment.apiUrl + '/eventMaster?eventcode=' + eventcode.value
        + '&eventcodeselect=' + eventcodeselect.value + '&authentication=' + localStorage.getItem('authentication');
    this.eventsGrid.updatebounddata('cells');
    this.eventsGrid.refreshdata();
    this.progress.complete();
    this._global.elementDisabled = false;

};

public onChange(event): void {
  const modeloperator = <HTMLInputElement>document.getElementById('modeloperator');
  const eventcode = <HTMLInputElement>document.getElementById('eventcode');
  
  if (modeloperator.value === 'Is not null' || modeloperator.value === 'Equal to null') {
    eventcode.disabled = true;
    this.eventSearchModel.eventcode = '';
    eventcode.value = null;
      } else {
        eventcode.disabled = false;
    }
}

handlekeyboardnavigation = (event): boolean => {
  const currentCell: any = this.eventsGrid.getselectedcell();
  if (currentCell !== null) {
      const key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
      if (currentCell.datafield === 'eventcode') {
          if (key === 86) {
              this.toastr.warningToastr("EventCode is a non editable field","Alert!",{dismiss:'click',showCloseButton:true });
              event.preventDefault();
              return true;
          }
      }
      if (currentCell.datafield === 'desc') {
        if (key === 86) {
            this.toastr.warningToastr("Description is a non editable field","Alert!",{dismiss:'click',showCloseButton:true });
            event.preventDefault();
            return true;
        }
    }
    if (currentCell.datafield === 'eventcategory') {
      if (key === 86) {
          this.toastr.warningToastr("EventCategory is a non editable field","Alert!",{dismiss:'click',showCloseButton:true });
          event.preventDefault();
          return true;
      }
  }
    }
  }

resetForm(): void {
  var eventcode = <HTMLInputElement>document.getElementById("eventcode");
  const modeloperator = <HTMLInputElement>document.getElementById('modeloperator');
  if (modeloperator.value === 'Is not null' || modeloperator.value === 'Equal to null') {
    eventcode.disabled = false;
  }
}
}