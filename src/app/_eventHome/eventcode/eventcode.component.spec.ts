import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventcodeComponent } from './eventcode.component';

describe('EventcodeComponent', () => {
  let component: EventcodeComponent;
  let fixture: ComponentFixture<EventcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
