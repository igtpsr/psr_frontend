import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventRoutingModule } from './event-routing.module';
import { EventsComponent } from './events/events.component';
import { EventModelAddComponent } from './event-model-add/event-model-add.component';
import { EventModelDetailsComponent } from './event-model-details/event-model-details.component';
import { EventcodeComponent } from './eventcode/eventcode.component';
import { EventcodeModelAddComponent } from './eventcode-model-add/eventcode-model-add.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared.module';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';


@NgModule({
  imports: [
    CommonModule,
    EventRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatTableModule
  ],
  declarations: [
    EventsComponent,
    EventModelAddComponent,
    EventModelDetailsComponent,
    EventcodeComponent,
    EventcodeModelAddComponent,

  ]
})
export class EventModule { }
