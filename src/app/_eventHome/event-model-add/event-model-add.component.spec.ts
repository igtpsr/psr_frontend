import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventModelAddComponent } from './event-model-add.component';

describe('EventModelAddComponent', () => {
  let component: EventModelAddComponent;
  let fixture: ComponentFixture<EventModelAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventModelAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventModelAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
