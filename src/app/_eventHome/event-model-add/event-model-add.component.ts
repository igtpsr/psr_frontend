import { AfterViewInit, Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
import {isUndefined} from "util";
import { NgProgress } from '@ngx-progressbar/core';
import { Globals } from '../../Globals';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
    selector: 'app-event-model-add',
    templateUrl: './event-model-add.component.html',
    styleUrls: ['./event-model-add.component.css'],
    providers: [Globals]
})

export class EventModelAddComponent implements OnInit {
    @ViewChild('modelGrid') modelGrid: jqxGridComponent;
    eventsRes: any;
    addNewModel = true;
    model: any = {};
    nextUrl: string;
    counter = 1;
    modelNameRes: any;
    fieldlists = [{ label: 'VPO', fieldValue: 'vpo' },
    { label: 'VPO', fieldValue: 'vpo' }
    ];
  
    constructor(private api: ApiService, private router: Router, public progress: NgProgress, public _global: Globals, public toastr: ToastrManager) { }
    ngOnInit() {
        let roleName = localStorage.getItem('roleName')
        if(roleName !== '' && roleName !== null && roleName !== 'null'){
            if(roleName !== 'PSR_ADMIN' && roleName !== 'PTL_ADMIN'){
                this.toastr.warningToastr('You are not authorized to access this page',"Oops!",{dismiss:'click',showCloseButton:true });
                window.location.href = '/welcome';
            } else {
                this.model.is_data = false;
            } 
        }       
    }
    //   onSubmit() {
    //       this.saveMatchCriteria();
    //   }
    matchCase: any[] = [
        { value: 'prod_office', label: 'prod_office' },
        { value: 'brand', label: 'brand' },
        { value: 'dept', label: 'dept' },
        { value: 'product_type', label: 'product_type' },
        { value: 'category', label: 'category' },
        { value: 'init_flow', label: 'init_flow' }
    ];
    matchCaseSource: any =
    {
        datatype: 'array',
        datafields: [
            { name: 'label', type: 'string' },
            { name: 'value', type: 'string' },
        ],
         localdata: this.matchCase
    };
    matchCaseAdapter: any = new jqx.dataAdapter(this.matchCaseSource, { autoBind: true });
    source: any =
    {
        datatype: 'array',
        localdata:
        [
            { matchCase: 'Please Choose:' }
        ],
        datafields: [ 
            { name: 'matchCase', value: 'matchCase', values: { source: this.matchCaseAdapter.records, value: 'value', name: 'label' } },
            { name: 'fieldValue', type: 'fieldValue' }
        ]
    };
    dataAdapter: any = new jqx.dataAdapter(this.source);
    dropdownlist = (row, cellvalue, editor, celltext, cellwidth, cellheight): void => {
            editor.jqxDropDownList({ autoDropDownHeight: true, source: this.dataAdapter.records[row].matchCase, promptText: 'Please Choose:' });

        };
    columns: any[] =
    [
        {
            text: 'Match Case', datafield: 'matchCase', width:'250', displayfield: 'matchCase', columntype: 'dropdownlist',
            createeditor: (row: number, value: any, editor: any): void => {
                editor.jqxDropDownList({ width: '55%', height: 27, source: this.matchCaseAdapter, displayMember: 'label', valueMember: 'value' });
            },
            cellvaluechanging: function (row, column, columntype, oldvalue, newvalue){
                if (newvalue == "") return oldvalue;
            }
        },
        { text: 'Value', datafield: 'fieldValue', width: 'auto' }
    ];
    
    // source: any =
    //     {
    //         width: '100%',
    //         height: '100%',
    //         datatype: 'json',
    //         datafields:
    //             [
    //                 { name: 'matchCase', type: 'string' },
    //                 { name: 'fieldValue', type: 'string' },
    //             ],
    //         unboundmode: true,

    //         id: 'vpo',

    //        // url: '../assets/model.txt',

    //         // url:'http://localhost:8080/psr/rest/all',
    //         autorowheight: true,
    //         autoheight: false,
    //         scrollbarsize: 5,
    //         autoshowloadelement: false
    //     };

    // dataAdapter: any = new jqx.dataAdapter(this.source);
    // dropdownlist = (row, cellvalue, editor, celltext, cellwidth, cellheight): void => {
    //     editor.jqxDropDownList({ autoDropDownHeight: true, source: this.dataAdapter.records[row].matchCase, promptText: 'Please Choose:' });

    // };
    // cellsrenderer = function (row, columnfield, fieldValue, defaulthtml, columnproperties) {
    //     return '<span style="position: relative; width: 100%; margin: 4px; float: ' + columnproperties.cellsalign + ';">' + fieldValue +
    //         '<img src="assets/images/icon-down.png" style="position: absolute; right: 5px;" />' +
    //         '</span>';
    // };


    // columns: any[] =
    //     [
    //         {
    //             text: 'Field', datafield: 'matchCase', width: 'auto', columntype: 'dropdownlist', initeditor: this.dropdownlist, cellvaluechanging: function (row, column, columntype, oldvalue, newvalue) {
    //                 // return the old value, if the new value is empty.
    //                 if (newvalue == "") return oldvalue;
    //             },
    //             cellsrenderer: this.cellsrenderer
    //         },
    //         { text: 'Value', datafield: 'fieldValue', width: 'auto' }
    //     ];
        rendertoolbar = (toolbar: any): void => {
            let container = document.createElement('div');
            container.style.margin = '5px';
            let addNewRowBtn = document.createElement('div');
            addNewRowBtn.id = 'addNewRowBtn';
            addNewRowBtn.style.cssText = 'float: left';
            container.appendChild(addNewRowBtn);
            toolbar[0].appendChild(container);
            let addRowButton = jqwidgets.createInstance('#addNewRowBtn', 'jqxButton', { width: 105, value: 'Add New Row' });
            addRowButton.addEventHandler('click', () => {
                this.modelGrid.addrow(null, {
                    'fieldValue': '',
                    'matchCase': [
                        "Please Choose:"
                      ]
                });
            });
        };
    saveMatchCriteria(): void {
       
        if(this.model.modelName === undefined || this.model.modelName === null || this.model.modelName === '') {
             document.getElementById('invalid-feedback').style.display = 'block';
             return;
        }
        
      const rowValues = new Array();
      let row;
      for (let index = 0; index < this.modelGrid.getrows().length; index++) {
          row = this.modelGrid.getrowdata(index);
          if ( row.matchCase instanceof Array  || row.fieldValue === '') {
              continue;
          }
          
          if(row.matchCase === 'Please Choose:' || row.fieldValue === '' || row.fieldValue === null || row.fieldValue === undefined) {
           this.toastr.warningToastr("please choose matchcase and its value","Alert!",{dismiss:'click',showCloseButton:true });
           return;
          } else {
            rowValues.push({'matchCase': row.matchCase, 'fieldValue': row.fieldValue});
          }
      }
      if(row.matchCase === 'Please Choose:' || row.fieldValue === '' || row.fieldValue === null || row.fieldValue === undefined) {
        this.toastr.warningToastr("Please select atleast one match case and value","Alert!",{dismiss:'click',showCloseButton:true });
       } else {
        this.progress.start();
        this._global.elementDisabled = true;
      let t = this.api.updateModelData(rowValues, this.model.modelName);
   
     // t.subscribe(res => {});
    //   t.subscribe(res => { this.model.id = res.id});
    //     this.progress.complete();
    //     this._global.elementDisabled = false;
    //     this.router.navigateByUrl('/event/event-model-details/' + this.model.modelName );
    t.subscribe(res => {
        this.modelNameRes = res; 
        if(this.modelNameRes.type === 'error') {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.errorToastr(this.modelNameRes.message,"Error!",{dismiss:'click',showCloseButton:true });

        } else {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.successToastr(this.modelNameRes.message, 'Success!');
            this.router.navigateByUrl('event/event-model-details/update/' + this.model.modelName );
        }
        },error => {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.errorToastr("Error","Error!",{dismiss:'click',showCloseButton:true });
        });
  };
    }
    Cellunselect(event): void {
        const args = event.args;
        const dataField = event.args.datafield;
        if(event.args.datafield !== 'matchCase'){
        const rowBoundIndex = event.args.rowindex;
        const value = args.value;
        this.modelGrid.setcellvalue(rowBoundIndex, dataField, value);
        }

    }
}
