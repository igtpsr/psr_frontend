
export class matchCriteria  {

    prodOffice = [
        // 'ALL',
        // 'BANGLADESH',
        // 'FIRST CUSTOMER CLASS',
        // 'HONG KONG',
        // 'HSN-COLLEEN LOPEZ',
        // 'HSN-CURATIONS',
        // 'HSN-FITTYBRITTTTY',
        // 'HSN-G BY GIULIANA',
        // 'HSN-IMAN',
        // 'HSN-MODERN SOUL',
        // 'INTERNAL',
        // 'JAKARTA',
        // 'KOREA',
        // 'NEW SRI LANKA',
        // 'PERU',
        // 'QVC-ANYBODY',
        // 'QVC-CENTIGRADE',
        // 'QVC-DENIM & CO.',
        // 'QVC-DU JOUR',
        // 'QVC-G.I.L.I.',
        // 'QVC-HUNTER MCGRADY',
        // 'QVC-IML',
        // 'QVC-LAURIE FELT',
        // 'QVC-LISA RINNA',
        // 'QVC-PEACE LOVE WORLD',
        // 'QVC-RACHEL HOLLIS',
        // 'QVC-SOULGANI',
        // 'QVC-TRACY ANDERSON',
        // 'QVC-ZUDA',
        // 'SHANGHAI',
        // 'SRI LANKA',
        // 'TAIWAN',
        // 'UNITED STATES',
        // 'VIETNAM'
        'BANGLADESH',
        'SRI LANKA',
        'HONG KONG',
        'JAKARTA',
        'PERU',
        'NEW SRI LANKA',
        'KOREA',
        'VIETNAM',
        'SHANGHAI',
        'TAIWAN',
        'UNITED STATES'

    ];
    brand = [
        'AFK',
        'ANF',
        'BHT',
        'BLK',
        'BPL',
        'BPR',
        'CAB',
        'CAT',
        'CHI',
        'CJB',
        'EFI',
        'EXP',
        'GIL',
        'HOL',
        'HSN',
        'LBR',
        'LTD',
        'MAU',
        'NYC',
        'PKD',
        'PKS',
        'QVC',
        'SOM',
        'STF',
        'TLB',
        'TOR',
        'VSD',
        'VSS',
        'WAB'
       
    ];
    category = [
        'AC-ACC',
        'CA-NOC',
        'DE-DEN',
        'FW-FWR',
        'IN-INT',
        'KN-KNT',
        'SW-SWT',
        'WV-WOV'
    ];
    init_flow = [
        'INIT',
        'FLOW'
    ];  

}