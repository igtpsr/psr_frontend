import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventModelDetailsComponent } from './event-model-details.component';

describe('EventModelDetailsComponent', () => {
  let component: EventModelDetailsComponent;
  let fixture: ComponentFixture<EventModelDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventModelDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventModelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
