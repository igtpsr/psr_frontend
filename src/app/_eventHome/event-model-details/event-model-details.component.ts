import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { FormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxListBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxlistbox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { ApiService } from '../../api.service';
import { NgProgress } from '@ngx-progressbar/core';
import { Globals } from '../../Globals';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { matchCriteria } from '../matchCriteria';

declare var window: any;

export interface EventData {
    eventcode: string;
    desc: string;
    eventcategory: string;
  }
export class Label {
    constructor() {

    }
    eventLabel: any = [
        'Code',
        'Description',
    ]
}

@Component({
    selector: 'app-event-model-details',
    templateUrl: './event-model-details.component.html',
    styleUrls: ['./event-model-details.component.css'],
    providers: [Globals]
})
export class EventModelDetailsComponent implements OnInit, AfterViewInit {
    @ViewChild('modelGridList') modelGrid: jqxGridComponent;
    @ViewChild('modelGrid') modelGridAccordian: jqxGridComponent;
    showEventModelPopoup:boolean = false;
    rowIndex:number;
    eventCodeList:Array<{eventcode:string; desc:string; eventcategory:string}> = [];

    displayedColumns: string[] = [
        'eventcode',
        'desc',
        ];

    dataSource: MatTableDataSource<EventData>;
    showNodataLayout:boolean = false;
    @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
    Hideflag:boolean = true;
    eventLabel: Label = new Label();
    show: string = 'close';

    matchCriteria = new matchCriteria();
    holdEvents:any;

    cellsrenderer = (row, columnfield, value, defaulthtml, columnproperties): string => {
        return `<div style="font-size:18px; color:#d6261a; text-align:center; margin-top:2px; cursor:pointer;" ><div onclick=removeRow(${row})><i class="fa fa-trash"></i></div></div>`;
    };

    addSearchIcon = (row: number, columnfield: string, value: any, defaulthtml: string, columnproperties: any, rowdata: any): string => {

    let Status_value: string = value;
    if (Status_value == 'undefined' || Status_value == undefined) {
        Status_value = '';
    }
        return `<div style='padding:5px 10px; display:flex; justify-content:space-between;'><span style='display:inline-block;overflow: hidden; text-overflow: ellipsis; margin-right:5px;'>${Status_value}</span><span onclick = 'showEventModelPopup(${row})' style='cursor:pointer;'><i class='fa fa-search' style='font-size:15px;'></i></span></div>`;
    };

    t = 'function (row, column, columntype, oldvalue, newvalue) { if (newvalue == "") return oldvalue; }';

    cellclass = (row, columnfield, value, defaulthtml, columnproperties): string => {
        let disableEvent = this.modelGrid.getrowdata(row);
        if(disableEvent.disableEvent === true) {
            return 'editable-cell-gray';
        }
        return '';
    }
   
    datafields = new Array();
    modelName: string;
    copyModelName: string = '';
    modeldataAdapter: any;
    modelDataListAdapter: any;
    modelNameRes: any;
    source: any;
    modelsource: any;
    editedRowId = new Array();
    
    deletedRowId = new Array();
    type: string;
    modelNameList:any;

    columns:any[]= [
        { text: "Code", datafield: "eventCode", width: "80", type: "string", editable: false, cellsrenderer:this.addSearchIcon, cellclassname: this.cellclass},
        { text: "Description", datafield: "description", type: "string", width: "260", editable: false, cellclassname: this.cellclass },
        { text: "Trigger Event", datafield: "triggerEvent", type: "string", width: "100", editable: true, cellclassname: this.cellclass },
        { text: "Trigger Days", datafield: "triggerDays", type: "string", width: "100", editable: true, cellclassname: this.cellclass },
        { text: 'Disable Event', datafield: 'disableEvent', width: 100, type: 'boolean', editable: true, columntype: 'checkbox', cellsformat: '', cellclassname: this.cellclass},
        { text: "Remove Row", datafield:"removeRow", type:"string", width: "92",editable: false, cellsrenderer: this.cellsrenderer, cellclassname: this.cellclass }
    ];

    getDropdownGenderAge = (row, column, editor): void => {
        let list: any = ['prod_office','brand','category','init_flow'];
        for(let index = 0; index<this.modelGridAccordian.getrows().length; index++) {
            let indexOf:any = list.indexOf(this.modelGridAccordian.getcellvalue(index, "field"));
            if(indexOf>-1) {
                    list.splice(indexOf,1);
            }
        } 
       
        editor.jqxDropDownList({ autoDropDownHeight: true, source: list,  promptText: 'Please Choose:' });

    }

    
    fieldValueEditor= (row, cellvalue, editor, celltext, cellwidth, cellheight) =>{

         var country =  this.modelGridAccordian.getcellvalue(row, "field");
         
         var cities = new Array();
         switch (country) {
           case "prod_office":
             cities = this.matchCriteria.prodOffice;
             break;
           case "brand":
             cities = this.matchCriteria.brand;
             break;
           case "category":
             cities = this.matchCriteria.category
             break;
           case "init_flow":
              cities = this.matchCriteria.init_flow
              break;   
         };

         editor.jqxComboBox({ autoDropDownHeight: true, source: cities,  promptText: 'Please Choose:' });
       }

       changeCellValue = (row, datafield, columntype, oldvalue, newvalue) =>{
            this.modelGridAccordian.setcellvalue(row, "value", "")
      }


   
    modelcolumns: any[] =
        [
            { text: 'Match Case', datafield: 'field', columntype: 'dropdownlist',initeditor: this.getDropdownGenderAge, width: 390, cellvaluechanging: this.changeCellValue },
            { text: 'Field Value', datafield: 'value', width: 310, columntype: 'combobox', initeditor: this.fieldValueEditor }
        ];
        constructor(private api: ApiService, private route: ActivatedRoute, public progress: NgProgress, public _global: Globals, public toastr: ToastrManager, private router: Router) {
            for (let i = 0; i < this.columns.length; i++) {
                 this.datafields.push({ name: this.columns[i].datafield, type: this.columns[i].type });
        }
        this.route.params.subscribe(params => {
            this.modelName = params.modelName;
            this.type = params.type;
        });
        this.getEventData();
    }

    ngOnInit() {
    
        
        window.removeRowEvents = window.removeRowEvents || {};
        window.removeRowEvents.namespace = window.removeRowEvents.namespace || {};
        window.removeRowEvents.namespace.removeRowCallFunction = this.removeRowCallFunction.bind(this);
 
        /** Lookup Event Model */
        window.showEventModel = window.showEventModel || {};
        window.showEventModel.namespace = window.showEventModel.namespace || {};
        window.showEventModel.namespace.showEventModelFunction = this.showEventModelFunction.bind(this);
 
 
        let roleName = localStorage.getItem('roleName')
        if (roleName !== '' && roleName !== null && roleName !== 'null') {
            if (roleName !== 'PSR_ADMIN' && roleName !== 'PTL_ADMIN') {
                this.toastr.warningToastr('You are not authorized to access this page', "Oops!", { dismiss: 'click', showCloseButton: true });
                window.location.href = '/welcome';
            } else {
                // this.progress.start();
                // this._global.elementDisabled = true;
                this.source = {
                    width: '100%',
                    height: '100%',
                    datatype: 'json',
                    datafields: this.datafields,
                    unboundmode: true,
                    id: 'vpo',
                    //  url: '../assets/model2.txt'
                    url: environment.apiUrl + '/eventModelDetails?modelName=' +
                        this.modelName + '&authentication=' + localStorage.getItem('authentication')
                };
                //this.columns = this.cols;
                this.modeldataAdapter = new jqx.dataAdapter(this.source,{
                    downloadComplete:(data)=>{
                        let res:any = data;
                        this.holdEvents = res;
                    }
                });
               
                this.modelsource = {
                    datatype: 'json',
                    datafields: [
                        { name: 'field', type: 'string' },
                        { name: 'value', type: 'string' }],
                    unboundmode: true,
                    id: 'vpo',
                    // url: '../assets/model3.txt',
                    url: environment.apiUrl + '/eventMatchCriteria?modelName=' + this.modelName +
                        '&authentication=' + localStorage.getItem('authentication')
                };
                this.modelDataListAdapter = new jqx.dataAdapter(this.modelsource);
                // this.progress.complete();
                // this._global.elementDisabled = false;
            }
        }

        this.getModelNames();

      }
    
    getEventData(){
        this.api.getEventCodeList().subscribe(res=>{
            // this.eventCodeList = res;
             const list = res;
             this.dataSource = new MatTableDataSource(list);
             if(typeof this.dataSource.filteredData == 'object' && this.dataSource.filteredData.length == 0) {
                 this.showNodataLayout = true;
               } else if(this.dataSource.filteredData == null) {
                 this.showNodataLayout = true;
               }
             
               this.dataSource.paginator = this.paginator.toArray()[0];
               this.Hideflag = false;
         })
    }

    ngOnDestroy(): void {
        window.removeRowEvents.namespace.removeRowCallFunction = null;
    }

    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    
        if (this.dataSource.paginator) {
          this.dataSource.paginator.firstPage();
        }
      }

    ngAfterViewInit() {
        // ...
    }

    deleteeventmodel(): void {
        let res = confirm("Are you sure want to delete the data ?");
        if (res != undefined && res != null && res === true) {
        this.progress.start();
        this._global.elementDisabled = true;
      const t =  this.api.deleteEventModel(this.modelName);
      t.subscribe(res => {
        this.modelNameRes = res;
        if (this.modelNameRes.type === 'auto-attach-error') {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.errorToastr(this.modelNameRes.message,"Error",{dismiss:'click',showCloseButton:true });

        } else {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.successToastr(this.modelNameRes.message);
            this.router.navigateByUrl('/event/events');
        }
        }, error => {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.errorToastr("Error","Error",{dismiss:'click',showCloseButton:true });
        });
    }
}

isGridEdited: boolean = false;
checkGridChanges(): void {

    window.onhashchange = function () {
        if (this.isGridEdited) {
            let res = confirm("Grid has changes, Are you sure want to go back?");
            if (res != undefined && res != null && res === true) {

            }
        }
    }

}


check: boolean = false;

    addeventmodel(): void {
   
        if (this.type == 'copy' && this.copyModelName == '') {
            this.check = true;
            return;
        }
        
        if(this.type === 'copy') {
            this.saveCopiedModelName();
        } else {
            this.saveEventModel();
        }
    };

    rendertoolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let addNewRowBtn = document.createElement('div');
        addNewRowBtn.id = 'addNewRowBtn';
        addNewRowBtn.style.cssText = 'float: left';
        container.appendChild(addNewRowBtn);
        toolbar[0].appendChild(container);
        let addRowButton = jqwidgets.createInstance('#addNewRowBtn', 'jqxButton', { width: 105, value: 'Add New Row' });
        addRowButton.addEventHandler('click', () => {
            if(this._global.elementDisabled) return;
            this.modelGrid.addrow(null, {
                'eventCode': '',
                'description': '',
                'planDate': '',
                'actualDate': '',
                'revisedDate': '',
                'triggerEvent': '',
                'triggerDays': '',
                'disableEvent': false,
            });
            this.modelGrid.scrolloffset(this.modelGrid.height()+500,0);
        });
    };

       
   rendertoolbarMatchCriteria = (toolbar: any): void => {
    let container = document.createElement('div');
    container.style.margin = '5px';
    let addNewRowBtnMatch = document.createElement('div');
    addNewRowBtnMatch.id = 'addNewRowBtnMatch';
    addNewRowBtnMatch.style.cssText = 'float: left';
    container.appendChild(addNewRowBtnMatch);
    toolbar[0].appendChild(container);
    let addRowButton = jqwidgets.createInstance('#addNewRowBtnMatch', 'jqxButton', { width: 105, value: 'Add New Row' });
    addRowButton.addEventHandler('click', () => {
        this.modelGridAccordian.addrow(null, {
            'fieldValue': '',
            'matchCase':"Please Choose:"
        });
    });
};


    Cellunselect(event): void {
        this.isGridEdited = true;
        const args = event.args;
        const dataField = event.args.datafield;
        const rowBoundIndex = event.args.rowindex;
        const value = args.value;
        this.modelGrid.setcellvalue(rowBoundIndex, dataField, value);

        if(dataField ==='disableEvent' && value ===true) {
            this.modelGrid.setcellvalue(rowBoundIndex, 'triggerEvent','');
            this.modelGrid.setcellvalue(rowBoundIndex, 'triggerDays','');
        }

    }

    cellValueChanged(event):void {
        this.editedRowId.push(event.args.rowindex);
    }

    recalcModel():void {

        let confirmUpdate:boolean = confirm("Confirm Recalc Model");
        if(!confirmUpdate) {
            return;
        }
       
        this.progress.start();
        this._global.elementDisabled = true;

        this.api.recalcModel(this.modelName).subscribe(res=>{
            let psrRes:any = res;
            if (psrRes.type === 'error') {
                    this.toastr.errorToastr(psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                } else {
                    this.toastr.successToastr(psrRes.message, 'Success!');
            }
            this.progress.complete();
            this._global.elementDisabled = false;

        },error => {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.errorToastr("error","Error",{dismiss:'click',showCloseButton:true });
        })
    }
    

    goBack(): void {
        this.router.navigateByUrl('event/events');
    }

    removeRowCallFunction(row: any) {

        if(this.modelGrid.getcellvalue(row,'disableEvent')) {
            return;
        } 

        let confirmUpdate:boolean = confirm("Are you sure you want to delete?");
        if(!confirmUpdate) {
            return;
        }
        
        let id: any = this.modelGrid.getrowid(row);
        let rowData = this.modelGrid.getrowdata(row);
        let checkEventCode = this.holdEvents.filter(event=>event.eventCode==rowData.eventCode);
        if(this.type == 'update' && checkEventCode.length>0) {
            this.toastr.warningToastr("Cannot delete, It's already Participated in Auto Attachment","Warning");
            return;
        }
        this.modelGrid.deleterow(id);
        let eventDesc = rowData.description.length>0?rowData.description:"Blank Row";
        this.toastr.successToastr(eventDesc+" Successfully Deleted","Success");
       
        
        //this.rowValues.push({ 'modelName': this.type == 'copy' ? this.copyModelName : this.modelName, 'eventCode': rowData.eventCode, 'description': rowData.description, 'planDate': rowData.planDate, 'actualDate': rowData.actualDate, 'revisedDate': rowData.revisedDate, 'triggerEvent': row.disableEvent===true?'':row.triggerEvent, 'triggerDays':  row.disableEvent===true?'':row.triggerDays, 'disableEvent': row.disableEvent });
    }

    showEventModelFunction(row:number) {
        if(!this.modelGrid.getcellvalue(row,'disableEvent')) {
            this.showEventModelPopoup = true;
            this.rowIndex = row;
        }
        
    }

    saveCopiedModelName() {

        let modelNameArr:Array<{matchCase:string; fieldValue:string}> = [];
        let row:any;
        
        if(this.type=='copy' && this.modelNameList.filter(model=>model.modelname==this.copyModelName.trim()).length>0) {
           this.toastr.errorToastr("Duplicate Model Name Exists","Error!");
           return;
        }

        if(this.copyModelName.toLocaleUpperCase() ==='NEW') {
            this.toastr.errorToastr("Invalid Model Name","Error!");
            return;
        } 
        for(let index = 0; index<this.modelGridAccordian.getrows().length; index++) {
            row = this.modelGridAccordian.getrowdata(index);
            if((row.field != undefined && row.field != null && row.field != '') && (row.value != undefined && row.value != null && row.value != '')) {
                modelNameArr.push({'matchCase':row.field, 'fieldValue':row.value});
            }
        }
        
        this.progress.start();
        this._global.elementDisabled = true;

        let updatedModel = this.api.updateModelData(modelNameArr,this.copyModelName.trim());
        updatedModel.subscribe(res=>{
            this.saveEventModel();
        });
    }


    saveEventModel() {
        
        let row:any;
        let rowValues = new Array();
        for (let index = 0; index < this.modelGrid.getrows().length; index++) {
            row = this.modelGrid.getrowdata(index);
            if(!!row.eventCode) {
                rowValues.push({ 'modelName': this.type == 'copy' ? this.copyModelName : this.modelName, 'eventCode': row.eventCode, 'description': row.description, 'planDate': row.planDate, 'actualDate': row.actualDate, 'revisedDate': row.revisedDate, 'triggerEvent': row.disableEvent===true?'':row.triggerEvent, 'triggerDays':  row.disableEvent===true?'':row.triggerDays, 'disableEvent': row.disableEvent });
            }
        }
        
        let t = this.api.addeventmodel(rowValues, this.type === 'copy'?this.copyModelName:this.modelName);
        t.subscribe(res => {
            this.modelNameRes = res;
            if (this.modelNameRes.type === 'error') {
                this.toastr.errorToastr(this.modelNameRes.message, "Error", { dismiss: 'click', showCloseButton: true });
            } else {
                this.toastr.successToastr(this.modelNameRes.message, 'Success!');
                if(this.type == 'copy' && this.modelNameList.filter(model=>model.modelname==this.copyModelName.trim()).length==0) {
                    this.modelNameList.push({modelname:this.copyModelName});
                }
            }
            this.progress.complete();
            this._global.elementDisabled = false;
        }, error => {
            this.progress.complete();
            this._global.elementDisabled = false;
            this.toastr.errorToastr("error", "Error", { dismiss: 'click', showCloseButton: true });
        }

        );
        
    }

    onPopupSelect(row) {

        let flag:boolean = false;
        let rowData:any;
        for (let index = 0; index < this.modelGrid.getrows().length; index++) {
            rowData = this.modelGrid.getrowdata(index);
            if(rowData.eventCode ==row.eventcode) {
                flag = true;
                this.toastr.errorToastr("Duplicate event Code found, Please change selection", "Error!")
                return;
            }
        }
        this.modelGrid.setcellvalue(this.rowIndex,'eventCode',row.eventcode);
        this.modelGrid.setcellvalue(this.rowIndex,'description',row.desc);
        this.showEventModelPopoup = false;
    }

    //New Code
    onCellBeginEdit(event: any): void {
        let args = event.args;
        let columnDataField = args.datafield;
        let rowIndex = args.rowindex;
        let cellValue = args.value;
        if (this.modelGrid.getcellvalue(rowIndex, 'disableEvent')){
            let value = this.modelGrid.getcellvalue(rowIndex, 'eventCode')
            this.modelGrid.endcelledit(rowIndex,'eventCode', true);
            this.modelGrid.setcellvalue(rowIndex, 'eventCode', value);
        }
    }

    getModelNames() {
        this.api.getModelNames().subscribe(res=>{
            let data = res;
            this.modelNameList = data;
        })
    }
    
}
