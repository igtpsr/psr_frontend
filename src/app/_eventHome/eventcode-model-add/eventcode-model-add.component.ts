import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Eventform } from './eventform';
import { EventsService } from './events.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgProgress } from '@ngx-progressbar/core';
import { Globals } from '../../Globals';
import { ApiService } from '../../api.service';



@Component({
    selector: 'app-eventcode-model-add',
    templateUrl: './eventcode-model-add.component.html',
    styleUrls: ['./eventcode-model-add.component.css'],
    providers: [Globals]
})
export class EventcodeModelAddComponent implements OnInit {
    eventModel = new Eventform(null, '', '');
    eventsRes: any;
    eventcode:string;
    isReadOnly: boolean = false;
    buttonStatus:boolean= true;
    constructor(private api: ApiService,private eventService: EventsService,private route: ActivatedRoute, public toastr: ToastrManager, private router: Router, public progress: NgProgress, public _global: Globals) {
        this.route.params.subscribe(params => {
            this.eventcode = params.eventcode; 
        }); 
    }

    ngOnInit() {
        let roleName = localStorage.getItem('roleName')
        if(roleName !== '' && roleName !== null && roleName !== 'null'){
            if(roleName !== 'PSR_ADMIN' && roleName !== 'PTL_ADMIN'){
                this.toastr.warningToastr("You are not authorized to access this page","Oops!",{dismiss:'click',showCloseButton:true });
                window.location.href = '/welcome';
            } else {
            let t = this.eventService.getEventCodeDetails(this.eventcode);        
            t.subscribe(res => {  
                if(res[0] !== undefined) {
                this.eventModel = res[0];
                if(this.eventModel = res[0]){
                    this.isReadOnly = true;
                    this.buttonStatus = false;
                }
                    console.log('RESPONS-------->' + res);
                }
                });
            }            
        } 
    }

    saveEventCodes(): void {
        if (this.eventModel.eventcode === '' || this.eventModel.eventcode === undefined || this.eventModel.eventcode === null ) {
            document.getElementById('invalid-feedback').style.display ='block';
          return;
        } else if (this.eventModel.desc === '' || this.eventModel.desc === undefined || this.eventModel.desc === null ) {
            document.getElementById('invalid-feedback').style.display ='block';
          return;
        } else if (this.eventModel.eventcategory === '' || this.eventModel.eventcategory === undefined || this.eventModel.eventcategory === null ) {
            document.getElementById('invalid-feedback').style.display ='block';
          return;
        } else {
            this.progress.start();
            this._global.elementDisabled = true;
            let t = this.eventService.addEventModel(this.eventModel);
            t.subscribe(
                res => {  
                    this.eventsRes = res;
                    
                    if(this.eventsRes.type === 'error') {
                        this.progress.complete();
                        this._global.elementDisabled = false;
                        this.toastr.errorToastr(this.eventsRes.message,"Error!",{dismiss:'click',showCloseButton:true });

                    } else {
                        this.api.getEventDescriptions(localStorage.getItem('loginUserId')).subscribe((res)=>{
                           let eventDetails:any = res;
                           localStorage.setItem('milestone', eventDetails.toString());
                           this.progress.complete();
                           this._global.elementDisabled = false;
                           this.toastr.successToastr(this.eventsRes.message, 'Success!');
                           this.router.navigateByUrl('/event/eventcode');
                        });
                        
                       
                    }

                    },error => {
                        this.toastr.errorToastr("Error","Error!",{dismiss:'click',showCloseButton:true });
                        this.progress.complete();
                        this._global.elementDisabled = false;
                    }
            );
        }
    };
 rendertoolbar = (toolbar: any): void => {
        let container = document.createElement('div');
        container.style.margin = '5px';
        let addNewRowBtn = document.createElement('div');

        addNewRowBtn.id = 'addNewRowBtn';

        addNewRowBtn.style.cssText = 'float: left';
        // buttonContainer2.style.cssText = 'float: left; margin-left: 5px';

        container.appendChild(addNewRowBtn);

    let addRowButton = jqwidgets.createInstance('#addNewRowBtn', 'jqxButton', { width: 105, value: 'Add New Row' });
    addRowButton.addEventHandler('click', () => {

    });
    
  };
  deleteEventCode(): void{
    let res = confirm("Are you sure want to delete the data ?");
    if (res != undefined && res != null && res === true) {
    this.progress.start();
    this._global.elementDisabled = true;
    let t = this.eventService.deleteEventCode(this.eventcode);
    t.subscribe(
        res => {  
            this.eventsRes = res;
            this.progress.complete();
            this._global.elementDisabled = false;
            if(this.eventsRes.type === 'error') {
                this.toastr.errorToastr(this.eventsRes.message,"Error!",{dismiss:'click',showCloseButton:true });

            } else {
                this.toastr.successToastr(this.eventsRes.message, 'Success!');
                this.router.navigateByUrl('/event/eventcode');
            }

            },error => {
                this.toastr.errorToastr("Error","Error!",{dismiss:'click',showCloseButton:true });
                this.progress.complete();
                this._global.elementDisabled = false;
            }
    );
}
}
goBack(): void{
           this.router.navigateByUrl('/eventcode');
   }
}
