import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventcodeModelAddComponent } from './eventcode-model-add.component';

describe('EventcodeModelAddComponent', () => {
  let component: EventcodeModelAddComponent;
  let fixture: ComponentFixture<EventcodeModelAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventcodeModelAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventcodeModelAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
