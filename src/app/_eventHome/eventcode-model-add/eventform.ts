export class Eventform {
    constructor(
        public eventcode: string,
        public desc: string,
        public eventcategory: string
    ) { }
}
