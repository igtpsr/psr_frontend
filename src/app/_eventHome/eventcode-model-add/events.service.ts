
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Eventform } from './eventform';
import { ToastrManager } from 'ng6-toastr-notifications';
import { environment } from '../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json','authentication':localStorage.getItem('authentication')})
};
//const apiUrl = 'http://psrwin.infogloballink.com:8080/psr/rest';
//const apiUrl = 'http://localhost/phpproject';
//const apiUrl = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private _http: HttpClient, public toastr: ToastrManager) {}
  
  addEventModel(eventModelform: Eventform) {
      console.log(eventModelform);
    return this._http.post<any>(environment.apiUrl + '/addeventmaster', eventModelform, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));

  }


 
  getModelNames(modelName:string): Observable<any> {
    return this._http.get(environment.apiUrl + '/getModelNames?modelName='+modelName, httpOptions).pipe(
        map(this.extractData),
        catchError(this.handleError));
  }

  getEventCodeDetails(eventcode:string): Observable<any> {
    return this._http.get(environment.apiUrl + '/eventCodeDetails?eventcode='+eventcode, httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError));
}; 

  deleteEventCode(eventcode: string): Observable<any>{
      return this._http.delete(environment.apiUrl +'/deleteEventMaster?eventcode='+eventcode, httpOptions );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      if (error.status == 417) {
        this.toastr.errorToastr('An error occurred:' + error.error.message, 'Oops!');
      }
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      this.toastr.errorToastr('Backend returned code ' + error.status,"Oops!",{dismiss:'click',showCloseButton:true });

      console.error(
          'Backend returned code ${error.status}, ' +
          'body was: ${error.error}');
          if (error.status == 417) {
            this.toastr.errorToastr('An error occurred:' + error.error.message, 'Oops!');
          }
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };

  private extractData(res: Response) {
    console.log(res);
    let body = res;
    return body || { };
  }
}
