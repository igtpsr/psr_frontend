import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsComponent } from './events/events.component';
import { EventModelAddComponent } from './event-model-add/event-model-add.component';
import { EventModelDetailsComponent } from './event-model-details/event-model-details.component';
import { EventcodeComponent } from './eventcode/eventcode.component';
import { EventcodeModelAddComponent } from './eventcode-model-add/eventcode-model-add.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
  {
    path:'', children:[

      {
        path: 'events',
        component: EventsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'event-model-add',
        component: EventModelAddComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'event-model-details/:type/:modelName',
        component: EventModelDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'eventcode',
        component: EventcodeComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'eventcode-model-add',
        component: EventcodeModelAddComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'eventcode-model-add/:eventcode',
        component: EventcodeModelAddComponent,
        canActivate: [AuthGuard]
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule { }
