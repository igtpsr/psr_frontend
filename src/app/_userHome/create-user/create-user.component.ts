import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, AfterViewChecked, AfterContentInit, OnDestroy, NgZone } from '@angular/core';
import { ApiService } from '../../api.service';
import { Globals } from '../../Globals';
import { IMyDpOptions } from 'mydatepicker';
import { SearchFormUser } from './searchFromUserCreate';
import { environment } from '../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgProgress } from '@ngx-progressbar/core';
import { Router } from '@angular/router';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
  providers: [Globals]
})
export class CreateUserComponent implements OnInit {
  @ViewChild('brUserProfileGrid') public brUserProfileGrid: jqxGridComponent;
  public Roleslist: Array<string> = [];
  public RolesIdlist: Array<string> = [];
  public countryList: Array<string> = [];
  public countryCodeList: Array<string> = [];
  public queryNameList: Array<string> = [];
  public brNameList: Array<string> = [];
  public queryIdList: Array<string> = [];
  passwordEffectiveDateG: any = null;
  passwordExpiredateG: any = null;
  brUserUrl: any;
  userIdUrl: any ='';
  PswdNvrExpreDisableFields: boolean;
  checkboxStatus: boolean = false;
  disableStatus: boolean = false;
  userUpdateDisable: boolean;
  globalElements: boolean = false;
  userLabel: string;
  hidesave: boolean = false;
  hideupdate: boolean = false;
  brUserLists: any;
  hideDisableuserPasCreateDate: boolean = true;
  queryViewList:any;
  /**validation fields starts*/
  useridError: string;
  usernameError: string;
  brUseridError: string;
  userpassError: string;
  userconfPassError: string;
  useremailError: string;
  userroleError: string;
  userqueryError:string;
  usercountryError:string;
  showBrGrid: boolean = false;

  rolesDetails:any;
  /**validation fields ends */

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'mm-dd-yyyy',
    inline: false,
    editableDateField: false,
  };
  searchUser = new SearchFormUser('', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '',0);


  /**br user profile grid starts */
  source: any;
  columns: any;
  dataAdapter: any;

  /**br user profile grid ends */


  constructor(private api?: ApiService, private router?: Router, private route?: ActivatedRoute, public progress?: NgProgress, public toastr?: ToastrManager) {

    this.route.params.subscribe(params => {
      this.userIdUrl = params.userId;
    })
  }

  ngOnInit() {

    this.getRole();
  
    if (this.userIdUrl) {
      this.globalElements = true;
      this.userUpdateDisable = true;
      this.hidesave = true;
      this.PswdNvrExpreDisableFields = true;
      this.hideDisableuserPasCreateDate = false;
      this.userLabel = 'UPDATE USER REGISTRATION';
        /**user update details starts here*/
        let t = this.api.getUserDetails(this.userIdUrl).subscribe(
            res => {
              let resp:any = res;
              this.globalElements = false;
              this.PswdNvrExpreDisableFields = false;
              
              this.extractQueryNameId(resp.allQueryIds);
              this.extractRoleName(resp.ptlRoleProfileDetails);
              this.extractCountry(resp.allCntryNames);


              this.searchUser.userid = resp.ptlUserProfileDetails[0].userId;
              this.searchUser.username = resp.ptlUserProfileDetails[0].userName;
              this.searchUser.brUserID = resp.ptlUserProfileDetails[0].brUserId;
              this.searchUser.userpassword = resp.ptlUserProfileDetails[0].pswd;
              this.searchUser.confirmPassword = resp.ptlUserProfileDetails[0].pswd;
              this.searchUser.useremail = resp.ptlUserProfileDetails[0].email;

              if(resp.ptlUserProfileDetails[0].roleName) {
                this.searchUser.userrole = [{id:resp.ptlUserProfileDetails[0].roleName, text:resp.ptlUserProfileDetails[0].roleName }];
              } else {
                this.searchUser.userrole ='';
              }
             
              if(resp.ptlUserProfileDetails[0].queryId) {
                let queryIndex = this.queryIdList.indexOf(resp.ptlUserProfileDetails[0].queryId.toString());
                if(queryIndex>=0) {
                  this.queryViewList = [{id:this.queryNameList[queryIndex],text:this.queryNameList[queryIndex]}];
                }
              } else {
                this.queryViewList ='';
              }

              if(resp.ptlUserProfileDetails[0].country) {
                let country_index = this.countryCodeList.indexOf(resp.ptlUserProfileDetails[0].country);
                if(country_index>=0) {
                  this.searchUser.usercountry = [{id:this.countryList[country_index],text:this.countryList[country_index]}];
                }
              } else {
                this.searchUser.usercountry ='';
              }
            
              if (resp.ptlUserProfileDetails[0].pswEffectiveDate) {
                this.searchUser.passwordEffectivedate = { jsdate: new Date(resp.ptlUserProfileDetails[0].pswEffectiveDate) };
              } else {
                this.searchUser.passwordEffectivedate = '';
              }

              if (resp.ptlUserProfileDetails[0].pswdExpireDate) {
                this.searchUser.passwordExpiredate = { jsdate: new Date(resp.ptlUserProfileDetails[0].pswdExpireDate) };
              } else {
                this.searchUser.passwordExpiredate = '';
              }

              if(resp.ptlUserProfileDetails[0].createDate) {
                this.searchUser.passwordCreatedate = {jsdate: new Date(resp.ptlUserProfileDetails[0].createDate)};
              } else {
                this.searchUser.passwordCreatedate = '';
              }


              this.searchUser.passwordEffectivedays = resp.ptlUserProfileDetails[0].pswdEffectiveDays;
              this.searchUser.passwordNotifydays = resp.ptlUserProfileDetails[0].pswdNotifyDays;

              if (resp.ptlUserProfileDetails[0].pswdNeverExpires == 'Y') {
                this.checkboxStatus = true;
                this.searchUser.passwordEffectivedate = '';
                this.searchUser.passwordExpiredate = '';
                this.searchUser.passwordEffectivedays = 0;
                this.searchUser.passwordNotifydays = 0;
                this.PswdNvrExpreDisableFields = true;
              } else {
                this.checkboxStatus = false;
              }

              if (resp.ptlUserProfileDetails[0].disableUser == 'Y') {
                this.disableStatus = true;
              } else {
                this.disableStatus = false;
              }
              
              
            }
          )

    } else {
      this.userLabel = 'NEW USER REGISTRATION';
      this.hideupdate = true;
      let t = this.api.getUserDetails(this.userIdUrl).subscribe(
        res => {

          let resp:any = res;
          this.extractQueryNameId(resp.allQueryIds);
          this.extractRoleName(resp.ptlRoleProfileDetails);
          this.extractCountry(resp.allCntryNames);
          
        });
    }  
  }

  getRole(): void {
    let t = this.api.getRoles('N').subscribe(
      res => {
       this.rolesDetails = res;
        // this.extractRoleName(res);

      });
  }

  showHidePassword(): void {
    let eye: any = document.getElementById("showHideEye");
    let eyeClass: any = eye.getAttribute('class');
    let input: any = document.getElementById("userpassword");
    if (eyeClass == 'fa fa-eye') {
      eye.setAttribute('class', 'fa fa-eye-slash');
      input.setAttribute('type', 'text');
    } else {
      eye.setAttribute('class', 'fa fa-eye');
      input.setAttribute('type', 'password');
    }
  }

  extractRoleName(col_array: any) {
    
    let roleArrayName = [];
    let roleArrayId = [];
    for (var key in col_array) {
      roleArrayName.push(col_array[key]);
      roleArrayId.push(key);
    }
    this.Roleslist = roleArrayName;
    this.RolesIdlist = roleArrayId;
  }

  extractCountry(col_array: any) {
    let countryArray = [];
    let countryCode = [];
    for (var key in col_array) {
      countryArray.push(col_array[key]);
      countryCode.push(key);
    }
    this.countryCodeList = countryCode;
    this.countryList = countryArray;
  }

  extractQueryNameId(col_array:any) {
    let queryName = [];
    let queryId = [];
    for (var key in col_array) {
      queryName.push(col_array[key]);
      queryId.push(key);
    }
    this.queryNameList = queryName;
    this.queryIdList = queryId;
  }
 
  extractBrName(col_array:any):void {
    this.brNameList = col_array;
  }

  enterValidate(name) {

    if (name == 'username') {
      if (this.searchUser.username == '') {
        this.usernameError = "Username Required";
      } else {
        this.usernameError = "";
      }
    } else if (name == 'userid') {
      if (this.searchUser.userid == '') {
        this.useridError = "Userid Required";
      } else {
        this.useridError = "";
      }
    } else if (name == 'userpassword') {
        if (this.searchUser.userpassword == '') {
          if(this.userIdUrl) {
            this.userpassError = "";
          } else {
            this.userpassError = "Password Required";
          }
        
      } else {
        this.userpassError = "";
        if (this.validatePassword(this.searchUser.userpassword)) {
          this.userpassError = "";
        } else {
          this.userpassError = "Invalid Password";
        }
      }
    } else if (name == 'useremail') {
      if (this.searchUser.useremail == '') {
        this.useremailError = "Email Required";
      } else {
        this.useremailError = '';
        if (this.validateEmail(this.searchUser.useremail)) {
          this.useremailError = '';
        } else {
          this.useremailError = 'Enter valid email address';

        }
      }

    } else {
      if (this.searchUser.confirmPassword == '') {
          if(this.userIdUrl) {
            this.userconfPassError = ' '
          } else {
            this.userconfPassError = 'Confirm Password Required'
          }
        
      } else {
        let password = this.searchUser.userpassword;
        let confirmPassword = this.searchUser.confirmPassword;
        if (password != confirmPassword) {
          this.userconfPassError = 'Password doesnot match';
        } else {
          this.userconfPassError = '';
        }
      }
    }

  }

  brUserCallchange():void {
    const brUserID: any = (document.getElementById("brUserShow")) as HTMLInputElement;
    brUserID.checked = false;
    this.showBrGrid = false;
  }
  brUserCall(): void {
        
    const brUserID: any = (document.getElementById("brUserShow")) as HTMLInputElement;
        if (this.searchUser.brUserID) {
          if (brUserID.checked == true) {
            this.showBrGrid = true;
            this.source =
              {
                datatype: 'json',
                datafields: [
                  { name: 'brUserName', type: 'string' },
                  { name: 'brRoleId', type: 'string' },
                  { name: 'brRoleName', type: 'string' },
                  { name: 'brAttrName', type: 'string' },
                  { name: 'brAttrValue', type: 'string' }
                ],
                unboundmode: true,
                id: 'userInput',
                url: environment.apiUrl + '/getBrUserProfile?brUserId=' + this.searchUser.brUserID + '&authentication=' + localStorage.getItem('authentication'),
                autorowheight: true,
                autoheight: false,
                autoshowloadelement: false
              };
            this.dataAdapter = new jqx.dataAdapter(this.source);
            this.columns = [
              { text: 'Br User Name', datafield: 'brUserName', width: '230', align: "center", editable: false },
              { text: 'Br Role Id', datafield: 'brRoleId', width: '200', align: "center", editable: false },
              { text: 'Br Role Name', datafield: 'brRoleName', width: '200', align: "center", editable: false },
              { text: 'Br Attr Name', datafield: 'brAttrName', width: '200', align: "center", editable: false },
              { text: 'Br Attr Value', datafield: 'brAttrValue', width: '200', align: "center", editable: false },
            ];
          } else {
            this.showBrGrid = false;
          }
        } else {
          this.toastr.warningToastr("BR User Id field is empty","Alert!",{dismiss:'click',showCloseButton:true });
          brUserID.checked = false;
          this.showBrGrid = false;
        }
  }

  validateForm() {
    let counter = 0;

    if (this.searchUser.username == '' || this.searchUser.username == 'null' || this.searchUser.username == null) {
      counter += 1;
      this.usernameError = "Username Required.";
    } else {
      this.usernameError = "";
    }

    if (this.searchUser.userid == '' || this.searchUser.userid == 'null' || this.searchUser.userid == null) {
      counter += 1;
      this.useridError = "Userid Required.";
    } else {
      this.useridError = "";
    }

    if (this.searchUser.userpassword == '' || this.searchUser.userpassword == 'null' || this.searchUser.userpassword == null) {
      if(this.userIdUrl) {
        this.userpassError = ' '
      } else {
        counter += 1;
        this.userpassError = "Password Required.";
      }
      
    } else {
      this.userpassError = "";
      if (this.validatePassword(this.searchUser.userpassword)) {
        this.userpassError = "";
      } else {
        this.userpassError = "Invalid Password";
        counter += 1;
      }
    }

    if (this.searchUser.confirmPassword == '' || this.searchUser.confirmPassword == 'null' || this.searchUser.confirmPassword == null) {
     
      if(this.userIdUrl) {
        if(this.searchUser.userpassword) {
          counter += 1;
          this.userconfPassError = "Confirm Password Required.";
        } else {
          this.userconfPassError = ' '
        }
        
      } else {
        counter += 1;
        this.userconfPassError = "Confirm Password Required.";
      }
    } else {
      let password = this.searchUser.userpassword;
      let confirmPassword = this.searchUser.confirmPassword;
      if (password != confirmPassword) {
        this.userconfPassError = 'Password doesnot match';
        counter += 1;
      } else {
        this.userconfPassError = '';
      }
    }

    if (this.searchUser.useremail == '' || this.searchUser.useremail == null || this.searchUser.useremail == 'null') {
      counter += 1;
      this.useremailError = "Email Required.";
    } else {
      this.useremailError = '';
      if (this.validateEmail(this.searchUser.useremail)) {
        this.useremailError = '';
      } else {
        this.useremailError = 'Please enter a valid email address.';
        counter += 1;

      }
    }
    if (this.searchUser.userrole == 'undefined' || this.searchUser.userrole == undefined || this.searchUser.userrole.length == 0 || this.searchUser.userrole ==0) {
      counter += 1;
      this.userroleError = "Role Required.";

    } else {
      this.userroleError = '';
    }
    
    if (this.queryViewList == 'undefined' || this.queryViewList == undefined || this.queryViewList == 0) {
      counter += 1;
      this.userqueryError = "Query Name Required.";

    } else {
      this.userqueryError = '';
    }

    if (this.searchUser.usercountry == 'undefined' || this.searchUser.usercountry == undefined || this.searchUser.usercountry == 0) {
      counter += 1;
      this.usercountryError = "Country  Required.";

    } else {
      this.usercountryError = '';
    }

    if(this.rolesDetails.filter(result=>result.roleName == this.searchUser.userrole[0].id && result.brUserIdStatus ==true).length>0 && !!this.searchUser.brUserID == false) {
      counter += 1; 
      this.brUseridError = "BR User Id Required";
    } else {
      this.brUseridError = "";
    }


    if (counter > 0) {
      return false;
    } else {
      return true;
    }

  }

  validateEmail(sEmail) {
    var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

    if (!sEmail.match(reEmail)) {
      //alert("Invalid email address");
      return false;
    }

    return true;
  }

  validatePassword(sPass) {
    var rePass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]{8,16}$/;
    if (!rePass.test(sPass)) {
      return false;
    }
    return true;
  }

  passwordExpireEvent(): void {
    const defaultView: any = (document.getElementById("passwordNeverExpire")) as HTMLInputElement;
    if (defaultView.checked == true) {
      this.searchUser.passwordEffectivedate = '';
      this.searchUser.passwordExpiredate = '';
      this.searchUser.passwordEffectivedays = 0;
      this.searchUser.passwordNotifydays = 0;
      this.PswdNvrExpreDisableFields = true;
      //this.searchUser.passwordNeverExpire = 'true';
    } else {
      this.PswdNvrExpreDisableFields = false;
      //this.searchUser.passwordNeverExpire = 'false';
    }
  }

  onChangeViews(event):void {
    if(this.searchUser.userrole != undefined) {
      if(this.searchUser.userrole.length > 1) {
    
        this.searchUser.userrole.splice(1);
        this.toastr.errorToastr('Multiple role selections not supported', 'Error!', { dismiss: 'click', showCloseButton: true });
      }
    }
  }

  submitUserForm() {
    if (this.validateForm()) {

      let role_id = [];
      let role_index = this.Roleslist.indexOf(this.searchUser.userrole[0].text);
      role_id[0] = this.RolesIdlist[role_index];
      let index = this.queryNameList.indexOf(this.queryViewList[0].text);
      let queryId = this.queryIdList[index];

      const defaultView: any = (document.getElementById("passwordNeverExpire")) as HTMLInputElement;
      let passwordcheckstatus: string = defaultView.checked == true ? 'Y' : 'N';

      if (this.searchUser.passwordEffectivedate != 'null' && this.searchUser.passwordEffectivedate != '' && this.searchUser.passwordEffectivedate != null) {
        var passwordEffec = new Date(this.searchUser.passwordEffectivedate.jsdate);
        this.passwordEffectiveDateG = passwordEffec.getFullYear() + '-' + (passwordEffec.getMonth() + 1) + '-' + passwordEffec.getDate();
      }

      if (this.searchUser.passwordExpiredate != 'null' && this.searchUser.passwordExpiredate != '' && this.searchUser.passwordExpiredate != null) {
        var passwordExp = new Date(this.searchUser.passwordExpiredate.jsdate);
        this.passwordExpiredateG = passwordExp.getFullYear() + '-' + (passwordExp.getMonth() + 1) + '-' + passwordExp.getDate();
      }

      /** country save details starts*/
      let countryIndex = this.countryList.indexOf(this.searchUser.usercountry[0].text);
      let countryCode: any = this.countryCodeList[countryIndex];
      if(!countryCode) {
        countryCode ='';
      }
      /** country save details ends*/

      let user_details = {
        "userId": this.searchUser.userid,
        "userName": this.searchUser.username,
        "brUserId":this.searchUser.brUserID,
        "pswd": this.searchUser.userpassword,
        "email": this.searchUser.useremail,
        "roleIds": role_id,
        "country": countryCode,
        "pswEffectiveDate": this.passwordEffectiveDateG,
        "pswdEffectiveDays": this.searchUser.passwordEffectivedays,
        "pswdNotifyDays": this.searchUser.passwordNotifydays,
        "pswdExpireDate": this.passwordExpiredateG,
        "pswdNeverExpires": passwordcheckstatus,
        "queryId":+queryId

      }
      this.progress.start();
      let userDetails = this.api.createUpdateUser(user_details, localStorage.getItem('loginUserId'));
      userDetails.subscribe(
        res => {
          let saveRes: any = res;

          if (saveRes.type == 'ERROR') {
            this.toastr.errorToastr(saveRes.message, 'Error!', { dismiss: 'click', showCloseButton: true });

          } else {
            this.toastr.successToastr(saveRes.message, 'Success!');
            this.router.navigate(['/user/user-profile']);
          }
          this.progress.complete();
        }, error => {
          this.toastr.errorToastr('error', 'Error!', { dismiss: 'click', showCloseButton: true });
          this.progress.complete();
        }
      );

    }


  }

  updateUser(): void {
   

    if (this.validateForm()) {

      let role_id = [];
      let role_index = this.Roleslist.indexOf(this.searchUser.userrole[0].text);
      role_id[0] = this.RolesIdlist[role_index];

      let index = this.queryNameList.indexOf(this.queryViewList[0].text);
      let queryId = this.queryIdList[index];

      const defaultView: any = (document.getElementById("passwordNeverExpire")) as HTMLInputElement;
      let passwordcheckstatus: string = defaultView.checked == true ? 'Y' : 'N';

      const disableUserStat: any = (document.getElementById("disableUser")) as HTMLInputElement;
      let disableUser: string = disableUserStat.checked == true ? 'Y' : 'N';

      if (this.searchUser.passwordEffectivedate != 'null' && this.searchUser.passwordEffectivedate != '' && this.searchUser.passwordEffectivedate != null) {
        var passwordEffec = new Date(this.searchUser.passwordEffectivedate.jsdate);
        this.passwordEffectiveDateG = passwordEffec.getFullYear() + '-' + (passwordEffec.getMonth() + 1) + '-' + passwordEffec.getDate();
      }

      if (this.searchUser.passwordExpiredate != 'null' && this.searchUser.passwordExpiredate != '' && this.searchUser.passwordExpiredate != null) {
        var passwordExp = new Date(this.searchUser.passwordExpiredate.jsdate);
        this.passwordExpiredateG = passwordExp.getFullYear() + '-' + (passwordExp.getMonth() + 1) + '-' + passwordExp.getDate();
      }
      /** country save details starts*/
      let countryIndex = this.countryList.indexOf(this.searchUser.usercountry[0].text);
      let countryCode: any = this.countryCodeList[countryIndex];
      if(!countryCode) {
        countryCode ='';
      }
      /** country save details ends*/
      let user_details = {
        "userId": this.searchUser.userid,
        "userName": this.searchUser.username,
        "brUserId": this.searchUser.brUserID,
        "pswd": this.searchUser.userpassword,
        "email": this.searchUser.useremail,
        "roleIds": role_id,
        "country": countryCode,
        "disableUser": disableUser,
        "pswEffectiveDate": this.passwordEffectiveDateG,
        "pswdEffectiveDays": this.searchUser.passwordEffectivedays,
        "pswdNotifyDays": this.searchUser.passwordNotifydays,
        "pswdExpireDate": this.passwordExpiredateG,
        "pswdNeverExpires": passwordcheckstatus,
        "queryId":+queryId
      }

      this.progress.start();
      let userDetails = this.api.updateUserProfile(user_details, this.searchUser.userid, localStorage.getItem('loginUserId'));
      userDetails.subscribe(res => {
        let saveRes: any = res;
        if (saveRes.type == 'ERROR') {
          this.toastr.errorToastr(saveRes.message, 'Error!', { dismiss: 'click', showCloseButton: true });

        } else {
          this.toastr.successToastr(saveRes.message, 'Success!');
          this.router.navigate(['/user/user-profile']);
        }
        this.progress.complete();
      }, error => {
        this.toastr.errorToastr('error', 'Error!', { dismiss: 'click', showCloseButton: true });
        this.progress.complete();

      });
    }
  }
  
}
