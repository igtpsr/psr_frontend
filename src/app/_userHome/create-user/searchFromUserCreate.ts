export class SearchFormUser {
    constructor(
      public userid: string,
      public username: string,
      public brUserID:any,
      public userpassword: string,
      public confirmPassword:string,
      public useremail: string,
      public usercountry: any,
      public passwordEffectivedate: any,
      public passwordCreatedate:any,
      public passwordEffectivedays:number,
      public passwordNotifydays: number,
      public passwordExpiredate: any,
      public passwordNeverExpire: string,
      public disableUser: string,
      public brUserShow:string,
      public DisabelUser: string,
      public passwordChangeDate: any,
      public passwordChangeNxtLogin: string,
      public passwordResetDigest: string,
      public userrole:any
    ) {}
  }
  