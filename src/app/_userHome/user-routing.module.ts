import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { CreateRoleComponent } from './create-role/create-role.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { RoleProfileComponent } from './role-profile/role-profile.component';
import { AuthGuard } from '../auth.guard';


const routes: Routes = [

  {
    path:'', children:[
     
      {
        path: 'role-profile',
        component: RoleProfileComponent,
        canActivate: [AuthGuard]
       
      },
      {
        path: 'user-profile',
        component: UserProfileComponent,
        canActivate: [AuthGuard]
      },
    
      {
        path: 'create-user',
        component: CreateUserComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'create-user/:userId',
        component: CreateUserComponent,
        canActivate: [AuthGuard]
      },
    
      {
        path: 'create-role',
        component: CreateRoleComponent,
        canActivate: [AuthGuard]
      },
    
      {
        path: 'create-role/:roleId',
        component: CreateRoleComponent,
        canActivate: [AuthGuard]
      },
    
    ]
  },

 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
