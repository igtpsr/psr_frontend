export class RoleForm {
    constructor(
      public roleId:string,
      public roleName: string,
      public description: string,
      public appName: string,
    ) {}
  }
  