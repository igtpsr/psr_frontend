import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgProgress } from '@ngx-progressbar/core';

import { Router } from '@angular/router';
import { RoleForm } from './roleFrom';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.css']
})
export class CreateRoleComponent implements OnInit {
  roleModel = new RoleForm('', '', '', '');
  roleRes: any;
  roleName: string;
  roleId: string;
  hideSubmit: boolean = false;
  hideUpdate: boolean = false;
  hideRoleId: boolean = false;
  disableRoleId: boolean = true;
  disableRoleHide:boolean = true;
  disableRoleStatus:boolean = false;

  brUserIdStatus:boolean = false;
  roleProfileLabel: string;
  roleNameError:string;
  roleDiscError:string;
  roleAppnameError:string;
  constructor(private api: ApiService, private route: ActivatedRoute, public toastr: ToastrManager, private router: Router, public progress: NgProgress) {
    this.route.params.subscribe(params => {
      this.roleId = params.roleId;
    });
  }

  ngOnInit() {
    if (this.roleId) {
      this.hideSubmit = true;
      this.roleProfileLabel = 'UPDATE ROLE PROFILE';
      this.disableRoleHide = false;
      let t = this.api.getRoleDetails(this.roleId);
      t.subscribe(res => {
        this.roleModel.roleId = res[0].roleId;
        this.roleModel.roleName = res[0].roleName;
        this.roleModel.description = res[0].description;
        if( res[0].appName) {
          this.roleModel.appName = res[0].appName;
        }
       
        if(res[0].disableRole =='Y') {
          this.disableRoleStatus = true;
        } else {
          this.disableRoleStatus = false;
        }

        if(res[0].brUserIdStatus ==1) {
          this.brUserIdStatus = true;
        } else {
          this.brUserIdStatus = false;
        }

      });
    } else {
      this.hideUpdate = true;
      this.roleProfileLabel = 'SAVE ROLE PROFILE';
      this.hideRoleId = true;
    }

  }
  validateForm() {
    let counter = 0;
    if(this.roleModel.roleName=='') {
      counter+=1;
      this.roleNameError = 'Role is Required';
    } else {
      this.roleNameError = '';
    }
    if(this.roleModel.description=='') {
      counter+=1;
      this.roleDiscError = 'Role Description is Required';
    } else {
      this.roleDiscError = '';
    }
    if(this.roleModel.appName=='') {
      counter+=1;
      this.roleAppnameError = 'Select Role App';
    } else {
      this.roleAppnameError = '';
    }
    if(counter>0) {
      return false;
    } else {
      return true;
    }

  }
 
  enterValidate(name) {
    
    if(name == 'roleName') {
      if(this.roleModel.roleName=='') {
        this.roleNameError = "Role is Required";
      } else {
        this.roleNameError = "";
      }
    }
    if(name == 'description') {
      if(this.roleModel.description=='') {
        this.roleDiscError = "Role Description is Required";
      } else {
        this.roleDiscError = "";
      }
    }

   
  }

  saveRole(): void {
    
    if(this.validateForm()) {
      this.progress.start();
      let role = {
        "roleName": this.roleModel.roleName,
        "description": this.roleModel.description,
        "appName": this.roleModel.appName
      }
      let t = this.api.addRoleName(role);
      t.subscribe(
        res => {
          this.roleRes = res;
          this.progress.complete();
          if (this.roleRes.type === 'ERROR') {
            this.toastr.errorToastr(this.roleRes.message, 'Error!' ,{dismiss:'click',showCloseButton:true });
          } else {
            this.toastr.successToastr(this.roleRes.message, 'Success!');
            this.router.navigateByUrl('/user/role-profile');
          }
        }, error => {
          this.toastr.errorToastr('error', 'Error!' ,{dismiss:'click',showCloseButton:true });
          this.progress.complete();
        }
      );
    }
    
  };

  updateRole(): void {
    
    if(this.validateForm()) {

      const disableRole:any = (document.getElementById("disableRole")) as HTMLInputElement;
      let passwordcheckstatus:string = disableRole.checked==true?'Y':'N';

      const brUserId:any = (document.getElementById("brUserId")) as HTMLInputElement;
      let brUserIdStatus:number = brUserId.checked==true?1:0;

      this.progress.start();
      let role = {
        "roleName": this.roleModel.roleName,
        "description": this.roleModel.description,
        "appName": this.roleModel.appName,
        "disableRole":passwordcheckstatus,
        "brUserIdStatus":brUserIdStatus
      }
      let t = this.api.updateRoleName(this.roleId, role);
      t.subscribe(
        res => {
          this.roleRes = res;
          this.progress.complete();
          if (this.roleRes.type === 'ERROR') {
            this.toastr.errorToastr(this.roleRes.message, 'Error!',{dismiss:'click',showCloseButton:true });
          } else {
            this.toastr.successToastr(this.roleRes.message, 'Success!');
            this.router.navigateByUrl('/user/role-profile');
          }
        }, error => {
          this.toastr.errorToastr('error', 'Error!',{dismiss:'click',showCloseButton:true });
          this.progress.complete();
        }
      );
    }
  };

}
