import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { CreateUserComponent } from './create-user/create-user.component';
import { RoleProfileComponent } from './role-profile/role-profile.component';
import { CreateRoleComponent } from './create-role/create-role.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import {SharedModule} from '../shared.module';



@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    CreateUserComponent,
    CreateRoleComponent, 
    RoleProfileComponent,
    UserProfileComponent,
    
  ]
})
export class UserModule { }
