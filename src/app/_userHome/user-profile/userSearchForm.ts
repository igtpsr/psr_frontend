export class UserSearchForm {
    constructor(
        public userInput: string,
        public roleInput: string,
        public userSelect: string,
        public roleSelect: string,
    ) {}
}