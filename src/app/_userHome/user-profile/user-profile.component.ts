import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { ApiService } from '../../api.service';
import { UserSearchForm } from './userSearchForm';
import { NgProgress } from '@ngx-progressbar/core';
import { Globals } from '../../Globals';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers: [Globals]
})
export class UserProfileComponent implements OnInit {
  UserSearchModel = new UserSearchForm('', '','','');
  @ViewChild('userProfileGrid') public userProfileGrid: jqxGridComponent;
  addNewModel = true;
  userIddata: any = null;
  roleRes: any;
  model: any = {};
  Useroperators = [
    { value: 'Like', label: 'Like' },
    { value: 'Not like', label: 'Not like' },
    { value: 'Equal to', label: 'Equal to' },
    { value: 'Not equal to', label: 'Not equal to' },
    { value: 'In the list', label: 'In the list' },
    { value: 'Not in the list', label: 'Not in the list' },
    { value: 'Starts with', label: 'Starts with' },
    { value: 'Ends with', label: 'Ends with' },
    { value: 'Equal to null', label: 'Equal to null' },
    { value: 'Is not null', label: 'Is not null' },
  ];

  source: any =
    {
      datatype: 'json',
      datafields: [
        { name: 'userId', type: 'string' },
        { name: 'roleName', type: 'string' },
      ],
      unboundmode: true,
      id: 'userInput',
      url: environment.apiUrl + '/getUserProfile?userId=' +'&disable='+'N'+'&roleInput='+'&authentication=' + localStorage.getItem('authentication'),
      autorowheight: true,
      autoheight: false,
      autoshowloadelement: false
    };

  dataAdapter: any = new jqx.dataAdapter(this.source);
  
  columns = [
    { text: 'Select', datafield: 'select', align: "center", columntype: 'checkbox', width: '50', pinned: true, editoptions: { value: "1:0" }, formatoptions: { disabled: false } },
    { text: 'USER NAME', datafield: 'userId', width: '230', align: "center", editable: false },
    { text: 'ROLE', datafield: 'roleName', width: '200', align: "center", editable: false }];

  constructor(private api: ApiService, public progress: NgProgress, public _global: Globals, private router: Router, public toastr: ToastrManager) {
    this.UserSearchModel.userSelect = 'Like';
    this.UserSearchModel.roleSelect = 'Like';
  }

  ngOnInit() {
    let roleName = localStorage.getItem('roleName')
    if (roleName !== '' && roleName !== null && roleName !== 'null') {
      if (roleName !== 'PSR_ADMIN' && roleName !== 'PTL_ADMIN') {
        this.toastr.warningToastr('You are not authorized to access this page','Oops!',{dismiss:'click',showCloseButton:true });
        window.location.href = '/welcome';
      }
    }
  };

  getUserData(): void {
    this.progress.start();
    this._global.elementDisabled = true;

    //user
    const userInput = <HTMLInputElement>document.getElementById('userInput');
    const userselect = <HTMLInputElement>document.getElementById('Useroperator');

    //role
    const roleoperator = <HTMLInputElement>document.getElementById('roleSelect');
    const roleInput = <HTMLInputElement>document.getElementById('roleInput');

    //Diable user
    const disableUserStat:any = (document.getElementById("userDisable")) as HTMLInputElement;
    let disableUser:string = disableUserStat.checked==true?'Y':'N';  

    this.source.url = environment.apiUrl + '/getUserProfile?userId=' + userInput.value
      +'&disable='+disableUser+ '&searchOperator=' + userselect.value +'&roleInput='+roleInput.value+'&roleOperator=' +roleoperator.value+'&authentication=' + localStorage.getItem('authentication');
    this.userProfileGrid.updatebounddata('cells');
    this.userProfileGrid.refreshdata();
    this.progress.complete();
    this._global.elementDisabled = false;

  };

  public onChange(id): void {

    if(id=='Useroperator') {
       //username
      const Useroperator = <HTMLInputElement>document.getElementById('Useroperator');
      const userInput = <HTMLInputElement>document.getElementById('userInput');
      if (Useroperator.value === 'Is not null' || Useroperator.value === 'Equal to null') {
        userInput.disabled = true;
        this.UserSearchModel.userInput = '';
        userInput.value = null;
      } else {
        userInput.disabled = false;
      }

    } else {
      //roleName
      const Roleoperator = <HTMLInputElement>document.getElementById('roleSelect');
      const roleInput = <HTMLInputElement>document.getElementById('roleInput');
      if (Roleoperator.value === 'Is not null' || Roleoperator.value === 'Equal to null') {
        roleInput.disabled = true;
        this.UserSearchModel.roleInput = '';
        roleInput.value = null;
      } else {
        roleInput.disabled = false;
      }

    }
   
  }

  handlekeyboardnavigation = (event): boolean => {
    const currentCell: any = this.userProfileGrid.getselectedcell();
    if (currentCell !== null) {
      const key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
      if (currentCell.datafield === 'userId') {
        if (key === 86) {
          this.toastr.warningToastr("User Name is a non editable field","Alert!",{dismiss:'click',showCloseButton:true });
          event.preventDefault();
          return true;
        }
      }
      if (currentCell.datafield === 'roleName') {
        if (key === 86) {
          this.toastr.warningToastr("Role is a non editable field","Alert!",{dismiss:'click',showCloseButton:true });
          event.preventDefault();
          return true;
        }
      }
    }
  }

  deleteUser(): void {
    if(this.userIddata) {
        let res = confirm("Are you sure want to delete the User ?");
        if (res != undefined && res != null && res === true) {
          this.progress.start();
          let t = this.api.deleteUser(this.userIddata);
          t.subscribe(
            res => {
              this.roleRes = res;
              this.progress.complete();
              if (this.roleRes.type === 'error') {
                this.toastr.errorToastr(this.roleRes.message, 'Error!',{dismiss:'click',showCloseButton:true });
              } else {
                this.toastr.successToastr(this.roleRes.message, 'Success!');
                this.getUserData();
                this.userIddata = null;
              }
            }, error => {
              this.toastr.errorToastr('error', 'Error!',{dismiss:'click',showCloseButton:true });
              this.progress.complete();
            }
          );
        }
    } else {
      this.toastr.warningToastr("Please select any user you want to delete","Alert!",{dismiss:'click',showCloseButton:true });
    }
    
  }

  resetForm(): void {
    var userInput = <HTMLInputElement>document.getElementById("userInput");
    var roleInput = <HTMLInputElement>document.getElementById("roleInput");
    const Useroperator = <HTMLInputElement>document.getElementById('Useroperator');
    const Roleoperator = <HTMLInputElement>document.getElementById('roleSelect');
    if (Useroperator.value === 'Is not null' || Useroperator.value === 'Equal to null') {
       userInput.disabled = false;
     }
    if (Roleoperator.value === 'Is not null' || Roleoperator.value === 'Equal to null') {
      roleInput.disabled = false;
    }
  }
  cellEndevent(event:any):void {
    if(event.args.value==true) {
      if( event.args.datafield === 'select') {
          this.userIddata = event.args.row.userId;
          if(this.userProfileGrid.getcellvalue(event.args.rowindex, event.args.datafield)) {
            for (let i = 0; i < this.userProfileGrid.getrows().length; i++) {
              if(i !== event.args.rowindex) {
                this.userProfileGrid.setcellvalue(i,event.args.datafield,false);
              }
          }
        }
      }
    } else {
      this.userIddata = null;
    }
    
  }
  updateUser():void {
    if(this.userIddata !=null) {
      this.router.navigate(['/user/create-user/'+this.userIddata]);
    } else {
      this.toastr.warningToastr("Please select any user you want to update","Alert!",{dismiss:'click',showCloseButton:true });

    }
    
  }
}