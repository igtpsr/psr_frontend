import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { ApiService } from '../../api.service';
import { RoleSearchForm } from './roleSearchForm';
import { NgProgress } from '@ngx-progressbar/core';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

export interface PeriodicElement {
  roleName: string;
  select: number;
}

@Component({
  selector: 'app-role-profile',
  templateUrl: './role-profile.component.html',
  styleUrls: ['./role-profile.component.css']
})
export class RoleProfileComponent implements OnInit {

  searchOperatorRole = new RoleSearchForm('', '');

  @ViewChild('roleGrid') public roleGrid: jqxGridComponent;
  roles: any = {};
  ELEMENT_DATA: PeriodicElement[];
  roleId: any = null;
  roleRes: any;
  roleOperators = [
    { value: 'Like', label: 'Like' },
    { value: 'Not like', label: 'Not like' },
    { value: 'Equal to', label: 'Equal to' },
    { value: 'Not equal to', label: 'Not equal to' },
    { value: 'In the list', label: 'In the list' },
    { value: 'Not in the list', label: 'Not in the list' },
    { value: 'Starts with', label: 'Starts with' },
    { value: 'Ends with', label: 'Ends with' },
    { value: 'Equal to null', label: 'Equal to null' },
    { value: 'Is not null', label: 'Is not null' },
  ];

  source: any =
    {
      datatype: 'json',
      datafields: [
        { name: 'select', type: 'bool' },
        { name: 'roleId', type: 'int' },
        { name: 'roleName', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'appName', type: 'string' },
      ],
      unboundmode: true,
      id: 'roleName',
      url: environment.apiUrl + '/getRoleProfile?roleName=' +'&disable='+'N'+ '&authentication=' + localStorage.getItem('authentication'),
      // url: '../assets/model6.txt',
      autorowheight: true,
      autoheight: false,
      autoshowloadelement: false
    };

  dataAdapter: any = new jqx.dataAdapter(this.source);
  columns = [
    { text: 'Select', datafield: 'select', align: "center", columntype: 'checkbox', width: '50', pinned: true, editoptions: { value: "1:0" }, formatoptions: { disabled: false } },
    { text: 'Role Id', datafield: 'roleId', width: '100', editable: false, hidden:true },
    { text: 'Role Name', datafield: 'roleName', width: '100', editable: false },
    { text: 'Description', datafield: 'description', width: '250', editable: false },
    { text: 'App Name', datafield: 'appName', width: '100', editable: false }
  ];

  constructor(private api: ApiService, public progress: NgProgress, public toastr: ToastrManager, private router: Router) {
    this.searchOperatorRole.searchOperator = 'Like';
  }

  ngOnInit() {
  };

  getroleName(): void {
   
    const roleName = <HTMLInputElement>document.getElementById('roleName');
    const roleNameselect = <HTMLInputElement>document.getElementById('roleOperator');

     //Diable Role
    const disableRoleStat:any = (document.getElementById("roleDisable")) as HTMLInputElement;
    let disableRole:string = disableRoleStat.checked==true?'Y':'N';
    this.source.url = environment.apiUrl + '/getRoleProfile?roleName=' + roleName.value
      +'&disable='+disableRole+ '&searchOperator=' + roleNameselect.value + '&authentication=' + localStorage.getItem('authentication');
    this.roleGrid.updatebounddata('cells');
    this.roleGrid.refreshdata();
    
  };

  public onChange(event): void {
    const roleOperator = <HTMLInputElement>document.getElementById('roleOperator');
    const roleName = <HTMLInputElement>document.getElementById('roleName');

    if (roleOperator.value === 'Is not null' || roleOperator.value === 'Equal to null') {
      roleName.disabled = true;
      this.searchOperatorRole.roleName = '';
      roleName.value = null;
    } else {
      roleName.disabled = false;
    }
  }

  handlekeyboardnavigation = (event): boolean => {
    const currentCell: any = this.roleGrid.getselectedcell();
    if (currentCell !== null) {
      const key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
      if (this.roleGrid.getrows().length !== 0) {
        if (key === 86 && event.ctrlKey === true) {
          this.toastr.errorToastr('paste not allowed on grid', 'Error!', { dismiss: 'click', showCloseButton: true });
          event.preventDefault();
          return true;
        }
      }
    }
  }

  resetForm(): void {
    var roleName = <HTMLInputElement>document.getElementById("roleName");
    const roleOperator = <HTMLInputElement>document.getElementById('roleOperator');
    if (roleOperator.value === 'Is not null' || roleOperator.value === 'Equal to null') {
      roleName.disabled = false;
    }
  }
  cellendeditrole(event: any) {
    // let getRoleevent: any = event;
    // let rowId = event.args.rowindex;
    // this.roleId = this.roleGrid.getcellvalue(rowId, 'roleId');

    if(event.args.value==true) {
      if( event.args.datafield === 'select') {
        this.roleId = event.args.row.roleId;
          if(this.roleGrid.getcellvalue(event.args.rowindex, event.args.datafield)) {
            for (let i = 0; i < this.roleGrid.getrows().length; i++) {
              if(i !== event.args.rowindex) {
                this.roleGrid.setcellvalue(i,event.args.datafield,false);
              }
          }
        }
      }
    } else {
      this.roleId = null;
    }
  }
  updateRole(): void {
    if(this.roleId != null) {
      this.router.navigate(['/user/create-role/' + this.roleId])
    } else {
      this.toastr.warningToastr("Please select any role you want to update","Alert!",{dismiss:'click',showCloseButton:true });
    }
  }

  deleteRole(): void {
    if(this.roleId) {
      let res = confirm("Are you sure want to delete the data ?");
      if (res != undefined && res != null && res === true) {
          this.progress.start();
          let t = this.api.deleteRole(this.roleId);
          t.subscribe(
            res => {
              this.roleRes = res;
              this.progress.complete();
              if (this.roleRes.type === 'ERROR') {
                this.toastr.errorToastr(this.roleRes.message, 'Error!' ,{dismiss:'click',showCloseButton:true });
              } else {
                this.toastr.successToastr(this.roleRes.message, 'Success!');
                this.getroleName();
                this.roleId = null;
              }
            }, error => {
              this.toastr.errorToastr('error', 'Error!',{dismiss:'click',showCloseButton:true });
              this.progress.complete();
            }
          );
      } 

    } else {
      this.toastr.warningToastr("Please select any role to delete","Alert!",{dismiss:'click',showCloseButton:true });
    }
    
  }

}
