export class RoleSearchForm {
    constructor(
        public roleName: string,
        public searchOperator: string,
    ) {}
}

