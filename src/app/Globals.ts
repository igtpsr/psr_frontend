import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
    public elementDisabled: boolean = false;
}