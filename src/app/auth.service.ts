import { Injectable } from '@angular/core';
import { Observable, of as observableOf, throwError, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { ToastrManager } from 'ng6-toastr-notifications';

const apiUrl = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, public toastr: ToastrManager) { }

  setLoggedIn(value: boolean): void {
    this.isLoggedIn.next(value);
  };

  setLoginSessionStartTime(authentication: string, startTime: number): any {
    //needs to use this function in future
  }

  getLoggedIn(): Observable<boolean> {
    const $user = localStorage.getItem('authentication');
   // const $user = JSON.parse($getAuth);
    if ($user === null) {
      return observableOf(false);
    }    
    this.setLoggedIn(true);   
    const isLogged = this.isLoggedIn.asObservable();
    return isLogged;
  };

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
      if (error.status == 417) {
        this.toastr.errorToastr('An error occurred:' + error.error.message, 'Oops!');
      }
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        'Backend returned code ${error.status}, ' +
        'body was: ${error.error}');
        if (error.status == 417) {
          this.toastr.errorToastr('An error occurred:' + error.error.message, 'Oops!');
        }
  
    }
    if (error.status === 0 || error.status === 503) {
      return throwError('backend not available');
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };

  private extractData(res: Response) {
    console.log(res);
    let body = res;
    return body || {};
  }
  getUserDetails(username, password): Observable<any> {
    return this.http.post(environment.apiUrl + '/login', { "username": username, "password": password }).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  isBackEndLive(): Observable<any> {
    return this.http.get(environment.apiUrl + '/isAlive').pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  logout(authKey, validSession): Observable<any> {
    //alert('authKey--->'+authKey+'    validSession '+validSession);
    return this.http.get(environment.apiUrl + '/logout?authKey=' + authKey + '&validSession=' + validSession).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }
}
