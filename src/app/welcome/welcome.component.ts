import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  message : string;

  constructor() { }

  ngOnInit() {
    const roleName = localStorage.getItem('roleName');
    if(roleName === undefined || roleName === null || roleName === 'null' || roleName === 'undefined'){
      this.message = 'There is no role configured for you to access the applications, could you please contact Admin!';
    }
  }

}
