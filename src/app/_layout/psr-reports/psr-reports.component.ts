import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl, } from '@angular/platform-browser';
import { } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { NgProgress } from '@ngx-progressbar/core';
import { ApiService } from '../../api.service';
@Component({
  selector: 'app-psr-reports',
  templateUrl: './psr-reports.component.html',
  styleUrls: ['./psr-reports.component.css']
})
export class PsrReportsComponent implements OnInit {

  reportId: any;
  safePsrReportUrls: SafeUrl;

  responseData: any;

  constructor(private sanitizer: DomSanitizer, private route: ActivatedRoute, private progress?: NgProgress, private api?: ApiService) {
    this.api.getReportUrls().subscribe(
      res => {
        console.log("-------reports-------" + JSON.stringify(res));
        this.responseData = res;
        console.log(this.responseData.report1)
        console.log(typeof(this.responseData.report1));
        this.progress.start();
        this.route.params.subscribe(params => {
          this.reportId = params.reportId;
          console.log(' reportId  ' + this.reportId);
          if (this.reportId == 1) {
            //console.log(' reportId 1 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('window.open(http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fPSR+Critical+Days+Reminder');            
            // Date : 11/2/2019 -- Added pop window as suggested by Ron

            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report1, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://reportrsas01/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fPSR+Critical+Days+Reminder','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');                               
          }
          if (this.reportId == 2) {
            //console.log(' reportId 2 '+this.reportId);
            // this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fANF+WIP+Report');               
            // Date : 11/2/2019 -- Added pop window as suggested by Ron


            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report2, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://reportrsas01/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fANF+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');                               
          }
          if (this.reportId == 3) {
            //console.log(' reportId 3 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report3, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://reportrsas01/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 4) {
            //console.log(' reportId 4 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report4, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fNYCO+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 5) {
            //console.log(' reportId 5 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report5, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('https://app.powerbi.com/groups/me/reports/ecc8a2fe-3d3c-4b82-b8a3-1378afc1407d/ReportSection936dff19f9b45bad1795','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 6) {
            //console.log(' reportId 6 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report6, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fFAS+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 7) {
            //console.log(' reportId 7 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report7, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+SWT+PSR+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 8) {
            //console.log(' reportId 8 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report8, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+SWT+PSR+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 9) {
            //console.log(' reportId 9 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report9, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fTOR+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 10) {
            //console.log(' reportId 9 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report10, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fTOR+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 11) {
            //console.log(' reportId 9 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report11, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fTOR+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 12) {
            //console.log(' reportId 9 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report12, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fTOR+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          if (this.reportId == 13) {
            //console.log(' reportId 9 '+this.reportId);
            // Commented to avoid unsafe scripts blockage
            //this.safePsrReportUrls = this.sanitizer.bypassSecurityTrustResourceUrl('http://axreportdev/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fEXP+PSR+Report');
            // Date : 11/2/2019 -- Added pop window as suggested by Ron
            //For Test Environment
            this.safePsrReportUrls = window.open(this.responseData.report13, 'popUpWindow', 'height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
            //For Prod Environment
            //this.safePsrReportUrls = window.open('http://sazrptraspaag/Reports/Pages/Report.aspx?ItemPath=%2fReports%2fMerchandising%2fTOR+WIP+Report','popUpWindow','height=500,width=800,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');  
          }
          this.progress.complete();
        }
        );
      },
      error => {
        console.log(error);
      }
    )


  }

  ngOnInit() {

  }

}


