import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PsrReportsComponent } from './psr-reports.component';

describe('PsrReportsComponent', () => {
  let component: PsrReportsComponent;
  let fixture: ComponentFixture<PsrReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PsrReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PsrReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
