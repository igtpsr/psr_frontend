import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;
  // logoSrc = 'assets/images/logo.jpg';
  logoSrc = environment.imagePath + '/logo.jpg';
  merchantTM = 'assets/manuals/merchandising_training_manual.pdf';
  vendorTM = 'assets/manuals/vendor_training_manual.pdf';
  excelJA = 'assets/manuals/PSR_importExport_JOBaid20191011_v3.pdf';
  pbiAssoc = 'assets/manuals/PowerBI_Associate.pdf';
  pbiVendor = 'assets/manuals/PowerBI_Vendor.pdf'
  psrui = environment.psrui;
  loginUser: string;
  isMessage: boolean = false;
  roleName: string;
  isRoleAvailable: boolean = false;
  constructor(private Auth: AuthService, private router: Router, public toastr: ToastrManager) { }

  ngOnInit() {
    this.loginUser = localStorage.getItem('loginUserId');
    this.isLoggedIn$ = this.Auth.getLoggedIn();
    if (this.isLoggedIn$) {
      this.roleName = localStorage.getItem('roleName');
            if (this.roleName !== undefined && this.roleName !== 'undefined' && this.roleName !== null && this.roleName !== 'null') {
                this.isRoleAvailable = true;
            }
    }
  }

  logout(): void {
    let savedSearch = localStorage.getItem('savedSearch');
    const result = this.Auth.logout(localStorage.getItem('authentication'), 'logout');
    result.subscribe(
      data => {
        console.log(' data --> ' + data.message);
        localStorage.clear();
        localStorage.setItem('savedSearch', savedSearch);
        window.location.href = environment.login;
      },
      err => {
        console.log("error occured in logout" + err);
        localStorage.clear();
        localStorage.setItem('savedSearch', savedSearch);
        window.location.href = environment.login;
      });
    //localStorage.clear();
    // window.location.href = environment.login;
  }

  // redirectToLogin(data: any): any {   
  // this.isMessage = true;
  // this.message = data.message;
  // this.toastr.warningToastr(data.message,null,{dismiss:'click',showCloseButton:true });

  // localStorage.clear();
  // window.location.href = environment.login;
  // }

  refresh() {
    window.location.reload();
  }
  showManualPopup: boolean = false;
  showManual() {
    this.showManualPopup = true;
  }
}
