import {
    Component, OnInit, ViewChild, Input, Output, EventEmitter, AfterViewInit,
    AfterContentInit
} from '@angular/core';
import * as $ from 'jquery';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthService } from '../../auth.service';
import { PsrService } from '../../_psr/staticpsr/psr.service';
import { PsrReportsComponent } from '../psr-reports/psr-reports.component';
import { Router } from '@angular/router';
import { StaticpsrComponent } from '../../_psr/staticpsr/staticpsr.component';



@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css'],

})
export class SidebarComponent implements OnInit, AfterContentInit {
    linkClick = '';
    isLoggedIn$: Observable<boolean>;
    isRoleAvailable: boolean = false;
    roleName: string;
    staticpsrComp = new StaticpsrComponent();
    constructor(private psrService: PsrService, private Auth: AuthService, private router: Router) { }

    public ngOnInit() {

        this.isLoggedIn$ = this.Auth.getLoggedIn();
        if (this.isLoggedIn$) {
            this.roleName = localStorage.getItem('roleName');
            if (this.roleName !== undefined && this.roleName !== 'undefined' && this.roleName !== null && this.roleName !== 'null') {
                this.isRoleAvailable = true;
            }
        }
    }

    ngAfterContentInit() {

        $(function () {

            $('.cmn-lft-drp-mn').mouseenter(function () {
                $(this).children().find('i').css('color', 'white');
            });
            $('.cmn-lft-drp-mn').mouseleave(function () {
                $(this).children().find('i').css('color', '#525672');
            });

            $('.submenue,.cmn-lft-drp-mn').mouseenter(function () {

            });

        });


    }

    setActiveClass(str: string): void {
        this.linkClick = str;
    }
    onReport(str: string, url: string): void {
        this.linkClick = str;
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
            this.router.navigate([url]));
    };
    hideSubMenu(): void {
        const t = document.getElementsByClassName('submenue-child');
        for (let i = 0; i < t.length; i++) {
            // t[i].style.display = 'none';
        }
    };
    showSubMenu(event): void {
        event.target.nextSibling.style.display = 'block';
    };
    changePsrWidth(): void {
        this.psrService.changeWidth();
    };
    sidebarExpColl(): void {
        $('.bar-dash').prev().toggle();
        $('.tb-vw-cmn-cls').toggleClass('tb-vw-cmn-cls-new');
        $('.sidebar-container').toggleClass('slide-container');
        $('.bar-dash').toggleClass('slide-mode');
        $('.submenue').toggleClass('hv-sub-menue');

        if ($('.submenue').hasClass('hv-sub-menue') === true) {
            $('.angledowncss').removeClass('angledowncss');
            $('.submenue-child').hide();
            $(".submenue-child-psr-nw").hide();
        }
        $('.content').toggleClass('reduce-margin-left');
        this.staticpsrComp.fullWidthCall();
    };
    showChildMenu(event): void {
        const target = $(event.target);
        $('.addbackgroundcolor').removeClass('addbackgroundcolor');
        $('.addcolorawe').removeClass('addcolorawe');
        $('.licommonclass').removeClass('licommonclass');
        $('.submenue-child').hide(200);
        $('.angledowncss').removeClass('angledowncss');
        $(".submenue-child-psr-nw").hide();
        if ($(target).next().hasClass('submenue-child')) {
            $(target).parent().toggleClass('licommonclass');
            $(target).toggleClass('addbackgroundcolor');
            $(target).children().find('.fa-angle-down').toggleClass('angledowncss');
        } else {
            $(target).toggleClass('addbackgroundcolor');
        }
        $(target).children().find('i').toggleClass('addcolorawe');
        $(target).next().toggle(200);

    };
    showChildMenunew(event): void {
        $(".submenue-child-psr-nw").toggle(200);
    }
    showChildMenuSmallSidebar(event): void {
        const target = $(event.target);
        $('.submenue-child-new').removeClass('submenue-child-new');
        if ($('.submenue').hasClass('hv-sub-menue') === true) {
            $(target).next().addClass('submenue-child-new');
        }

        $('.submenue-child-new').on('mouseleave', function () {
            $(target).removeClass('submenue-child-new');
        });
    }

}
