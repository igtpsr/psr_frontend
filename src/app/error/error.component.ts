import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { NgProgress } from '@ngx-progressbar/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  backendAvailable: string;

  constructor(private Auth: AuthService, private router: Router, public toastr: ToastrManager, public progress?: NgProgress) {
	  
    // this.Auth.isBackEndLive().subscribe(
    //   data => {
    //     console.log(' data '+data[0]);
    //     if (data === 'success') {
    //       this.router.navigate(['/login']);
    //     }
    //   }, error => {
    //     if (error === 0) {
    //       this.router.navigate(['/error']);
    //     }
    //   });
  }

  ngOnInit() {
    this.backendAvailable = 'Backend is not available, please contact administrator..!';
  }

}
