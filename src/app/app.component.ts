import { Component, ViewChild, OnInit, ElementRef, HostListener, OnDestroy } from '@angular/core';
import { Idle, DEFAULT_INTERRUPTSOURCES, EventTargetInterruptSource } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { Router, NavigationStart } from '@angular/router';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { HeaderComponent } from './_layout/header/header.component';
import { Observable, Subscription } from 'rxjs';
import 'rxjs/add/observable/timer';
import { ApiService } from './api.service';
import * as uuid from 'uuid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  idleState = '';
  timedOut = false;
  lastPing?: Date = null;
  isNetwork: boolean = false;
  isJDBCNetwork: boolean = false;
  showDialog: boolean = false;
  sessionEnded: boolean = false;
  configLoaded = false;
  countDown: any;
  public observe: any;
  observeCounter: number = 20;
  tick = 1000;
  isSubscribed: boolean = false;

  constructor(private idle: Idle, private keepalive: Keepalive, private router: Router, private Auth: AuthService, private element: ElementRef, private header: HeaderComponent, private api: ApiService) {

  // console.log = function () { };
  }

  state: boolean = false;
  ngOnInit() {
    setInterval(() => {
      if (localStorage.getItem('authentication') === null) {
        if (!window.location.href.includes(environment.login) && !window.location.href.includes(environment.error)) {
          localStorage.clear();
          window.location.href = environment.login;
        }
      }
    }, 1000);

    // const UrlSubString = window.location.href.substring(0, window.location.href.indexOf(environment.login));

    // if (window.location.href !== UrlSubString + environment.login) {

      if (localStorage.getItem('authentication')) {
        // to check token is available in database
      this.api.checkTokenAvailable().subscribe((response) => {
        console.log(response);
        if(response===false){
          localStorage.clear();
          window.location.href = environment.login;
        }
      },
        error => {
          console.log(error);
        })
        
        this.connect(); // websocket connecting

        this.idle.setIdle(60); // sets an idle timeout of 60 secs.
        this.idle.setTimeout(1620); // sets a timeout period of 30 mins.
        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
        this.idle.onIdleStart.subscribe(() => {
          this.idleState = '';
          this.state = true;
          console.log("Idle time started");
        });
        this.idle.onIdleEnd.subscribe(() => {
          this.idleState = '';
          console.log("Idle time ended");
          this.state = false;
        });



        this.idle.onTimeout.subscribe(() => {
          this.observe = Observable.timer(0, this.tick)
            .take(this.observeCounter)
            .map(() => --this.observeCounter).subscribe(res => {
              this.isSubscribed = true;
              this.countDown = res;
              console.log(this.countDown);
              this.showDialog = true;
              if (this.countDown == 0) {
                this.idleState = '';
                this.timedOut = true;
                this.doLogout();
              }
            })
        });

        // sets the ping interval to 15 seconds
        // keepalive.interval(45);
        // keepalive.request(environment.apiUrl + '/keepAlive?authentication=' + localStorage.getItem('authentication'));
        // keepalive.onPing.subscribe(() => this.lastPing = new Date());
        // this.reset();
      //}
    }
  }



  reset() {
    this.idle.watch();
    this.idleState = '';
    this.timedOut = false;
    this.showDialog = false;
    this.state = false;
    if (this.isSubscribed) {
      this.observe.unsubscribe();
      this.observeCounter = 20;
      this.isSubscribed = false;
    }
    console.log("resetted");
  }

  //  Web socket with sockJS for realtime response
  socket: any;
  stompClient: any;
  ws: any;

  connect() {
    //  console.log()
    //connect to stomp where stomp endpoint is exposed
    this.socket = new SockJS(environment.apiUrl + "/psrSocket", {});
    this.ws = Stomp.over(this.socket);
    this.stompClient = this.ws;
    this.stompClient.connect(
      {},
      (success) => this.connectSuccessCallback(),
      (error) => this.connectErrorCallback(error)
    );
  }
  // successfull socket connect callback function 
  connectSuccessCallback() {
    this.isNetwork = false;
    AppComponent.counter = 0;
    this.lastPing = new Date();
    this.reset();
    const myId = uuid.v4();
    localStorage.setItem('uniqueId', myId+localStorage.getItem('authentication'));
    this.updateSessionTime();
    setInterval(() => this.updateSessionTime(), 30000);
    // to logout if session ended
    this.stompClient.subscribe('/queue/logout/'+localStorage.getItem('uniqueId'), (res) => {
      if (res) {
        this.showDialog = false;
        this.sessionEnded = true;
        setTimeout(() => this.doLogout(), 5000);
      }
    });
    // to show network error
    this.stompClient.subscribe('/queue/network/'+localStorage.getItem('uniqueId'), (res) => {
      if (res) {
       this.isJDBCNetwork = true;
      }
    })
  }
  // Error while connecting webscoket connection callback 
  static counter: number = 0;
  connectErrorCallback(error) {
    console.log(' SockJS error', error);
    AppComponent.counter++;
    console.log(AppComponent.counter++)
    if (AppComponent.counter > 24) {
      this.isNetwork = true;
      AppComponent.counter = 0;
      this.connect();
      // this.doLogout();
      // window.location.href = environment.error;
    }
    else {
      setTimeout(() => {
        console.log('reconnecting socket ...');
        this.connect();
      }, 5000);
    }
  }

  updateSessionTime() {
      if (localStorage.getItem('authentication')) {
        this.stompClient.send("/psr/rest/updateSession", {}, JSON.stringify({ tokenId: localStorage.getItem('authentication'), uniqueId: localStorage.getItem('uniqueId') }));
      }
  }
  doLogout() {
    this.header.logout();
    this.stompClient.unsubscribe('/queue/logout/'+localStorage.getItem('uniqueId'));
    this.stompClient.disconnect();
  }

  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event) {
    console.log($event);
    if (localStorage.getItem('authentication')) {
      this.api.sendBrowserCloseStatus();
    }
  }

}
