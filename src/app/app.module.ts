﻿import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { MatDialogModule, MatCardModule, MatButtonModule } from '@angular/material';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { jqxListBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxlistbox';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput';
import { AppComponent } from './app.component';
import { SidebarComponent } from './_layout/sidebar/sidebar.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './_layout/header/header.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { StaticpsrComponent } from './_psr/staticpsr/staticpsr.component';
import { ToastrModule } from 'ng6-toastr-notifications';
import { NgProgressModule } from '@ngx-progressbar/core';
import { PagerService } from './pager.service';;
import { WelcomeComponent } from './welcome/welcome.component'
import { ErrorComponent } from './error/error.component';
import { ConfigService } from './config.service';
import { of, Observable, ObservableInput } from '../../node_modules/rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../environments/environment';
//CHG0016311 Rupan Start
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
//CHG0016311 Rupan End
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment';
import { TranslateModule } from 'ng2-translate';
import { PsrReportsComponent } from './_layout/psr-reports/psr-reports.component';
import {SharedModule} from './shared.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';




export function load(http: HttpClient, config: ConfigService): (() => Promise<boolean>) {
  return (): Promise<boolean> => {
    return new Promise<boolean>((resolve: (a: boolean) => void): void => {
      var dmode = 'dev';
      if (environment.production) {
        dmode = 'prod';
      }
      http.get('./assets/config/' + dmode + '.config.json')
        .pipe(
          map((x: ConfigService) => {

            config.apiUrl = x.apiUrl;
            config.imagePath = x.imagePath;
            config.excelPath = x.excelPath;
            config.excelDownloadPath = x.excelDownloadPath;
            config.psrui = x.psrui;
            config.baseUrl = x.baseUrl;
            environment.apiUrl = x.apiUrl;
            environment.imagePath = x.imagePath;
            environment.excelPath = x.excelPath;
            environment.excelDownloadPath = x.excelDownloadPath;
            environment.psrui = x.psrui;
            environment.login = x.login;
            resolve(true);
          }),
          catchError((x: { status: number }, caught: Observable<void>): ObservableInput<{}> => {
            if (x.status !== 404) {
              resolve(false);
            }

            config.apiUrl = environment.apiUrl;
            config.imagePath = environment.imagePath;
            config.excelPath = environment.excelPath;
            config.excelDownloadPath = environment.excelDownloadPath;
            config.psrui = environment.psrui;
            config.login = environment.login;
            config.baseUrl = 'devbaseurl';
            resolve(true);
            return of({});
          })
        ).subscribe();
    });
  };
}
@NgModule({
  declarations: [
    AppComponent, 
    SidebarComponent, 
    DashboardComponent, 
    jqxListBoxComponent, 
    HeaderComponent,
     jqxDropDownListComponent, 
     jqxDateTimeInputComponent,  
    LoginComponent, 
    WelcomeComponent,
    ErrorComponent, 
    StaticpsrComponent,
    PsrReportsComponent
  ], 
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // SelectModule,
    ReactiveFormsModule,
    CommonModule,
    AngularFontAwesomeModule,
    AppRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
   // NgSelectModule,
    // MatExpansionModule,
    MatIconModule,
    MatTableModule,
    ToastrModule.forRoot(),
    NgProgressModule.forRoot(),
    // MyDatePickerModule,
    MomentModule,
    NgIdleKeepaliveModule.forRoot(),
    TranslateModule.forRoot(),
    // CHG0016311 Rupan Start
    DateRangePickerModule
    //CHG0016311 Rupan End
    ,SharedModule,
    InfiniteScrollModule
  ],
  providers: [AuthService, AuthGuard, PagerService,HeaderComponent,
    {
      provide: APP_INITIALIZER,
      useFactory: load,
      deps: [HttpClient,
        ConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
