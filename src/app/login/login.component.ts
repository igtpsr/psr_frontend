import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { environment } from '../../environments/environment';
import { NgProgress } from '@ngx-progressbar/core';
import { log } from 'util';
import { ApiService } from '../api.service';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loading = false;
  buttonStatus: boolean;
  submitted = false;
  isMessage = false;
  message: string;
  returnUrl: string;
  model: any = {};
  currLogged  = false;
  // logoSrc = 'assets/images/logo.jpg';
  logoSrc = environment.imagePath + '/logo.jpg';

  constructor(private Auth: AuthService,private api: ApiService, private router: Router, public toastr: ToastrManager, private http: HttpClient, public progress?: NgProgress) {
    this.readTextFile();
    this.Auth.isBackEndLive().subscribe(
      data => {

      }, error => {
        if (error === 'backend not available' ) {
          console.log(' error --> '+error);
          this.router.navigate(['/error']);
        }
      });
  }

  ngOnInit() {
    this.Auth.getLoggedIn().subscribe(data =>  this.currLogged = data  );
    if (this.currLogged) {
      this.router.navigateByUrl('/psr');
    }

    // this.user.is_data = false;
  }

  // convenience getter for easy access to form fields
  // get f() { return this.loginForm.controls; }

  onSubmit(event): void {
    event.preventDefault();
    const username = event.target.querySelector('#username').value;
    const password = event.target.querySelector('#password').value;
    let flag = false;
    if (username === undefined || username === '') {
      event.target.querySelector('#username').nextSibling.style.display = 'block';
      flag = true;
    }
    if (password === undefined || password === '') {
      event.target.querySelector('#password').nextSibling.style.display = 'block';
      flag = true;
    }
    if (flag) {
      return;
    }
    this.progress.start();
    this.buttonStatus = true;
    const nextTimeStamp = (new Date()).getTime() + (30 * 60000);
    // const authString = username + ":" + password + ":" + (new Date()).getTime();
    // const authStringEnc = btoa(authString);
    const result = this.Auth.getUserDetails(username, password);

    result.subscribe(
      data => {
       // console.log(' data --> ' + data);
        if (data.type === 'success') {
          this.setUserAttributes(data);          
          if (data.roleName !== undefined || data.roleName !== '' || data.roleName !== null || data.roleName !== 'PTL_HOME' ) {
            window.location.href = environment.psrui;
            this.progress.complete();
          } 
        } else if(data.type === 'NoRole') {
            this.setUserAttributes(data);
            window.location.href = "/welcome";
            this.progress.complete();
        } else {
          localStorage.clear();
          this.isMessage = true;
          this.buttonStatus = false;
          if(data.message !== undefined && data.message !== null && data.message !== '' && data.message !== 'null' && data.message !== 'undefined') {
            this.message = data.message;
          } else {
            this.message = 'Username / Password invalid!';
          }          
          this.toastr.errorToastr(this.message,'Oops!',{dismiss:'click',showCloseButton:true });
          this.progress.complete();
        }
      });
  }
  
  setUserAttributes(data): any {
    this.Auth.setLoggedIn(true);
    localStorage.setItem('brUserId', data.brUserId);
    localStorage.setItem('loginUserId', data.loginUserId);
    localStorage.setItem('authentication', data.tokenId);
    localStorage.setItem('roleName', data.roleName);
    localStorage.setItem('queryName', data.queryName);
    localStorage.setItem('queryId', data.queryId);
    localStorage.setItem('milestone', data.milestone);
    localStorage.setItem('gridRowsLimit', data.gridRowsLimit);
  }

  readTextFile(): void {
    this.http.get('assets/test.txt', { responseType: 'text' })
      .subscribe(data => {
        document.getElementById("maintenance").innerHTML = data;
      });
  }
  
}
