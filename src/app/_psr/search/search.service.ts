import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SearchForm} from './search-form';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  _url = environment.apiUrl;
  constructor(private _http: HttpClient) {}
  search(searchform: SearchForm) {
    return this._http.post<any>(this._url+"/search", searchform);
  }
}
