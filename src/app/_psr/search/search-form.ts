export class SearchForm {
  constructor(
    public vpo: string,
    public vendorId: string,
    public ocCommit: string,
    public factoryId: string,
    public factoryName: string,
    public brand: string,
    public style: string,
    public styleDescription: string,
    public productionOffice: string,
    public countryOrigin: string,
   
    public cpo: string,
    public dept: string,
    public season: string,
    public colorWashName: string,
    public ndcdate: string,
    public vpoNltGacDate: string,
    public vpoFtyGacDate: string,
    public ndcWk: string,
    public vpoStatus: string,
    public vpoLineStatus: string,
    public category: string,
    public cpoColorDesc: string,
    public cpoDept: string,
    public initFlow: string,

    public vpoSelect: string,
    public vendorIdSelect: string,
    public ocCommitSelect: string,
    public factoryIdSelect: string,
    public factoryNameSelect: string,
    public brandSelect: string,
    public styleSelect: string,
    public styleDescSelect: string,
    public prodOfficeSelect: string,
    public countryOriginSelect: string,
    public milestoneselect: string,
   
    public cpoSelect: string,
    public deptSelect: string,
    public seasonSelect: string,
    public colorWashNameSelect: string,
    public ndcdateSelect: string,
    public vpoNltGacDateSelect: string,
    public vpoFtyGacDateSelect: string,
    public ndcWkSelect: string,
    public vpoStatusSelect: string,
    public vpoLineStatusSelect: string,
    public categorySelect: string,
    public cpoColorDescSelect: string,
    public cpoDeptSelect: string,
    public initFlowSelect: string
  ) {}
}
