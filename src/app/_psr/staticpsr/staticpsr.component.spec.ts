import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaticpsrComponent } from './staticpsr.component';

describe('StaticpsrComponent', () => {
  let component: StaticpsrComponent;
  let fixture: ComponentFixture<StaticpsrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticpsrComponent ]
    })
    .compileComponents();
  }));
 
  beforeEach(() => {
    fixture = TestBed.createComponent(StaticpsrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
