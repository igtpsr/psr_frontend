import { TestBed, inject } from '@angular/core/testing';

import { PsrService } from './psr.service';

describe('PsrService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PsrService]
    });
  });

  it('should be created', inject([PsrService], (service: PsrService) => {
    expect(service).toBeTruthy();
  }));
});
