import { AfterViewInit, Component, OnInit, ViewChild, ElementRef, AfterViewChecked, AfterContentInit, OnDestroy, NgZone } from '@angular/core';
import * as XLSX from 'xlsx';
// import { NgForm, FormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SearchForm } from '../search/search-form';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxListBoxComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxlistbox';
// import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { ApiService } from '../../api.service';
import { SearchService } from '../search/search.service';
// import { DISABLED } from '../../../node_modules/@angular/forms/src/model';
import { ToastrManager } from 'ng6-toastr-notifications';
// import { forEach } from '@angular/router/src/utils/collection';
import { NgProgress } from '@ngx-progressbar/core';
import { Globals } from '../../Globals';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { PagerService } from '../../pager.service';
import { Router } from '@angular/router';
import { IMyDpOptions } from 'mydatepicker';
import { ExcelService } from '../../excel/excel.service';
import { searchFields } from './searchFilelds';
import * as moment from 'moment';
const EXCEL_EXTENSION = '.xlsm';
type AOA = any[][];


declare var window: any;
declare var increment_counter: any;
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'authentication': localStorage.getItem('authentication') })
};
@Component({
    selector: 'app-staticpsr',
    templateUrl: './staticpsr.component.html',
    styleUrls: ['./staticpsr.component.css'],
    providers: [Globals]
})
export class StaticpsrComponent implements OnInit, OnDestroy, AfterViewChecked {
    null_increment_counter() {
        new increment_counter();
    }
    roleName = localStorage.getItem('roleName');
    searchFormModel = new SearchForm('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '','');
    query: any = {};
    @ViewChild('eventLog') eventLog: ElementRef;
    @ViewChild('psrGrid') public psrGrid: jqxGridComponent;
    @ViewChild('psrCheckBox') public psrCheckBox: jqxListBoxComponent;
    showDialog: boolean = false;
    filterArr = {
        'vendor-id': '', 'vendor-name': '', 'factory-id': '', 'factory-name': '', 'brand': '', 'style': '', 'style-description': '',
        'vpo': '', 'production-office': '', 'country-origin': ''
    };
    gridFilterName = {
        'vendor-id': 'vendorId', 'vendor-name': 'vendorName', 'factory-id': 'factoryId', 'factory-name': 'factoryName',
        'brand': 'brand', 'style': 'style', 'style-description': 'styleDesc',
        'vpo': 'vpo', 'production-office': 'prodOffice', 'country-origin': 'countryOrigin'
    };
    saveData = false;
    userId: string;
    oldvalue: any;
    psrRes: any;
    psrExcelDataResponse: any;
    books: any;
    data1: any;
    selectedCheckbox: any[] = [];
    showPopup: boolean = false;
    showFileTrackPopup: boolean = false;
    changeTrackList: any[];
    fileChangeTrackList: any[];
    public tracking: Map<string, any> = new Map<string, any>();
    changeModelId = [];
    editedRowPsrId = [];
    getmilestonedata = [];
    invalidCell = [];
    publicviewdropdown: any[] = [];
    getCellValueChanged: any;
    gridStatus: boolean = false;
    availableViewlablelist = [];
    availableViewidlist = []
    checkpublicviewstatus = [];
    availableviewid: any;
    StatusPublicView: boolean = false;
    hideUpdateDelete: boolean = false;
    editedMilestoneField = [];
    flagCheck: boolean = false;
    showHideHeader: boolean = false;
    isIE = /msie\s|trident\//i.test(window.navigator.userAgent);
    scrollList = [];
    fileUploadModelInput: any;
    x: any = [];
    dataExcel: AOA = [];
    wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
    excelUivaliPopoup:boolean = false;
    macroSrc = environment.imagePath + '/Screenshot_1.png';
    smartTagSrc = environment.smartTag;
    showSmartPopup:boolean = false;
    showSmartDetails:any;
    //CHG0016311 Rupan Start
    ndcDateValue: Date[];
    vpoNltGacDateValue: Date[];
    vpoFtyGacDateValue: Date[];
    //CHG0016311 Rupan End


    // page total items
    private allItems = [];
    private cndcdate = null;
    private cvpoNltGacDate = null;
    private cvpoFtyGacDate = null;
    // pager object
    pager: any = {};
    panelOpenState = true;
    checkedvalue = 'checked';
    
    searchFields = new searchFields();
    public myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'mm/dd/yyyy',
        inline: false,
        height: '17px',
        width: '165px',
        editableDateField: false,
    };
    public changeTrackDatePickerOptions: IMyDpOptions = {
        dateFormat: 'mm-dd-yyyy',
        inline: false,
        height: '17px',
        width: '165px',
        editableDateField: false,
    };
    // paged items
    modelOperators = this.searchFields.modelOperators;
    modelOperatorsfordate = this.searchFields.modelOperatorsfordate;
    modelOperatorsforstatus = this.searchFields.modelOperatorsforstatus;

    pagedItems: any[];
    currentPage = 1;
    isValidCheck: boolean = true;
    isValid: boolean = true;
    serachWidth = null;
    jsonData: any[] = [];
    availableViews = [];
    columngroups1: any[];
    columngroups: any[] = [];

    rowValues = new Array();
    mainSheetName:string;
    status:string = 'All';
    listBoxStatus: string[] = ['All', 'Visible', 'Hidden'];
    holdCheckedColumns = [];
    holdUncheckedColumns = [];
    secondScrollList:any[] = [];
    reorderFlag:boolean = false;

    datafields: any[] = [
        { name: 'select', text: 'Select', type: 'bool' },
        { name: 'vpo', text: 'Vpo #', type: 'string' },
        { name: 'image', text: 'Image', type: 'File' },
        { name: 'initFlow', text: 'Init Flow', type: 'string' },
        { name: 'colorWashName', text: 'Color Wash Name', type: 'string' },
        // { name: 'highlightRevisedDate', type: 'bool' },
        { name: 'shipTo', text: 'Ship To', type: 'string' },
        { name: 'soId', text: 'So Id', type: 'BigDecimal' },
        { name: 'prodOffice', text: 'Production Office', type: 'string' },
        { name: 'vendorId', text: 'Vendor Id', type: 'string' },
        { name: 'vendorName', text: 'Vendor Name', type: 'string' },
        { name: 'factoryId', text: 'Factory Id', type: 'string' },
        { name: 'factoryName', text: 'Factory Name', type: 'string' },
        { name: 'countryOrigin', text: 'Country Origin', type: 'string' },
        { name: 'brand', text: 'Brand', type: 'string' },
        { name: 'dept', text: 'Dept', type: 'string' },
        { name: 'category', text: 'Category', type: 'string' },
        { name: 'merchant', text: 'Merchant', type: 'string' },
        { name: 'style', text: 'Style #', type: 'string' },
        { name: 'styleDesc', text: 'Style Desc', type: 'string' },
        { name: 'fob', text: 'Fob', type: 'BigDecimal' },
        { name: 'grossSellPrice', text: 'Gross Sell Price', type: 'BigDecimal' },
        { name: 'margin', text: 'Margin', type: 'BigDecimal' },
        { name: 'productType', text: 'Product Type', type: 'string' },
        { name: 'programName', text: 'Program Name', type: 'string' },
        { name: 'shipMode', text: 'Ship Mode', type: 'string' },
        { name: 'gender', text: 'Gender', type: 'string' },
        { name: 'orderQty', text: 'Order Qty', type: 'int' },
        { name: 'shippedQty', text: 'Shipped Qty', type: 'int' },
        { name: 'ocCommit', text: 'Order Commit #', type: 'string' },
        { name: 'cpo', text: 'Cpo', type: 'string' },
        { name: 'season', text: 'Season', type: 'string' },
        { name: 'ndcDate', text: 'Ndc Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'vpoNltGacDate', text: 'Vpo Nlt Gac Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'vpoFtyGacDate', text: 'Vpo Fty Gac Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'keyItem', text: 'Key Item', type: 'string' },
        { name: 'vpoStatus', text: 'Vpo Status', type: 'string' },
        { name: 'vpoLineStatus', text: 'Vpo Line Status', type: 'string' },
        { name: 'interfaceTs', text: 'Interface Ts', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'cpoOrderQty', text: 'Cpo Order Qty', type: 'int' },
        { name: 'cpoShipMode', text: 'Cpo Ship Mode', type: 'string' },
        { name: 'cpoColorDesc', text: 'Cpo Color Desc', type: 'string' },
        { name: 'cpoPackType', text: 'Cpo Pack Type', type: 'string', length: 10 },
        { name: 'modifyUser', text: 'Modify User', type: 'string' },
        { name: 'actualPoundageAvailable', text: 'Actual Poundage Available', type: 'BigDecimal', length: 10 },
        { name: 'apprFabricYarnLot', text: 'Appr Fabric Yarn Lot', type: 'string', length: 20 },
        { name: 'asn', text: 'ANF ASN #', type: 'string' },
        { name: 'balanceShip', text: 'Balance Ship', type: 'int' },
        { name: 'careLabelCode', text: 'Care Label Code', type: 'string', length: 10 },
        { name: 'colorCodePatrn', text: 'Color Code Pattern', type: 'string', length: 20 },
        { name: 'comment', text: 'Comment', type: 'string', length: 1000 },
        { name: 'createDate', text: 'Create Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'custFtyId', text: 'Cust Fty Id', type: 'string', length: 20 },
        { name: 'cutApprLetter', text: 'Cut Appr Letter', type: 'string', length: 20 },
        { name: 'dailyOutputPerLine', text: 'Daily Output Per Line', type: 'int', length: 6 },
        { name: 'extraProcess', text: 'Extra Process', type: 'bool' },
        { name: 'fabTrimOrder', text: 'Fab Trim Order', type: 'string', length: 20 },
        { name: 'fabricCode', text: 'Fabric Code', type: 'string', length: 50 },
        { name: 'fabricEstDate', text: 'Fabric Est Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'fabricMill', text: 'Fabric Mill', type: 'string', length: 80 },
        { name: 'fabricTestReportNumeric', text: 'Fabric Test Report#', type: 'string', length: 20 },
        { name: 'factoryRef', text: 'Factory Ref', type: 'string', length: 20 },
        { name: 'fiberContent', text: 'Fiber Content', type: 'string', length: 80 },
        { name: 'plActualGac', text: 'Pl Actual Gac', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'cpoGacDate', text: 'Cpo Gac Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'garmentTestActualStr', text: 'Garment Test Actual', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'gauge', text: 'Gauge', type: 'string', length: 20 },
        { name: 'graphicName', text: 'Artwork name', type: 'string', length: 50 },
        { name: 'knitting', text: 'Knitting %', type: 'BigDecimal', length: 10 },
        { name: 'knittingMachines', text: '# of Knitting Machines', type: 'int', length: 6 },
        { name: 'knittingPcs', text: 'Knitting Pcs', type: 'int', length: 6 },
        { name: 'linking', text: 'Linking %', type: 'BigDecimal', length: 10 },
        { name: 'linkingMachines', text: '# of Linking Machines', type: 'int', length: 6 },
        { name: 'linkingPcs', text: 'Linking Pcs', type: 'int', length: 6 },
        { name: 'ndcWk', text: 'Ndc Wk', type: 'string', length: 10 },
        { name: 'orderType', text: 'Sell Channel', type: 'string', length: 20 },
        { name: 'packType', text: 'Pack Type', type: 'string', length: 10 },
        { name: 'packagingReadyDateStr', text: 'Packaging Ready Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'salesAmount', text: 'Sales Amount', type: 'BigDecimal' },
        { name: 'sewingLines', text: '# of Sewing Lines', type: 'int', length: 6 },
        { name: 'ticketColorCode', text: 'Ticket Color Code', type: 'string', length: 20 },
        { name: 'ticketStyleNumeric', text: 'Ticket Style Number', type: 'string', length: 20 },
        { name: 'profomaPo', text: 'Profoma Po', type: 'string' },
        { name: 'vpoFtyGacDateTemp', text: 'Vpo Fty Gac Date Temp', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'vpoHeadStatus', type: 'string' },
        { name: 'psrOrderId', text: 'Psr Order Id', type: 'int' },
        { name: 'planId', text: 'Plan Id', type: 'string' },
        { name: 'vpoOrderType', text: 'VPO Order Type', type: 'string' },
        { name: 'speedIndicator', text: 'VPO Speed Indicator', type: 'string' },
        { name: 'garmentTestReportNo', text: 'Garment Test Report#', type: 'string', length: 20 },
        { name: 'plShipDate', text: 'Ship Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'cpoStyleNo', text: 'CPO Style No', type: 'string' },
        { name: 'requestNo', text: 'Request Number', type: 'string' },
        { name: 'ladingPoint', text: 'Port of Origin', type: 'string' },
        { name: 'techDesigner', text: 'Fit Owner', type: 'string' },
        { name: 'plmNo', text: 'Design Card Number/PLM Number', type: 'string', length: 35 },
        { name: 'washYn', text: 'Wash', type: 'bool' },
        { name: 'washFacility', text: 'Wash Facility', type: 'string', length: 80 },
        { name: 'fabricShipMode', text: 'Fabric Ship Mode', type: 'string', length: 35 },
        { name: 'techPackReceivedDateStr', text: 'Tech Pack Received Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'sampleMaterialStatus', text: 'Sample Material Status', type: 'string', length: 35 },
        { name: 'retailPrice', text: 'Retail Price', type: 'BigDecimal', length: 30 },
        { name: 'fabricPpDateStr', text: 'Fabric Pp Date', type: 'date', format: 'yyyy-MM-dd' },
//        { name: 'deliveryMonth', text: 'Delivery Month', type: 'string', length: 12 },
        { name: 'deliveryMonth', text: 'Delivery Month', type: 'string', length: 24 },
        { name: 'cpoAccDateByVendorStr', text: 'Cpo Acc Date By Vendor', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'distChannel', text: 'Dist Channel', type: 'string', length: 20 },
        { name: 'floorSet', text: 'Floor Set', type: 'bool' },
        { name: 'styleAtRisk', text: 'Style At Risk', type: 'bool' },
        { name: 'sampleMerchEtaStr', text: 'Sample Merch Eta', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'sampleFloorSetEtaStr', text: 'Sample Floor Set Eta', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'sampleDcomEtaStr', text: 'Sample Dcom Eta', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'sampleMailer', text: 'Sample Mailer', type: 'bool' },
        { name: 'sampleMailerEtaStr', text: 'Sample Mailer Eta', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'topSampleEtaDateStr', text: 'TOP sample ETA Date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'topSampleComment', text: 'TOP sample Comment', type: 'string', length: 1000 },
        { name: 'photoMerchantSampleSendDateStr', text: 'Photo/Merchant Sample Send date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'photoMerchantSampleEtaDateStr', text: 'Photo/Merchant Sample ETA date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'photoMerchantSampleComment', text: 'Photo/Merchant Sample Comment', type: 'string', length: 50 },
        { name: 'marketingSampleSendDateStr', text: 'Marketing Sample Send date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'marketingSampleEtaDateStr', text: 'Marketing Sample ETA date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'marketingSampleComment', text: 'Marketing Sample Comment', type: 'string', length: 50 },
        { name: 'visualSampleSendDateStr', text: 'Visual Sample Send date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'visualSampleEtaDateStr', text: 'Visual Sample ETA date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'visualSampleComment', text: 'Visual Sample Comment', type: 'string', length: 50 },
        { name: 'copyrightSampleSendDateStr', text: 'Copyright Sample Send date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'copyrightSampleEtaDateStr', text: 'Copyright Sample ETA date', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'copyrightSampleComment', text: 'Copyright Sample Comment', type: 'string', length: 50 },
        { name: 'reasonForLateGac', text: 'Reason For Late Gac', type: 'string', length: 50 },
        { name: 'cpoDept', text: 'CPO Dept', type: 'string' },
        { name: 'additionalBulkLotApproveStr', text: 'Additional Bulk Lot Approve', type: 'date', format: 'yyyy-MM-dd' },
        { name: 'colorComment', text: 'Color Comment', type: 'string', length: 1000 },
        { name: 'fabricComment', text: 'Fabric Comment', type: 'string', length: 1000 },
        { name: 'fabricTestComment', text: 'Fabric Test Comment', type: 'string', length: 1000 },
        { name: 'garmentTestComment', text: 'Garment Test Comment', type: 'string', length: 1000 },
        { name: 'trimComment', text: 'Trim Comment', type: 'string', length: 1000 },
        { name: 'sampleComment', text: 'Sample Comment', type: 'string', length: 1000 },
        { name: 'fitSampleComment', text: 'Fit Sample Comment', type: 'string', length: 1000 },
        { name: 'ppSampleComment', text: 'PP Sample Comment', type: 'string', length: 1000 },
        { name: 'garmentTreatmentPcs', text: 'Garment Treatment Pcs', type: 'int', length: 8 },
        { name: 'finishedGarmentPcs', text: 'Finished Garment Pcs', type: 'int', length: 8 },
        { name: 'packedUnitsPcs', text: 'Packed Units Pcs', type: 'int', length: 8 },
        
    ];

    ngOnInit() {
        window.uploadimagefile = window.uploadimagefile || {};
        window.uploadimagefile.namespace = window.uploadimagefile.namespace || {};
        window.uploadimagefile.namespace.uploadImageCallFunction = this.uploadImageCallFunction.bind(this);
        window.deleteimagefile = window.deleteimagefile || {};
        window.deleteimagefile.namespace = window.deleteimagefile.namespace || {};
        window.deleteimagefile.namespace.deleteImageCallFunction = this.deleteImageCallFunction.bind(this);

        
        /** Lookup Event Model */
        window.smartTagBr = window.smartTagBr || {};
        window.smartTagBr.namespace = window.smartTagBr.namespace || {};
        window.smartTagBr.namespace.smartTagFunction = this.smartTagFunction.bind(this);
        //    const print = this.model.check();

        this.searchFormModel.vpoStatus = 'VOID,CLOSED';
        this.searchFormModel.vpoLineStatus = 'VOID,CLOSED,CWOLIAB,CNCLIAB';


        this.autoPopulateViews();

        /* dynamic grid implementation starts  */
        // let t1 = this.api.getEventDescriptions(localStorage.getItem('loginUserId')).subscribe(
        //     columngroups => {
        let columngroups: any = localStorage.getItem('milestone').split(',');
        this.columngroups1 = columngroups;

        for (let index = 0; index < columngroups.length; index++) {
            this.getmilestonedata.push(columngroups[index]);
            // this.psrGrid.setcolumnproperty('name': columngroups[index] + 'PlannedDate', 'name': columngroups[index] + 'PlannedDate', 'date')
            this.datafields.push({ 'name': columngroups[index] + 'eventCode', 'type': 'string' });
            this.datafields.push({ 'name': columngroups[index] + 'PlannedDate', 'type': 'date', format: 'yyyy-MM-dd' });
            this.datafields.push({ 'name': columngroups[index] + 'RevisedDate', 'type': 'date', format: 'yyyy-MM-dd' });
            this.datafields.push({ 'name': columngroups[index] + 'ActualDate', 'type': 'date', format: 'yyyy-MM-dd' });
            this.datafields.push({ 'name': columngroups[index] + 'highlightRevisedDate', 'type': 'bool' });
            //new code
           this.datafields.push({ 'name': columngroups[index] + 'disableEvent', 'type': 'bool' });
            // this.columns.push({
            //     text: 'Event Code', datafield: columngroups[index] + 'eventCode',
            //     editable: false, columngroup: columngroups[index], hidden: true
            // });
            this.columns.push({
                text: 'Planned Date', datafield: columngroups[index] + 'PlannedDate', width: '98px',
                editable: false, columngroup: columngroups[index], cellsformat: 'MM/dd/yyyy', columntype: 'datetimeinput', cellclassname: this.cellclass,
                cellbeginedit: this.cellbeginedit
            });
            this.columns.push({
                text: 'Revised Date', datafield: columngroups[index] + 'RevisedDate', width: '97px',
                editable: true, columngroup: columngroups[index], cellsformat: 'MM/dd/yyyy', columntype: 'datetimeinput', cellclassname: this.cellclass, cellbeginedit: this.cellbeginedit
            });
            //if(this.roleName === 'PSR_VENDOR' ){
            if (columngroups[index].indexOf('GAC by Vendor') > -1) {
                this.columns.push({
                    text: 'Actual Date', datafield: columngroups[index] + 'ActualDate', width: '87px', editable: false, columngroup: columngroups[index], cellsformat: 'MM/dd/yyyy', columntype: 'datetimeinput', cellbeginedit: this.cellbeginedit
                });
            } else {
                this.columns.push({
                    text: 'Actual Date', datafield: columngroups[index] + 'ActualDate', width: '87px', editable: true, columngroup: columngroups[index], cellsformat: 'MM/dd/yyyy', columntype: 'datetimeinput', cellclassname: this.cellclass, cellbeginedit: this.cellbeginedit
                });
            }
            // this.columns.push({
            //     text: 'Highlight Revised Date', datafield: columngroups[index] + 'highlightRevisedDate', columngroup: columngroups[index], hidden: true
            // });
            this.columngroups.push({ text: columngroups[index], align: 'center', name: columngroups[index] });
        }
        // });
        /* dynamic grid implementation ends  */
       
        this.temporaryColumn = this.columns;
        // console.log(this.temporaryColumn);
    }
    temporaryColumn:any[] = [];

    ngOnDestroy() {
        window.uploadimagefile.namespace.uploadImageCallFunction = null;
    }

    ngOndeleteDestroy() {
        window.deleteimagefile.namespace.deleteImageCallFunction = null;
    }

    uploadImageCallFunction(psrorderid) {
        if (this.editedRowPsrId.indexOf(psrorderid) == -1) {
            this.editedRowPsrId.push(psrorderid);
        }
    }

    deleteImageCallFunction(flag) {
        if (flag == 'start') {
            this.progress.start();
        } else {
            this.progress.complete();
            this.toastr.successToastr('Deleted Successfully', 'Success!');
        }
    }


    prodCosting = [];
    techSpec = [];
    vpoDetails = [];

    smartTagFunction(row:number) {
       
        

            let requestNo = this.psrGrid.getcellvalue(row,'requestNo');
            let style = this.psrGrid.getcellvalue(row,'style');
            let colorWashName = this.psrGrid.getcellvalue(row,'colorWashName');
            let profomaPo = this.psrGrid.getcellvalue(row,'profomaPo');
         
            //this.psrGrid.setcellvalue(row,'select',true);

            this.progress.start();
            this.api.getSmartTagRecords(requestNo, style, colorWashName, profomaPo).subscribe(res=>{
                // console.log(res);
                this.showSmartPopup = true;
                let Details = res;
                this.techSpec = [];
                this.vpoDetails = [];
                this.prodCosting = [];
                for (const key in  Details) {
                  if(key == 'Fabrication Yarn') {
                    this.techSpec.push({[key]:Details[key]})
                  } else if(key == 'Internal Style Number' || key == 'Size Range' || key == 'Vpo Color Way') {
                    this.vpoDetails.push({[key]:Details[key]})
                  } else {

                    this.prodCosting.push({[key]:Details[key]})
                  }
                }


                //this.showSmartDetails = JSON.parse(this.showSmartDetails);
                this.progress.complete();
            })
    
       
        
    }

    cellclass = (row, columnfield, value, defaulthtml, columnproperties): string => {

        if (columnfield == 'GAC by VendorRevisedDate') {
            let eventCode = this.psrGrid.getcellvalue(row, 'GAC by VendoreventCode');
            if (eventCode !== undefined && eventCode !== null && eventCode !== '' && eventCode === 'E000') {
                if (this.psrGrid.getcellvalue(row, 'GAC by VendorhighlightRevisedDate') === true) {
                    return 'editable-cell-red';
                } else {
                    return 'editable-cell-bg';
                }
            }
        } else if (columnfield === 'vpoFtyGacDate') {
            let vpoFtyGacDate = this.psrGrid.getcellvalue(row, 'vpoFtyGacDate');
            let vpoFtyGacDateTemp = this.psrGrid.getcellvalue(row, 'vpoFtyGacDateTemp');
            if (vpoFtyGacDate !== undefined && vpoFtyGacDate !== null && vpoFtyGacDate !== '' && vpoFtyGacDateTemp !== undefined && vpoFtyGacDateTemp !== null && vpoFtyGacDateTemp !== '') {
                let ovalue = (("0" + (value.getMonth() + 1)).slice(-2)) + '/' + (("0" + value.getDate()).slice(-2)) + '/' + value.getFullYear();
                vpoFtyGacDateTemp = (("0" + (vpoFtyGacDateTemp.getMonth() + 1)).slice(-2)) + '/' + (("0" + vpoFtyGacDateTemp.getDate()).slice(-2)) + '/' + vpoFtyGacDateTemp.getFullYear();
                if (ovalue !== vpoFtyGacDateTemp) {
                    return 'editable-cell-yl';
                }
            }
        }
        else {
            let fieldName = '';
            if (columnfield.indexOf('PlannedDate') > -1) {
                fieldName = 'PlannedDate';
            } else if (columnfield.indexOf('RevisedDate') > -1) {
                fieldName = 'RevisedDate'
            } else if (columnfield.indexOf('ActualDate') > -1) {
                fieldName = 'ActualDate';
            }

            if (fieldName.length > 0) {
                const ccol = columnfield.replace(fieldName, '');
                const eventCode = this.psrGrid.getcellvalue(row, ccol + 'eventCode');
                const disableEvent = this.psrGrid.getcellvalue(row, ccol + 'disableEvent');

                if (eventCode === undefined || eventCode === 'undefined' || eventCode === '' || eventCode === null || eventCode === 'null' || disableEvent ===true) {

                    return 'editable-cell-gray';
                }
            }

            if (columnfield.indexOf('PlannedDate') > -1) {
                return '';
            }

            if (columnfield.indexOf('RevisedDate') > -1) {
                const ccol = columnfield.replace(fieldName, '');
                const highlightdate = this.psrGrid.getcellvalue(row, ccol + 'highlightRevisedDate');
                if (highlightdate == 'true' || highlightdate == true) {
                    return 'editable-cell-pink';
                }

            }

            return 'editable-cell-bg';

        }
    };

    cellbeginedit = (row, datafield, columntype, value): boolean => {
        if (datafield.indexOf('RevisedDate') > -1) {
            const ccol = datafield.replace('RevisedDate', '');

            const eventCode = this.psrGrid.getcellvalue(row, ccol + 'eventCode');
            const disableEvent = this.psrGrid.getcellvalue(row, ccol + 'disableEvent');
            if (eventCode === undefined || eventCode === 'undefined' || eventCode === '' || eventCode === null || eventCode === 'null' || disableEvent ===true) {
                return false;
            }
            const actualDate = this.psrGrid.getcellvalue(row, ccol + 'ActualDate');
            if (actualDate !== undefined && actualDate !== null && actualDate !== '') {
                this.toastr.errorToastr('Actual Date already exists.', 'Error!', { dismiss: 'click', showCloseButton: true });
                return false;
            }

            //New code
        //    const disableEvent = this.psrGrid.getcellvalue(row, ccol + 'disableEvent');
        //    if (disableEvent) {
        //        return false;
        //    }

        } else if (datafield.indexOf('ActualDate') > -1) {
            const ccol = datafield.replace('ActualDate', '');
            const eventCode = this.psrGrid.getcellvalue(row, ccol + 'eventCode');
            const disableEvent = this.psrGrid.getcellvalue(row, ccol + 'disableEvent');
            if (eventCode === undefined || eventCode === 'undefined' || eventCode === '' || eventCode === null || eventCode === 'null' || disableEvent ===true) {
                return false;
            }

        //      //New code
        //    const disableEvent = this.psrGrid.getcellvalue(row, ccol + 'disableEvent');
        //    if (disableEvent) {
        //         return false;
        //    }
        }

    };

    handlekeyboardnavigation = (event): boolean => {

        const currentCell: any = this.psrGrid.getselectedcell();
        const key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;


        if (currentCell !== null) {

            if (key === 88 && event.ctrlKey === true) {
                this.toastr.errorToastr('Cut not allowed on grid', 'Error!', { dismiss: 'click', showCloseButton: true });
                event.preventDefault();
                return true;
            }
            if (key === 86 && event.ctrlKey === true) {

                /* handlekeyboardnavigation for Milestones  starts here*/
                for (let index = 0; index < this.columngroups1.length; index++) {
                    if (currentCell.datafield === this.columngroups1[index] + 'PlannedDate') {
                        this.toastr.warningToastr(this.columngroups1[index] + ' Planned Date' + ' is a non editable field', "Alert!", { dismiss: 'click', showCloseButton: true });
                        event.preventDefault();
                        return true;
                    }
                    if (currentCell.datafield === this.columngroups1[index] + 'RevisedDate') {
                        const eventCode = this.psrGrid.getcellvalue(currentCell.rowindex, this.columngroups1[index] + 'eventCode');
                        const disableEvent = this.psrGrid.getcellvalue(currentCell.rowindex, this.columngroups1[index] + 'disableEvent');
                        if (eventCode === undefined || eventCode === null || eventCode === "" || disableEvent ===true) {
                            this.toastr.warningToastr(this.columngroups1[index] + ' Revised Date' + ' is a non editable field', "Alert!", { dismiss: 'click', showCloseButton: true });
                            event.preventDefault();
                            return true;
                        } else {
                            const actualDate = this.psrGrid.getcellvalue(currentCell.rowindex, this.columngroups1[index] + 'ActualDate');
                            if (actualDate !== undefined && actualDate !== null && actualDate !== '') {
                                this.toastr.errorToastr('Actual Date already exists.', 'Error!', { dismiss: 'click', showCloseButton: true });
                                event.preventDefault();
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                    if (currentCell.datafield === this.columngroups1[index] + 'ActualDate') {
                        const eventCode = this.psrGrid.getcellvalue(currentCell.rowindex, this.columngroups1[index] + 'eventCode');
                        const disableEvent = this.psrGrid.getcellvalue(currentCell.rowindex, this.columngroups1[index] + 'disableEvent');
                        if (eventCode === undefined || eventCode === null || eventCode === "" || disableEvent ===true) {
                            this.toastr.warningToastr(this.columngroups1[index] + ' Actual Date' + ' is a non editable field', "Alert!", { dismiss: 'click', showCloseButton: true });
                            event.preventDefault();
                            return true;
                        }
                    }
                }
                /* handlekeyboardnavigation for Milestones  starts here*/

                /* handlekeyboardnavigation for other fields starts here*/
                for (let h = 0; h <= this.columns.length; h++) {
                    if ((currentCell.datafield == this.columns[h].datafield) && (this.columns[h].editable == false)) {
                        if (this.columns[h].datafield == 'GAC by VendorActualDate') {
                            this.toastr.warningToastr('GAC by Vendor Actual Date ' + " is a non editable field", "Alert!", { dismiss: 'click', showCloseButton: true });
                        } else {
                            this.toastr.warningToastr(this.columns[h].text + " is a non editable field", "Alert!", { dismiss: 'click', showCloseButton: true });
                        }
                        event.preventDefault();
                        return true;
                    } else if (currentCell.datafield == this.columns[h].datafield && (this.columns[h].editable == true)) {
                        if (this.columns[h].columntype == 'checkbox') {
                            this.toastr.warningToastr("Checkbox doesn't allow Copy paste", "Alert!", { dismiss: 'click', showCloseButton: true });
                            return true;
                        } else {
                            return false;
                        }

                    }
                }
                /* handlekeyboardnavigation for other fields ends here*/

            }

        }

    }

    source: any =
        {
            datatype: 'json',
            datafields: this.datafields,
            unboundmode: true,
            // url: '../assets/products.txt',
            url: '',
            autowidth: true,
        };

    dataAdapter = new jqx.dataAdapter(this.source, {
        async: true, autoBind: true,
        downloadComplete: (data) => {
            localStorage.setItem('allItems', data.totalRows);
            localStorage.setItem('searchTs', data.searchTs.toString());
            this.setPage(this.currentPage);
            this.getReorder();
        }
    });
    appendPercentage = (row: number, columnfield: string, value: string, defaulthtml: string, columnproperties: any,
        rowdata: any): string => {
        if (value !== undefined && value !== null && value !== '' && value !== 'null') {
            //if(value.indexOf('%') !== -1)
             return Number(value).toFixed(2) +"%";
        }
    };

    filerenderer = (row: number, columnfield: string, value: string, defaulthtml: string, columnproperties: any,
        rowdata: any): string => {
        const rowid = 'image' + row;
        let fileUploadStr: string;
        if (value !== undefined && value !== null && value !== '' && value !== 'null') {
            fileUploadStr = '<table><tr><td><i class="fa fa-paperclip" onclick="browsefile(\'' + rowid + '\')"></i></td>' +
                '<td><div style="width: 50px;"><img style="margin: 0 auto;text-align: center;display: block;width: 100%;" src="' + environment.imagePath + value + '" id="image-' + row + '" height="30" (mouseWheelUp)="" onclick="showFullImage(\'' + row + '\')"  /></div><input type="file" name="imageType" id="image' + row + '"   onchange="previewFile(' + row + ',' + this.psrGrid.getcellvalue(row, 'psrOrderId') + ')" style="display: none"/></td><td><i class="fa fa-remove" onclick="removeImage(\'' + value + '\',\'' + row + '\',\'' + environment.apiUrl + '\')"></i></td></tr></table>';
        } else {
            fileUploadStr = '<table><tr><td><i class="fa fa-paperclip" onclick="browsefile(\'' + rowid + '\')"></i></td>' +
                '<td><img src="" id="image-' + row + '" height="30" (mouseWheelUp)="" onclick="showFullImage(\'' + row + '\')"  /><input type="file" name="imageType" id="image' + row + '"   onchange="previewFile(' + row + ',' + this.psrGrid.getcellvalue(row, 'psrOrderId') + ')" style="display: none"/></td></tr></table>';
        }
        return fileUploadStr;
    };

    addSmartTag = (row: number, columnfield: string, value: any, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        return `<div style='padding:5px 10px; display:flex; justify-content:space-between;'><span style='display:inline-block;overflow: hidden; text-overflow: ellipsis; margin-right:5px;'>${value}</span><span onclick = 'smartTag(${row})' style='cursor:pointer;'><img src="${this.smartTagSrc}" alt=""></span></div>`;

    }

    dropdownlist = (row, cellvalue, editor, celltext, cellwidth, cellheight): void => {
        editor.jqxDropDownList({ source: this.dataAdapter.records[0].locations });
    };

    checkdatevalidation = function (value) {
        if (typeof (value) == 'string') {
            var bits = value.split('/');
            var d = new Date(+bits[2], +bits[0] - 1, +bits[1]);
            return d && (d.getMonth() + 1) == +bits[0];
        }
    }

    /**editable validation starts here */

    validation = (cell, value) => {

        for (let val = 0; val < this.datafields.length; val++) {

            if (cell.datafield == this.datafields[val].name) {

                if (this.datafields[val].type == 'string') {
                    if (value.length > this.datafields[val].length) {
                        this.isValid = false;
                        return { result: false, message: `${this.datafields[val].text} value is not more than ${this.datafields[val].length} characters` };
                    } else {
                        this.isValid = true;
                        return true;
                    }
                } else {
                    if (isNaN(value)) {
                        this.isValid = false;
                        return { result: false, message: `${this.datafields[val].text}  Field does not allow character` };
                    } else if (value.length > this.datafields[val].length) {
                        this.isValid = false;
                        return { result: false, message: `${this.datafields[val].text} value is not more than ${this.datafields[val].length} characters` };
                    } else {
                        this.isValid = true;
                        return true;
                    }
                }


            }

        }
    }

    /**editable validation ends here */

    /**copy paste valiadtion starts here*/
    validationCopyPaste(event: any, old_value, flagfillupdown) {

        let datafield = event.datafield;

        let rowindex = event.rowindex;
        for (let val = 0; val < this.datafields.length; val++) {

            if ((this.datafields[val].type == 'string' || this.datafields[val].type == 'int' || this.datafields[val].type == 'BigDecimal') && (datafield == this.datafields[val].name)) {

                let value;
                if (this.datafields[val].type == 'BigDecimal') {
                    value = event.value.toString();
                } else {
                    value = event.value;
                }
                if (value.length > this.datafields[val].length) {

                    if (this.isIE === false) {
                        this.toastr.errorToastr(`${this.datafields[val].text} value is not more than ${this.datafields[val].length} characters`, 'Error!', { dismiss: 'click', showCloseButton: true });
                        this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                    } else {
                        if (this.invalidCell.indexOf(rowindex + datafield) === -1) {
                            this.toastr.errorToastr(`${this.datafields[val].text} value is not more than ${this.datafields[val].length} characters`, 'Error!', { dismiss: 'click', showCloseButton: true });
                            this.invalidCell.push(rowindex + datafield)
                            this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                        } else {
                            let index = this.invalidCell.indexOf(rowindex + datafield);
                            delete this.invalidCell[index];
                            this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                        }
                    }

                }

            } else if (this.datafields[val].type == 'date') {

                let value = event.value;
                if (datafield == this.datafields[val].name) {

                    if (datafield.indexOf('RevisedDate') > -1) {
                        const ccol = datafield.replace('RevisedDate', '');
                        const eCode = this.psrGrid.getcellvalue(rowindex, ccol + 'eventCode');

                        if (eCode !== undefined && eCode !== null && eCode !== '') {

                            let date_status = this.checkdatevalidation(value);
                            if (this.isIE === false) {
                                if (date_status == false) {
                                    this.toastr.errorToastr('Please enter valid date', 'Error!', { dismiss: 'click', showCloseButton: true });
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                }
                            } else {
                                if (date_status === false && this.invalidCell.indexOf(rowindex + datafield) === -1) {
                                    this.toastr.errorToastr('Please enter valid date', 'Error!', { dismiss: 'click', showCloseButton: true });
                                    this.invalidCell.push(rowindex + datafield)
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                } else if (date_status === false) {
                                    let index = this.invalidCell.indexOf(rowindex + datafield);
                                    delete this.invalidCell[index];
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                }
                            }

                            const actualDate = this.psrGrid.getcellvalue(rowindex, ccol + 'ActualDate');
                            if (actualDate !== undefined && actualDate !== null && actualDate !== '') {
                                if (this.isIE === false) {
                                    this.toastr.errorToastr('Actual Date already exists.', 'Error!', { dismiss: 'click', showCloseButton: true });
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                } else {
                                    if (this.invalidCell.indexOf(rowindex + datafield) === -1) {
                                        this.toastr.errorToastr('Actual Date already exists.', 'Error!', { dismiss: 'click', showCloseButton: true });
                                        this.invalidCell.push(rowindex + datafield)
                                        this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                    } else {
                                        let index = this.invalidCell.indexOf(rowindex + datafield);
                                        delete this.invalidCell[index];
                                        this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                    }
                                }

                            }
                        } else {
                            if (this.isIE === false) {
                                if (flagfillupdown != 'fillupdownevent') {
                                    this.toastr.warningToastr(ccol + ' Revised Date' + ' is a non editable field', "Alert!", { dismiss: 'click', showCloseButton: true });
                                }
                                this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                            } else {
                                if (this.invalidCell.indexOf(rowindex + datafield) === -1) {
                                    if (flagfillupdown != 'fillupdownevent') {
                                        this.toastr.warningToastr(ccol + ' Revised Date' + ' is a non editable field', "Alert!", { dismiss: 'click', showCloseButton: true });
                                    }
                                    this.invalidCell.push(rowindex + datafield)
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                } else {
                                    let index = this.invalidCell.indexOf(rowindex + datafield);
                                    delete this.invalidCell[index];
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                }
                            }

                        }


                    } else if (datafield.indexOf('ActualDate') > -1) {
                        const ccol = datafield.replace('ActualDate', '');
                        const eCode = this.psrGrid.getcellvalue(rowindex, ccol + 'eventCode');

                        if (eCode !== undefined && eCode !== null && eCode !== '') {
                            let date_status = this.checkdatevalidation(value);
                            if (this.isIE === false) {
                                if (date_status == false) {
                                    this.toastr.errorToastr('Please enter valid date', 'Error!', { dismiss: 'click', showCloseButton: true });
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                }
                            } else {
                                if (date_status === false && this.invalidCell.indexOf(rowindex + datafield) === -1) {
                                    this.toastr.errorToastr('Please enter valid date', 'Error!', { dismiss: 'click', showCloseButton: true });
                                    this.invalidCell.push(rowindex + datafield)
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                } else if (date_status === false) {
                                    let index = this.invalidCell.indexOf(rowindex + datafield);
                                    delete this.invalidCell[index];
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                }
                            }
                        } else {
                            if (this.isIE === false) {
                                if (flagfillupdown != 'fillupdownevent') {
                                    this.toastr.warningToastr(ccol + ' Actual Date' + ' is a non editable field', "Alert!", { dismiss: 'click', showCloseButton: true });
                                }
                                this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                            } else {
                                if (this.invalidCell.indexOf(rowindex + datafield) === -1) {
                                    if (flagfillupdown != 'fillupdownevent') {
                                        this.toastr.warningToastr(ccol + ' Actual Date' + ' is a non editable field', "Alert!", { dismiss: 'click', showCloseButton: true });
                                    }
                                    this.invalidCell.push(rowindex + datafield)
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                } else {
                                    let index = this.invalidCell.indexOf(rowindex + datafield);
                                    delete this.invalidCell[index];
                                    this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                                }
                            }
                        }



                    } else {
                        let value = event.value;
                        let date_status = this.checkdatevalidation(value);

                        if (this.isIE === false) {
                            if (date_status === false) {
                                this.toastr.errorToastr('Please enter valid date', 'Error!', { dismiss: 'click', showCloseButton: true });
                                this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                            }
                        } else {
                            if (date_status === false && this.invalidCell.indexOf(rowindex + datafield) === -1) {
                                this.toastr.errorToastr('Please enter valid date', 'Error!', { dismiss: 'click', showCloseButton: true });
                                this.invalidCell.push(rowindex + datafield)
                                this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                            } else if (date_status === false) {
                                let index = this.invalidCell.indexOf(rowindex + datafield);
                                delete this.invalidCell[index];
                                this.psrGrid.setcellvalue(rowindex, datafield, old_value);
                            }
                        }

                    }

                }


            }
        }
    }

    /**copy paste valiadtion ends here*/

    columns: any[] =
        [
            { text: '', datafield: 'select', align: "center", columntype: 'checkbox', width: '50px', pinned: true, editoptions: { value: "1:0" }, formatoptions: { disabled: false } },
            { text: 'Style #', datafield: 'style', width: '7%', editable: false, pinned: true, cellsrenderer: this.addSmartTag },
            { text: 'Color Wash Name', datafield: 'colorWashName', width: '12%', editable: false, pinned: true },
            { text: 'Order Commit #', datafield: 'ocCommit', width: '10%', editable: false, pinned: true },
            { text: 'Vpo #', datafield: 'vpo', width: '167px', editable: false, pinned: true },
            { text: 'Psr Order Id', datafield: 'psrOrderId', width: '8%', editable: false },
            { text: 'Style Desc', datafield: 'styleDesc', width: '16%', editable: false },
            { text: 'Init Flow', datafield: 'initFlow', width: '6%', editable: false },
            { text: 'Image', datafield: 'image', width: '6%', editable: false, resizable: false, cellsrenderer: this.filerenderer },
            { text: 'Order Qty', datafield: 'orderQty', width: '6%', editable: false },
            { text: 'Cpo', datafield: 'cpo', width: '6%', editable: false },
            { text: 'Ndc Date', datafield: 'ndcDate', width: '7%', editable: false, cellsformat: 'MM/dd/yyyy' },
            { text: 'Vpo Fty Gac Date', datafield: 'vpoFtyGacDate', width: '10%', editable: false, cellsformat: 'MM/dd/yyyy', cellclassname: this.cellclass },
            { text: 'Vpo Nlt Gac Date', datafield: 'vpoNltGacDate', width: '10%', editable: false, cellsformat: 'MM/dd/yyyy' },
            { text: 'Ndc Wk', datafield: 'ndcWk', width: '8%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Vendor Id', datafield: 'vendorId', width: '7%', editable: false },
            { text: 'Factory Id', datafield: 'factoryId', width: '7%', editable: false },
            { text: 'Factory Name', datafield: 'factoryName', width: '22%', editable: false },
            { text: 'Production Office', datafield: 'prodOffice', width: '10%', editable: false },
            { text: 'Dept', datafield: 'dept', width: '6%', editable: false },
            { text: 'Fob', datafield: 'fob', width: '4%', editable: false },
            { text: 'Gross Sell Price', datafield: 'grossSellPrice', width: '9%', editable: false },
            { text: 'Margin', datafield: 'margin', width: '5%', editable: false },
            { text: 'Sales Amount', datafield: 'salesAmount', width: '8%', editable: false },
            { text: 'Ship To', datafield: 'shipTo', width: '6%', editable: false },
            { text: 'Ship Mode', datafield: 'shipMode', width: '7%', editable: false },
            { text: 'CPO Style No', datafield: 'cpoStyleNo', width: '8%', editable: false },
            { text: 'Key Item', datafield: 'keyItem', width: '10%', editable: false },
            { text: 'CPO Dept', datafield: 'cpoDept', width: '10%', editable: false },
            { text: 'Design Card Number/PLM Number', datafield: 'plmNo', width: '20%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Cpo Color Desc', datafield: 'cpoColorDesc', width: '9%', editable: false },
            { text: 'Cpo Order Qty', datafield: 'cpoOrderQty', width: '9%', editable: false },
            { text: 'Cpo Ship Mode', datafield: 'cpoShipMode', width: '9%', editable: false },
            { text: 'Artwork name', datafield: 'graphicName', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Ticket Style Number', datafield: 'ticketStyleNumeric', width: '12%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Ticket Color Code', datafield: 'ticketColorCode', width: '10%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Care Label Code', datafield: 'careLabelCode', width: '10%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Color Code Pattern', datafield: 'colorCodePatrn', width: '11%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Cpo Pack Type', datafield: 'cpoPackType', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Cust Fty Id', datafield: 'custFtyId', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Cut Appr Letter', datafield: 'cutApprLetter', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Extra Process', datafield: 'extraProcess', width: '8%', editable: true, align: "center", columntype: 'checkbox' },
            { text: 'Fabric Code', datafield: 'fabricCode', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Fiber Content', datafield: 'fiberContent', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Fabric Mill', datafield: 'fabricMill', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Request Number', datafield: 'requestNo', width: '10%', editable: false },
            { text: 'Program Name', datafield: 'programName', width: '9%', editable: false },
            { text: 'Fab Trim Order', datafield: 'fabTrimOrder', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Factory Ref', datafield: 'factoryRef', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Sell Channel', datafield: 'orderType', width: '8%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Pack Type', datafield: 'packType', width: '8%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'VPO Order Type', datafield: 'vpoOrderType', width: '10%', editable: false },
            { text: 'VPO Speed Indicator', datafield: 'speedIndicator', width: '12%', editable: false },
            { text: 'ANF ASN #', datafield: 'asn', width: '7%', editable: false },
            { text: 'Shipped Qty', datafield: 'shippedQty', width: '9%', editable: false },
            { text: 'Balance Ship', datafield: 'balanceShip', width: '8%', editable: false },
            { text: 'Comment', datafield: 'comment', width: '8%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Season', datafield: 'season', width: '10%', editable: false },
            { text: 'Vpo Status', datafield: 'vpoStatus', width: '7%', editable: false },
            { text: 'Vpo Line Status', datafield: 'vpoLineStatus', width: '9%', editable: false },
            { text: 'Merchant', datafield: 'merchant', width: '16%', editable: false },
            { text: 'Product Type', datafield: 'productType', width: '9%', editable: false },
            { text: 'Gender', datafield: 'gender', width: '6%', editable: false },
            { text: 'Country Origin', datafield: 'countryOrigin', width: '10%', editable: false },
            { text: 'Port of Origin', datafield: 'ladingPoint', width: '8%', editable: false },
            { text: 'Fit Owner', datafield: 'techDesigner', width: '7%', editable: false },
            { text: 'Wash', datafield: 'washYn', width: '5%', editable: true, align: "center", columntype: 'checkbox' },
            { text: 'Wash Facility', datafield: 'washFacility', width: '8%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Fabric Ship Mode', datafield: 'fabricShipMode', width: '10%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Tech Pack Received Date', datafield: 'techPackReceivedDateStr', width: '14%', editable: true, cellclassname: this.cellclass, cellsformat: 'MM/dd/yyyy', "columntype": "datetimeinput" },
            { text: 'Sample Material Status', datafield: 'sampleMaterialStatus', width: '14%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Daily Output Per Line', datafield: 'dailyOutputPerLine', width: '12%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Fabric Test Report#', datafield: 'fabricTestReportNumeric', width: '12%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Garment Test Report#', datafield: 'garmentTestReportNo', width: '15%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Garment Test Actual', datafield: 'garmentTestActualStr', width: '12%', editable: true, cellclassname: this.cellclass, cellsformat: 'MM/dd/yyyy', "columntype": "datetimeinput", cellbeginedit: this.cellbeginedit, hidden: true, customhidden: true },
            { text: 'Actual Poundage Available', datafield: 'actualPoundageAvailable', filtertype: 'numericfilter', cellsformat: 'f2', width: '16%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Appr Fabric Yarn Lot', datafield: 'apprFabricYarnLot', width: '12%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Gauge', datafield: 'gauge', width: '7%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Knitting %', datafield: 'knitting', filtertype: 'numericfilter', cellsformat: 'f2', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation, cellsrenderer: this.appendPercentage },
            { text: '# of Knitting Machines', datafield: 'knittingMachines', width: '13%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Knitting Pcs', datafield: 'knittingPcs', width: '10%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Linking %', datafield: 'linking', filtertype: 'numericfilter', cellsformat: 'f2', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation, cellsrenderer: this.appendPercentage },
            { text: '# of Linking Machines', datafield: 'linkingMachines', width: '13%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Linking Pcs', datafield: 'linkingPcs', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: '# of Sewing Lines', datafield: 'sewingLines', width: '11%', editable: true, validation: this.validation, cellclassname: this.cellclass },
            { text: 'Garment Treatment Pcs', datafield: 'garmentTreatmentPcs',filtertype: 'numericfilter',  width: '15%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Finished Garment Pcs', datafield: 'finishedGarmentPcs',filtertype: 'numericfilter',  width: '13%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Packaging Ready Date', datafield: 'packagingReadyDateStr', width: '14%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy', cellbeginedit: this.cellbeginedit },
            { text: 'Packed Units Pcs', datafield: 'packedUnitsPcs',filtertype: 'numericfilter',  width: '10%', editable: true, cellclassname: this.cellclass, validation: this.validation },          
            { text: 'Ship Date', datafield: 'plShipDate', width: '7%', editable: false, cellsformat: 'MM/dd/yyyy' },
            { text: 'So Id', datafield: 'soId', width: '5%', editable: false },
            { text: 'Vendor Name', datafield: 'vendorName', width: '18%', editable: false, hidden: true, customhidden: true },
            { text: 'Brand', datafield: 'brand', width: '5%', editable: false, hidden: true, customhidden: true },
            { text: 'Category', datafield: 'category', width: '7%', editable: false, hidden: true, customhidden: true },
            { text: 'Modify User', datafield: 'modifyUser', width: '7%', editable: false, hidden: true, customhidden: true },
            { text: 'Interface Ts', datafield: 'interfaceTs', width: '8%', editable: false, cellsformat: 'MM/dd/yyyy', hidden: true, customhidden: true },
            { text: 'Pl Actual Gac', datafield: 'plActualGac', width: '14%', editable: false, cellsformat: 'MM/dd/yyyy', hidden: true, customhidden: true },
            { text: 'Cpo Gac Date', datafield: 'cpoGacDate', width: '12%', editable: false, cellsformat: 'MM/dd/yyyy', hidden: true, customhidden: true },
            { text: 'Create Date', datafield: 'createDate', width: '8%', editable: false, cellsformat: 'MM/dd/yyyy', hidden: true, customhidden: true },
            { text: 'Profoma Po', datafield: 'profomaPo', width: '10%', editable: false, hidden: true, customhidden: true },
            { text: 'Vpo Fty Gac Date Temp', datafield: 'vpoFtyGacDateTemp', width: '10%', editable: false, cellsformat: 'MM/dd/yyyy', hidden: true, customhidden: true },
            { text: 'Plan Id', datafield: 'planId', width: '10%', editable: false, hidden: true, customhidden: true },
            { text: 'Fabric Est Date', datafield: 'fabricEstDate', width: '9%', editable: true, cellclassname: this.cellclass, cellsformat: 'MM/dd/yyyy', "columntype": "datetimeinput", cellbeginedit: this.cellbeginedit, hidden: true, customhidden: true },
            //  { text: 'Highlight Revised Date', datafield: 'highlightRevisedDate', width: '20%', editable: false}
            { text: 'Retail Price', datafield: 'retailPrice',filtertype: 'numericfilter', cellsformat: 'f2', width: '7%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Fabric Pp Date', datafield: 'fabricPpDateStr', width: '9%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Delivery Month', datafield: 'deliveryMonth', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Cpo Acc Date By Vendor', datafield: 'cpoAccDateByVendorStr', width: '14%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Dist Channel', datafield: 'distChannel', width: '8%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Floor Set', datafield: 'floorSet', width: '6%', editable: true, align: "center", columntype: 'checkbox' },
            { text: 'Style At Risk', datafield: 'styleAtRisk', width: '8%', editable: true, align: "center", columntype: 'checkbox' },
            { text: 'Sample Merch Eta', datafield: 'sampleMerchEtaStr', width: '11%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Sample Floor Set Eta', datafield: 'sampleFloorSetEtaStr', width: '13%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Sample Dcom Eta', datafield: 'sampleDcomEtaStr', width: '11%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Sample Mailer', datafield: 'sampleMailer', width: '9%', editable: true, align: "center", columntype: 'checkbox' },
            { text: 'Sample Mailer Eta', datafield: 'sampleMailerEtaStr', width: '11%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Photo/Merchant Sample Send date', datafield: 'photoMerchantSampleSendDateStr', width: '20%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Photo/Merchant Sample ETA date', datafield: 'photoMerchantSampleEtaDateStr', width: '19%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Photo/Merchant Sample Comment', datafield: 'photoMerchantSampleComment', width: '20%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Marketing Sample Send date', datafield: 'marketingSampleSendDateStr', width: '17%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Marketing Sample ETA date', datafield: 'marketingSampleEtaDateStr', width: '17%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Marketing Sample Comment', datafield: 'marketingSampleComment', width: '17%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Visual Sample Send date', datafield: 'visualSampleSendDateStr', width: '15%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Visual Sample ETA date', datafield: 'visualSampleEtaDateStr', width: '14%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Visual Sample Comment', datafield: 'visualSampleComment', width: '14%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Copyright Sample Send date', datafield: 'copyrightSampleSendDateStr', width: '17%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Copyright Sample ETA date', datafield: 'copyrightSampleEtaDateStr', width: '17%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Copyright Sample Comment', datafield: 'copyrightSampleComment', width: '17%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Additional Bulk Lot Approve', datafield: 'additionalBulkLotApproveStr', width: '17%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'Reason For Late Gac', datafield: 'reasonForLateGac', width: '13%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Color Comment', datafield: 'colorComment', width: '10%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Fabric Comment', datafield: 'fabricComment', width: '10%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Fabric Test Comment', datafield: 'fabricTestComment', width: '13%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Garment Test Comment', datafield: 'garmentTestComment', width: '15%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Trim Comment', datafield: 'trimComment', width: '9%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Sample Comment', datafield: 'sampleComment', width: '11%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'Fit Sample Comment', datafield: 'fitSampleComment', width: '12%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'PP Sample Comment', datafield: 'ppSampleComment', width: '12%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            { text: 'TOP sample ETA Date', datafield: 'topSampleEtaDateStr', width: '13%', editable: true, cellclassname: this.cellclass, columntype: "datetimeinput", cellsformat: 'MM/dd/yyyy' },
            { text: 'TOP sample Comment', datafield: 'topSampleComment', width: '14%', editable: true, cellclassname: this.cellclass, validation: this.validation },
            
        ];

    listBoxSource: any[] = [];
    holdlistOriginal: any[] = [];
    constructor(private api?: ApiService, private searchService?: SearchService, public toastr?: ToastrManager,
        public progress?: NgProgress, private router?: Router, public _global?: Globals, private pagerService?: PagerService, private excelService?: ExcelService, private ngZone?: NgZone) {
        this.searchFormModel.vpoSelect = this.searchFormModel.factoryNameSelect = 'Like';
        this.searchFormModel.vendorIdSelect = this.searchFormModel.ocCommitSelect = 'Like';
        this.searchFormModel.factoryIdSelect = this.searchFormModel.brandSelect = 'Like';
        this.searchFormModel.styleSelect = this.searchFormModel.styleDescSelect = 'Like';
        this.searchFormModel.prodOfficeSelect = this.searchFormModel.countryOriginSelect = 'Like';
        this.searchFormModel.categorySelect = this.searchFormModel.cpoColorDescSelect = 'Like';
        this.searchFormModel.cpoSelect = this.searchFormModel.colorWashNameSelect = 'Like';
        this.searchFormModel.deptSelect = this.searchFormModel.seasonSelect = 'Like';
        this.searchFormModel.ndcdateSelect = this.searchFormModel.vpoFtyGacDateSelect = 'Greater';
        this.searchFormModel.vpoNltGacDateSelect = 'Greater';
        this.searchFormModel.ndcWkSelect = 'Like';
        this.searchFormModel.vpoStatusSelect = this.searchFormModel.vpoLineStatusSelect = 'Not in the list';
        this.searchFormModel.cpoDeptSelect = 'Like';
        this.searchFormModel.initFlowSelect = 'Like';

    }

    ngAfterViewChecked() {
        this.changeWidth3();
    }
    BindingComplete(event: any): void {

    }
    Cellbegineditpsr(event: any) {
        this.oldvalue = this.psrGrid.getcellvalue(event.args.rowindex, event.args.datafield);
    }
    Cellvaluechanged(event: any, getcall): void {
        if ((event.args.datafield !== 'select') && (event.args.datafield !== 'extraProcess') && (event.args.datafield !== 'washYn') && (event.args.datafield !== 'floorSet') && (event.args.datafield !== 'styleAtRisk') && (event.args.datafield !== 'sampleMailer')) {
            this.getCellValueChanged = event;
            if (getcall == 'fillUpDown') {
                this.Cellendedit(event);
            }
        }
    }
    cellclick(): void {
        this.getCellValueChanged = undefined;
    }
    fillGridUp(): void {

        if ((this.getCellValueChanged != undefined) && (this.getCellValueChanged.args.value != null)) {
            let row_index = this.getCellValueChanged.args.rowindex;
            let cellvalue = this.getCellValueChanged.args.value;
            let datafield = this.getCellValueChanged.args.datafield;
            this.psrGrid.beginupdate();
            for (let i = +row_index - 1; i >= 0; i--) {
                this.psrGrid.setcellvalue(i, datafield, cellvalue);
                this.Cellvaluechanged(this.getCellValueChanged, 'fillUpDown');
            }
            this.psrGrid.begincelledit(row_index, datafield);
            this.psrGrid.endupdate();
        } else {
            this.toastr.warningToastr("You have not edited any field or your field is empty", "Alert!", { dismiss: 'click', showCloseButton: true });
        }
    }
    fillGridDown(): void {
        if ((this.getCellValueChanged != undefined) && (this.getCellValueChanged.args.value != null)) {
            let row_index = this.getCellValueChanged.args.rowindex;
            let cellvalue = this.getCellValueChanged.args.value;
            let datafield = this.getCellValueChanged.args.datafield;
            this.psrGrid.beginupdate();
            let grid_length = this.psrGrid.getrows().length;
            this.psrGrid.beginupdate();
            for (let i = +row_index + 1; i < grid_length; i++) {
                this.psrGrid.setcellvalue(i, datafield, cellvalue);
                this.Cellvaluechanged(this.getCellValueChanged, 'fillUpDown');
            }
            this.psrGrid.begincelledit(row_index, datafield);
            this.psrGrid.endupdate();
        } else {
            this.toastr.warningToastr("You have not edited any field or your field is empty", "Alert!", { dismiss: 'click', showCloseButton: true });

        }

    }

    scrollColumn(event): void {

        let width: number = this.psrGrid.getcolumnproperty('select', 'width');

        if (this.psrGrid.iscolumnvisible('colorWashName') == true) {
            width += this.psrGrid.getcolumnproperty('colorWashName', 'width')
        }
        if (this.psrGrid.iscolumnvisible('ocCommit') == true) {
            width += this.psrGrid.getcolumnproperty('ocCommit', 'width')
        }
        if (this.psrGrid.iscolumnvisible('style') == true) {
            width += this.psrGrid.getcolumnproperty('style', 'width')
        }
        if (this.psrGrid.iscolumnvisible('vpo') == true) {
            width += this.psrGrid.getcolumnproperty('vpo', 'width')
        }



        for (let i = 0; i < this.secondScrollList.length; i++) {

            if (this.secondScrollList[i].originalItem.label == event.text) {

                if (this.secondScrollList[i].originalItem.milestone == true) {
                    this.secondScrollList[i].originalItem.value = event.text + 'PlannedDate';
                }
                if (!this.psrGrid.iscolumnvisible(this.secondScrollList[i].originalItem.value)) {
                    this.toastr.warningToastr("Selected column is hidden", "Alert!", { dismiss: 'click', showCloseButton: true });

                } else {


                    let t = this.psrGrid.getcolumnindex(this.secondScrollList[i].originalItem.value);
                    let arrcolumn: any
                    if (this.isIE == true) {
                        arrcolumn = this.psrGrid.getcolumnat(t).element.attributes[2].nodeValue;
                    } else {
                        arrcolumn = this.psrGrid.getcolumnat(t).element.attributes[1].nodeValue;
                    }

                    var properties = arrcolumn.split(';');
                    let margin_left: any;
                    for (let item of properties) {
                        let tup: any = item.split(':');
                        if (tup[0].indexOf('left') > -1) {
                            margin_left = tup[1];
                        }
                    }
                    this.psrGrid.scrolloffset(0, parseInt(margin_left) - width);

                }
                break;

            }

        }
    }

    Cellendedit(event: any): void {
        if (event.args.value !== null) {
            if (event.type == 'cellvaluechanged') {
                this.validationCopyPaste(event.args, event.args.oldvalue, 'fillupdownevent');
            } else {
                this.validationCopyPaste(event.args, this.oldvalue, '');
            }

        }
        if (event.args.datafield != 'select') {
            let val = this.psrGrid.getcellvalue(event.args.rowindex, 'psrOrderId');
            if (this.editedRowPsrId.indexOf(val) == -1) {
                this.editedRowPsrId.push(val);
            }
            if (this.getMilestoneAllcolumn().indexOf(event.args.datafield) > -1) {
                let date_datafield = event.args.datafield;
                let changed_milestone_pointer: any;
                if (date_datafield.indexOf('ActualDate') > -1) {
                    let extract_ActualText = date_datafield.replace('ActualDate', '');
                    changed_milestone_pointer = extract_ActualText + '+' + val;
                } else {
                    let extract_ActualText = date_datafield.replace('RevisedDate', '');
                    changed_milestone_pointer = extract_ActualText + '+' + val;
                }
                if (this.editedMilestoneField.indexOf(changed_milestone_pointer) == -1) {
                    this.editedMilestoneField.push(changed_milestone_pointer);
                }
            }
        }

    }
    getMilestoneAllcolumn() {
        let milestorecolumndata = [];
        for (let md = 0; md < this.columngroups.length; md++) {
            let milestone_append_field = ['eventCode', 'ActualDate', 'RevisedDate'];
            for (let mc = 0; mc < milestone_append_field.length; mc++) {
                milestorecolumndata.push(this.columngroups[md].name + milestone_append_field[mc]);
            }
        }
        return milestorecolumndata
    }

    psrGridBoxOnCheckChange(event: any): void {

        if (this.flagCheck) {
            this.psrGrid.beginupdate();
        }
        if (event.args.checked) {
            this.selectedCheckbox.push(event.args.value);

            if(this.reorderFlag) {
                this.holdCheckedColumns.push(event.args.item);
                let holdValueIndex:any;
                for (let index = 0; index < this.holdUncheckedColumns.length; index++) {
                    
                    if(event.args.item.label ==this.holdUncheckedColumns[index].label) {
                        holdValueIndex = index;
                        break;
                    }
                    
                }
                this.holdUncheckedColumns.splice(holdValueIndex,1);
            }
            
            

        } else {
            var index = this.selectedCheckbox.indexOf(event.args.value);
            this.selectedCheckbox.splice(index, 1);

            if(this.reorderFlag) {
                
                this.holdUncheckedColumns.push(event.args.item);
                let holdValueIndex:any;
                for (let index = 0; index < this.holdCheckedColumns.length; index++) {
                    
                    if(event.args.item.label ==this.holdCheckedColumns[index].label) {
                        holdValueIndex = index;
                        break;
                    }
                    
                }
                this.holdCheckedColumns.splice(holdValueIndex,1);
            }
            

        }


        const cur_val = event.args.value;
        let colg = [];
        if (!event.args.checked) {
            this.columngroups.forEach(function (item, index) {
                if (cur_val === item.name) {
                    let grps = item.groups;
                    grps.forEach(function (itemi, indexi) {
                        if (itemi.text !== 'Event Code' && itemi.text !== 'Highlight Revised Date') {
                            colg.push(itemi.datafield);
                        }
                    });
                    return;
                }
            });
            if (colg.length > 0) {
                this.showhideColumns(this.psrGrid, colg, true);
            } else {
                this.psrGrid.showcolumn(event.args.value);
            }
        } else {
            this.columngroups.forEach(function (item, index) {
                if (cur_val === item.name) {
                    let grps = item.groups;
                    grps.forEach(function (itemi, indexi) {
                        if (itemi.text !== 'Event Code' && itemi.text !== 'Highlight Revised Date') {
                            colg.push(itemi.datafield);
                        }
                    });
                    return;
                }
            });
            if (colg.length > 0) {
                this.showhideColumns(this.psrGrid, colg, false);
            } else {
                this.psrGrid.hidecolumn(event.args.value);
            }
        }
        if (this.flagCheck) {
            this.psrGrid.endupdate();
        }
    };

    showhideColumns(psrt: any, col_array: any, is_show: boolean): void {
        if (is_show) {
            col_array.forEach(function (item, index) {
                psrt.showcolumn(item);
            });
        } else {
            col_array.forEach(function (item, index) {
                psrt.hidecolumn(item);
            });
        }
    };

    customFilter = (): void => {
        this.psrGrid.clearfilters();
        let filtergroup = new jqx.filter();
        const filtercondition = 'contains';
        let filter_and_operator = 1;
        let filtervalue = '';
        let filter = null;
        let data = [];
        let counter = 0;
        for (var prop in this.filterArr) {
            if (!this.filterArr.hasOwnProperty(prop)) continue;
            filtervalue = this.filterArr[prop];
            if (filtervalue.length > 0) {
                filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
                filtergroup.addfilter(filter_and_operator, filter);
                this.psrGrid.addfilter(this.gridFilterName[prop], filtergroup);
                // console.log(prop);
            }
        }
        this.psrGrid.applyfilters();
    };

    setVendorID(event: any): void {
        // this.code_operator = event.target.value;
    };

    setFilterValue(event: any): void {
        const filterId = event.target.id;
        this.filterArr[filterId] = event.target.value;
        if (event.target.id === 'ndcdate') {
            this.searchFormModel.ndcdate = event.target.value;
        } else if (event.target.id === 'vpoNltGacDate') {
            this.searchFormModel.vpoNltGacDate = event.target.value;
        } else if (event.target.id === 'vpoFtyGacDate') {
            this.searchFormModel.vpoFtyGacDate = event.target.value;
        }
    };

    onDateChanged(event, id) {
        // event properties are: event.date, event.jsdate, event.formatted and event.epoc
        const sdate = event.formatted;
        if (id === 'ndcdate') {
            this.searchFormModel.ndcdate = sdate;
        } else if (id === 'vpoNltGacDate') {
            this.searchFormModel.vpoNltGacDate = sdate;
        } else if (id === 'vpoFtyGacDate') {
            this.searchFormModel.vpoFtyGacDate = sdate;
        }
    };

    //CHG0016311 Rupan Start
    onDateRangeChnage(event, id) {
        if (id === 'ndcdaterange') {
            this.searchFormModel.ndcdate = "'" + moment(event.value[0]).format('MM/DD/YYYY') + "' AND '" + moment(event.value[1]).format('MM/DD/YYYY') + "'";
        } else if (id === 'vpoNltGacDateRange') {
            this.searchFormModel.vpoNltGacDate = "'" + moment(event.value[0]).format('MM/DD/YYYY') + "' AND '" + moment(event.value[1]).format('MM/DD/YYYY') + "'";
        } else if (id === 'vpoFtyGacDateRange') {
            this.searchFormModel.vpoFtyGacDate = "'" + moment(event.value[0]).format('MM/DD/YYYY') + "' AND '" + moment(event.value[1]).format('MM/DD/YYYY') + "'";
        }
    }
    //CHG0016311 Rupan End

    clearFiltering(): void {
        this.psrGrid.clearfilters();
        var cls = document.getElementsByClassName('cmn-cls-input-fld-srch');
        let t = null;
        for (let i = 0; i < cls.length; i++) {
            t = cls.item(i);
            t.value = '';
        }
    };

    /* autoResize function starts here  */
    autoResize(): void {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot resize data", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            this.psrGrid.beginupdate();
            this.psrGrid.autoresizecolumns();
            this.psrGrid.setcolumnproperty('image', 'width', 75);
            this.psrGrid.setcolumnproperty('vpo', 'width', 167);
            this.psrGrid.setcolumnproperty('ndcDate', 'width', '7%');
            this.psrGrid.setcolumnproperty('plShipDate', 'width', '8%');
            this.psrGrid.setcolumnproperty('styleDesc', 'width', '13%');
            this.psrGrid.setcolumnproperty('colorWashName', 'width', '13%');
            this.psrGrid.setcolumnproperty('keyItem', 'width', '13%');
            this.psrGrid.setcolumnproperty('shipTo', 'width', '6%');
            this.psrGrid.setcolumnproperty('cpo', 'width', '6%');
            this.psrGrid.setcolumnproperty('dept', 'width', '6%');
            this.psrGrid.setcolumnproperty('merchant', 'width', '16%');
            this.psrGrid.setcolumnproperty('gender', 'width', '6%');
            this.psrGrid.setcolumnproperty('washYn', 'width', '5%');
            this.psrGrid.setcolumnproperty('productType', 'width', '8%');
            this.psrGrid.setcolumnproperty('graphicName', 'width', '10%');
            this.psrGrid.setcolumnproperty('ticketStyleNumeric', 'width', '12%');
            this.psrGrid.setcolumnproperty('comment', 'width', '22%');
            this.psrGrid.setcolumnproperty('ticketColorCode', 'width', '10%');
            this.psrGrid.setcolumnproperty('fabricCode', 'width', '10%');
            this.psrGrid.setcolumnproperty('soId', 'width', '6%');
            this.psrGrid.setcolumnproperty('fiberContent', 'width', '10%');
            this.psrGrid.setcolumnproperty('fabricMill', 'width', '10%');
            this.psrGrid.setcolumnproperty('plmNo', 'width', '20%');
            this.psrGrid.setcolumnproperty('colorCodePatrn', 'width', '13%');
            this.psrGrid.setcolumnproperty('custFtyId', 'width', '13%');
            this.psrGrid.setcolumnproperty('cutApprLetter', 'width', '13%');
            this.psrGrid.setcolumnproperty('fabTrimOrder', 'width', '13%');
            this.psrGrid.setcolumnproperty('factoryRef', 'width', '13%');
            this.psrGrid.setcolumnproperty('knitting', 'width', '8%');
            this.psrGrid.setcolumnproperty('linking', 'width', '8%');
            this.psrGrid.setcolumnproperty('knittingPcs', 'width', '7%');
            this.psrGrid.setcolumnproperty('style', 'width', '8%');
            this.psrGrid.setcolumnproperty('fob', 'width', '6%');
            this.psrGrid.setcolumnproperty('margin', 'width', '6%');
            this.psrGrid.setcolumnproperty('cpoColorDesc', 'width', '15%');
            this.psrGrid.setcolumnproperty('topSampleComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('fabricComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('fabricTestComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('garmentTestComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('trimComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('sampleComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('fitSampleComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('ppSampleComment', 'width', '22%');
            this.psrGrid.setcolumnproperty('colorComment', 'width', '22%');
            this.psrGrid.endupdate();
            this.toastr.successToastr('Auto resize Successfully', 'Success!');
        }
    }
    /* autoResize function ends here */

    /* post logic starts here */
    generateXml(): void {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot post data", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            var isOk = confirm(" Hope you have saved the data before hitting the post data. ");
            if (isOk != undefined && isOk != null && isOk === true) {
                for (let i = 0; i < this.psrGrid.getrows().length; i++) {
                    let row = this.psrGrid.getrowdata(i);
                    if (row.select !== undefined && row.select !== null && row.select !== '' && row.select === true) {
                        let vpo = this.psrGrid.getcellvalue(row, 'vpo');
                        let vpoLineStatus = this.psrGrid.getcellvalue(row, 'vpoLineStatus');
                        let plActualGac = this.psrGrid.getcellvalue(row, 'plActualGac');
                        let GacByVendorDate = this.psrGrid.getcellvalue(row, 'GAC by VendorRevisedDate');
                        let VpoNltGacDate = this.psrGrid.getcellvalue(row, 'vpoNltGacDate');
                        if (VpoNltGacDate !== undefined && VpoNltGacDate !== null && VpoNltGacDate !== '' && GacByVendorDate !== undefined && GacByVendorDate !== null && GacByVendorDate !== '') {
                            let GACrevisedDate = ("0" + (GacByVendorDate.getMonth() + 1)).slice(-2) + '/' + (("0" + (GacByVendorDate.getDate())).slice(-2)) + '/' + GacByVendorDate.getFullYear();
                            let vpoNltGacDate = ("0" + (VpoNltGacDate.getMonth() + 1)).slice(-2) + '/' + (("0" + (VpoNltGacDate.getDate())).slice(-2)) + '/' + this.psrGrid.getcellvalue(row, 'vpoNltGacDate').getFullYear();
                            var GACrevDate = new Date(GACrevisedDate);
                            var vpoNltGacDatetmp = new Date(vpoNltGacDate);
                            if (GACrevDate > vpoNltGacDatetmp) {
                                this.toastr.errorToastr('For VPO : ' + vpo + ' GAC by Vendor Revised Date is Greater than Vpo Nlt Gac Date. Hence data cannot be posted to Bamboorose.', 'Error!', { dismiss: 'click', showCloseButton: true });
                                return;
                            }
                            else if (vpoLineStatus !== 'OPEN') {
                                this.toastr.errorToastr('For VPO : ' + vpo + ' Vpo Line Status is not open. Hence data cannot be posted to Bamboorose.', 'Error!', { dismiss: 'click', showCloseButton: true });
                                return;
                            }
                            else if (plActualGac !== null && plActualGac !== '' && plActualGac !== 'null') {
                                this.toastr.errorToastr('For VPO : ' + vpo + ' Pl Actual Gac is not null. Hence data cannot be posted to Bamboorose.', 'Error!', { dismiss: 'click', showCloseButton: true });
                                return;
                            } else {
                                continue;
                            }
                        }
                    }
                }
                let rowValues = new Array();
                let flag = false;
                for (let i = 0; i < this.psrGrid.getrows().length; i++) {
                    let row = this.psrGrid.getrowdata(i);
                    if (row.select !== undefined && row.select !== null && row.select !== '' && row.select === true) {
                        flag = true;
                        for (let index = 0; index <= this.columngroups.length; index++) {
                            let eventCode = this.columngroups[index];
                            if (flag && eventCode !== undefined && eventCode !== null && eventCode !== '') {
                                if (eventCode.name !== undefined && eventCode.name !== null && eventCode.name !== "") {
                                    let eventCodeE000 = this.psrGrid.getcellvalue(i, eventCode.name + 'eventCode');
                                    //console.log("=================="+eventCodeE000) ;                
                                    if (eventCodeE000 !== undefined && eventCodeE000 !== null && eventCodeE000 !== '' && eventCodeE000 === 'E000') {
                                        rowValues.push(
                                            {
                                                "shipTo": row.shipTo,
                                                "colorWashName": row.colorWashName,
                                                "planId": row.planId,
                                                "profomaPo": row.profomaPo,
                                                "revisedDateStr": this.convertDateFormat(this.psrGrid.getcellvalue(i, eventCode.name + 'RevisedDate')),
                                                "psrOrderId": row.psrOrderId
                                            });
                                    }
                                }
                            }
                        }
                    }
                }
                if (!flag) {
                    this.toastr.infoToastr('Please select row', 'Info!', { dismiss: 'click', showCloseButton: true });
                    return;
                }
                // Server Call to input revised date and generate XML  Starts     
                let t = this.api.generateXML(rowValues);
                this.progress.start();
                t.subscribe(res => {
                    this.psrRes = res;
                    this._global.elementDisabled = false;
                    if (this.psrRes.type === 'error') {
                        this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                        this.progress.complete();
                    } else {
                        // refresh the grid with latest update
                        this.onFormSubmit(1);
                        this.toastr.successToastr(this.psrRes.message, 'Success!');
                        this.progress.complete();
                    }
                }, error => {
                    this.toastr.errorToastr("error", "Error", { dismiss: 'click', showCloseButton: true });
                    this.progress.complete();
                    this._global.elementDisabled = false;
                }
                );
            }
        }
    }
    /* post logic End */

    /* recalculation logic starts here */
    doRecalc(): void {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot recalc data", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            this.progress.start();
            let orderIds = new Array();
            let flag = false;
            for (let i = 0; i < this.psrGrid.getrows().length; i++) {

                let row = this.psrGrid.getrowdata(i);
                if (row.select !== undefined && row.select !== null && row.select !== '' && row.select === true) {
                    orderIds.push(row.psrOrderId);
                    flag = true;
                }
            }
            if (!flag) {
                this.toastr.infoToastr('Please select row', 'Info!', { dismiss: 'click', showCloseButton: true });
                return;
            }
            // Server Call to update data from GUI Starts
            let t = this.api.doRecalc(orderIds);
            t.subscribe(res => {
                this.psrRes = res;
                if (this.psrRes.type === 'error') {
                    this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                } else {
                    this.toastr.successToastr(this.psrRes.message, 'Success!');
                }
                // refresh the grid with latest update
                this.onFormSubmit(1);
                this.progress.complete();
                this._global.elementDisabled = false;
            }, error => {
                this.toastr.errorToastr("Error", "Error", { dismiss: 'click', showCloseButton: true });
                this.progress.complete();
                this._global.elementDisabled = false;
            });
            // Server Call to update data from GUI Ends
        }
    }

    /* recalculation logic ends here */


    doSave(): void {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot save data", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            if (this.editedRowPsrId.length > 0) {
                let photo;
                if (this.isValid === false) {
                    this.toastr.warningToastr("Please rectify the errors to save the Data", "Alert!", { dismiss: 'click', showCloseButton: true });
                } else {
                    this.progress.start();
                    this._global.elementDisabled = true;
                    let rowValues = new Array();
                    let orders = new Array();
                    let events = new Array();
                    for (let i = 0; i < this.psrGrid.getrows().length; i++) {
                        let row = this.psrGrid.getrowdata(i);
                        if (this.editedRowPsrId.indexOf(row.psrOrderId) > -1) {
                            if (document.getElementById("image-" + i) !== null) {
                                photo = document.getElementById("image-" + i);
                                row.image = photo.getAttribute('data-filename');
                                row.imageType = photo.getAttribute('src');
                            }
                            orders.push(
                                {
                                    "psrOrderId": row.psrOrderId,
                                    "comment": row.comment,
                                    "custFtyId": row.custFtyId,
                                    "factoryRef": row.factoryRef,
                                    "fabricCode": row.fabricCode,
                                    "fiberContent": row.fiberContent,
                                    "fabricMill": row.fabricMill,
                                    "fabTrimOrder": row.fabTrimOrder,
                                    "colorCodePatrn": row.colorCodePatrn,
                                    "graphicName": row.graphicName,
                                    "orderType": row.orderType,
                                    "ndcWk": row.ndcWk,
                                    "apprFabricYarnLot": row.apprFabricYarnLot,
                                    "extraProcess": row.extraProcess,
                                    "cpoPackType": row.cpoPackType,
                                    "actualPoundageAvailable": row.actualPoundageAvailable,
                                    "cutApprLetter": row.cutApprLetter,
                                    "careLabelCode": row.careLabelCode,
                                    "gauge": row.gauge,
                                    "knittingPcs": row.knittingPcs,
                                    "knitting": row.knitting,
                                    "knittingMachines": row.knittingMachines,
                                    "linkingPcs": row.linkingPcs,
                                    "linking": row.linking,
                                    "sewingLines": row.sewingLines,
                                    "linkingMachines": row.linkingMachines,
                                    "dailyOutputPerLine": row.dailyOutputPerLine,
                                    "fabricEstDateStr": this.convertDateFormat(row.fabricEstDate),
                                    "fabricTestReportNumeric": row.fabricTestReportNumeric,
                                    "garmentTestActualStr": this.convertDateFormat(row.garmentTestActualStr),
                                    "packType": row.packType,
                                    "packagingReadyDateStr": this.convertDateFormat(row.packagingReadyDateStr),
                                    "ticketColorCode": row.ticketColorCode,
                                    "ticketStyleNumeric": row.ticketStyleNumeric,
                                    "garmentTestReportNo": row.garmentTestReportNo,
                                    "plmNo": row.plmNo,
                                    "washYn": row.washYn,
                                    "washFacility": row.washFacility,
                                    "fabricShipMode": row.fabricShipMode,
                                    "techPackReceivedDateStr": this.convertDateFormat(row.techPackReceivedDateStr),
                                    "sampleMaterialStatus": row.sampleMaterialStatus,
                                    "retailPrice": row.retailPrice,
                                    "fabricPpDateStr": this.convertDateFormat(row.fabricPpDateStr),
                                    "deliveryMonth": row.deliveryMonth,
                                    "cpoAccDateByVendorStr": this.convertDateFormat(row.cpoAccDateByVendorStr),
                                    "distChannel": row.distChannel,
                                    "floorSet": row.floorSet,
                                    "styleAtRisk": row.styleAtRisk,
                                    "sampleMerchEtaStr": this.convertDateFormat(row.sampleMerchEtaStr),
                                    "sampleFloorSetEtaStr": this.convertDateFormat(row.sampleFloorSetEtaStr),
                                    "sampleDcomEtaStr": this.convertDateFormat(row.sampleDcomEtaStr),
                                    "sampleMailer": row.sampleMailer,
                                    "sampleMailerEtaStr": this.convertDateFormat(row.sampleMailerEtaStr),
                                    "topSampleEtaDateStr": this.convertDateFormat(row.topSampleEtaDateStr),
                                    "topSampleComment": row.topSampleComment,
                                    "photoMerchantSampleSendDateStr": this.convertDateFormat(row.photoMerchantSampleSendDateStr),
                                    "photoMerchantSampleEtaDateStr": this.convertDateFormat(row.photoMerchantSampleEtaDateStr),
                                    "photoMerchantSampleComment": row.photoMerchantSampleComment,
                                    "marketingSampleSendDateStr": this.convertDateFormat(row.marketingSampleSendDateStr),
                                    "marketingSampleEtaDateStr": this.convertDateFormat(row.marketingSampleEtaDateStr),
                                    "marketingSampleComment": row.marketingSampleComment,
                                    "visualSampleSendDateStr": this.convertDateFormat(row.visualSampleSendDateStr),
                                    "visualSampleEtaDateStr": this.convertDateFormat(row.visualSampleEtaDateStr),
                                    "visualSampleComment": row.visualSampleComment,
                                    "copyrightSampleSendDateStr": this.convertDateFormat(row.copyrightSampleSendDateStr),
                                    "copyrightSampleEtaDateStr": this.convertDateFormat(row.copyrightSampleEtaDateStr),
                                    "copyrightSampleComment": row.copyrightSampleComment,
                                    "reasonForLateGac": row.reasonForLateGac,
                                    "additionalBulkLotApproveStr": this.convertDateFormat(row.additionalBulkLotApproveStr),
                                    "colorComment": row.colorComment,
                                    "fabricComment": row.fabricComment,
                                    "fabricTestComment": row.fabricTestComment,
                                    "garmentTestComment": row.garmentTestComment,
                                    "trimComment": row.trimComment,
                                    "sampleComment": row.sampleComment,
                                    "fitSampleComment": row.fitSampleComment,
                                    "ppSampleComment": row.ppSampleComment,
                                    "garmentTreatmentPcs": row.garmentTreatmentPcs,
                                    "finishedGarmentPcs": row.finishedGarmentPcs,
                                    "packedUnitsPcs": row.packedUnitsPcs,
                                    "image": row.image,
                                    "imageType": row.imageType

                                });
                            for (let index = 0; index <= this.columngroups.length; index++) {
                                let eventCode = this.columngroups[index];
                                if (eventCode !== undefined && eventCode !== null && eventCode !== '') {
                                    if (eventCode.name !== undefined && eventCode.name !== null && eventCode.name !== "") {
                                        if (this.editedMilestoneField.indexOf(eventCode.name + '+' + row.psrOrderId) > -1) {
                                            events.push({
                                                "psrOrderId": row.psrOrderId,
                                                "eventCode": this.psrGrid.getcellvalue(i, eventCode.name + 'eventCode'),
                                                "actualDateStr": this.convertDateFormat(this.psrGrid.getcellvalue(i, eventCode.name + 'ActualDate')),
                                                "revisedDateStr": this.convertDateFormat(this.psrGrid.getcellvalue(i, eventCode.name + 'RevisedDate'))
                                            });
                                        }

                                    }
                                }
                            }
                        }
                    }
                    rowValues.push({ "orders": orders, "events": events });
                    // Server Call to update data from GUI Starts
                    let t = this.api.updateOrderAndEventsData(rowValues, localStorage.getItem('loginUserId'), localStorage.getItem('roleName'));
                    t.subscribe(res => {
                        this.psrRes = res;
                        if (this.psrRes.type != null && this.psrRes.type === 'error') {
                            this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                        } else {
                            this.saveData = true;
                            this.toastr.successToastr(this.psrRes.message, 'Success!');
                        }
                        // refresh the grid with latest update
                        this.onFormSubmit(1);
                        this.progress.complete();
                        this._global.elementDisabled = false;
                    }, error => {
                        this.toastr.errorToastr("Error", "Error", { dismiss: 'click', showCloseButton: true });
                        this.progress.complete();
                        this._global.elementDisabled = false;
                    });
                    // Server Call to update data from GUI Ends
                    this.editedRowPsrId = [];
                    this.editedMilestoneField = [];
                    this.getCellValueChanged = null;
                }


            } else {
                this.toastr.warningToastr("You have not edited anything", "Alert!", { dismiss: 'click', showCloseButton: true });

            }
        }

    };

    public convertDateFormat(date): string {
        if (date !== undefined && date !== null && date !== '') {
            // console.log(' year --> '+date.getFullYear()+' month --> '+date.getMonth()+' day --> '+date.getDate() );
            let mon = date.getMonth() + 1;
            return date.getFullYear() + '-' + mon + '-' + date.getDate();
        }
    }
    public compareFields(a, b) {
        if (a.label < b.label)
            return -1;
        if (a.label > b.label)
            return 1;
        return 0;
    }



    onChange(id): void {
        const inputElement = <HTMLInputElement>document.getElementById(id);
        let ngModelID = id;

        //Rupan -> CHG0017385 - (PSR) "Between" Operator Refresh issue prod fix (start)
        if (id = 'ndcdate') {
            this.searchFormModel.ndcdate = '';
        }
        if (id = 'vpoNltGacDate') {
            this.searchFormModel.vpoNltGacDate = '';
        }
        if (id = 'vpoFtyGacDate') {
            this.searchFormModel.vpoFtyGacDate = '';
        }
        //Rupan -> CHG0017385 - (PSR) "Between" Operator Refresh issue prod fix  (end)

        if (id == 'styleDescription') {
            id = 'styleDesc';
        } else if (id == 'productionOffice') {
            id = 'prodOffice';
        }
        var selectElement = <HTMLSelectElement>document.getElementById(id + 'Select');

        if (selectElement.value === 'Is not null' || selectElement.value === 'Equal to null') {
            inputElement.disabled = true;
            this.isValidCheck = false;
            this.searchFormModel[ngModelID] = null;
        } else {

            inputElement.disabled = false;
            if (id == 'vpoStatus') {
                if (selectElement.value == 'Not in the list') {
                    this.searchFormModel[ngModelID] = 'VOID,CLOSED';
                } else {
                    this.searchFormModel[ngModelID] = null;
                }

            } else if (id == 'vpoLineStatus') {
                if (selectElement.value == 'Not in the list') {
                    this.searchFormModel[ngModelID] = 'VOID,CLOSED,CWOLIAB,CNCLIAB';
                } else {
                    this.searchFormModel[ngModelID] = null;
                }
            }
        }

    }


    resetForm(): void {

        var inputFields = document.getElementById("searchFieldDivCont").getElementsByTagName('input');
        var selectFields = document.getElementById("searchFieldDivCont").getElementsByTagName('select');

        for (var i = 0; i < inputFields.length; i++) {
            if (inputFields[i].id != 'ndcdate' && inputFields[i].id != 'vpoNltGacDate' && inputFields[i].id != 'vpoFtyGacDate') {
                inputFields[i].disabled = false;
                if (inputFields[i].id == 'vpoStatus') {
                    this.searchFormModel.vpoStatus = 'VOID,CLOSED';
                } else if (inputFields[i].id == 'vpoLineStatus') {
                    this.searchFormModel.vpoLineStatus = 'VOID,CLOSED,CWOLIAB,CNCLIAB';
                } else {
                    this.searchFormModel[inputFields[i].id] = '';
                }
            }


        }

        for (var i = 0; i < selectFields.length; i++) {

            if (selectFields[i].id == 'ndcdateSelect' || selectFields[i].id == 'vpoFtyGacDateSelect' || selectFields[i].id == 'vpoNltGacDateSelect') {
                this.searchFormModel[selectFields[i].id] = 'Greater';
            } else if (selectFields[i].id == 'vpoStatusSelect' || selectFields[i].id == 'vpoLineStatusSelect') {
                this.searchFormModel[selectFields[i].id] = 'Not in the list';
            } else {
                this.searchFormModel[selectFields[i].id] = 'Like';
            }

        }
        //CHG0016311 Rupan Start
        this.searchFormModel.ndcdate = '';
        this.searchFormModel.vpoNltGacDate = '';
        this.searchFormModel.vpoFtyGacDate = '';
        //CHG0016311 Rupan End
    }

    returnSearchFieldsList(page) {

        this.currentPage = page;
        page = page - 1;

        let searchFieldData = environment.apiUrl + '/psrDynamicData?vpo='
            + this.searchFormModel.vpo
            + '&productionOffice=' + this.searchFormModel.productionOffice
            + '&vendorId=' + this.searchFormModel.vendorId
            + '&ocCommit=' + this.searchFormModel.ocCommit
            + '&factoryId=' + this.searchFormModel.factoryId
            + '&factoryName=' + this.searchFormModel.factoryName
            + '&countryOrigin=' + this.searchFormModel.countryOrigin
            + '&brand=' + this.searchFormModel.brand
            + '&style=' + this.searchFormModel.style
            + '&styleDescription=' + this.searchFormModel.styleDescription
            + '&cpo=' + this.searchFormModel.cpo
            + '&dept=' + this.searchFormModel.dept
            + '&season=' + this.searchFormModel.season
            + '&colorWashName=' + this.searchFormModel.colorWashName
            + '&ndcdate=' + this.searchFormModel.ndcdate
            + '&vpoNltGacDate=' + this.searchFormModel.vpoNltGacDate
            + '&vpoFtyGacDate=' + this.searchFormModel.vpoFtyGacDate
            + '&ndcWk=' + this.searchFormModel.ndcWk
            + '&vpoStatus=' + this.searchFormModel.vpoStatus
            + '&vpoLineStatus=' + this.searchFormModel.vpoLineStatus
            + '&category=' + this.searchFormModel.category
            + '&cpoColorDesc=' + this.searchFormModel.cpoColorDesc
            + '&cpoDept=' + this.searchFormModel.cpoDept
            + '&initFlow=' + this.searchFormModel.initFlow
            + '&vposelect=' + this.searchFormModel.vpoSelect
            + '&vendorIdSelect=' + this.searchFormModel.vendorIdSelect
            + '&ocCommitSelect=' + this.searchFormModel.ocCommitSelect
            + '&factoryIdSelect=' + this.searchFormModel.factoryIdSelect
            + '&factoryNameSelect=' + this.searchFormModel.factoryNameSelect
            + '&brandSelect=' + this.searchFormModel.brandSelect
            + '&styleSelect=' + this.searchFormModel.styleSelect
            + '&styleDescSelect=' + this.searchFormModel.styleDescSelect
            + '&prodOfficeSelect=' + this.searchFormModel.prodOfficeSelect
            + '&countryOriginSelect=' + this.searchFormModel.countryOriginSelect
            + '&milestoneselect=' + this.searchFormModel.milestoneselect
            + '&cpoSelect=' + this.searchFormModel.cpoSelect
            + '&deptSelect=' + this.searchFormModel.deptSelect
            + '&seasonSelect=' + this.searchFormModel.seasonSelect
            + '&colorWashNameSelect=' + this.searchFormModel.colorWashNameSelect
            + '&ndcdateSelect=' + this.searchFormModel.ndcdateSelect
            + '&vpoNltGacDateSelect=' + this.searchFormModel.vpoNltGacDateSelect
            + '&vpoFtyGacDateSelect=' + this.searchFormModel.vpoFtyGacDateSelect
            + '&ndcWkSelect=' + this.searchFormModel.ndcWkSelect
            + '&vpoStatusSelect=' + this.searchFormModel.vpoStatusSelect
            + '&vpoLineStatusSelect=' + this.searchFormModel.vpoLineStatusSelect
            + '&categorySelect=' + this.searchFormModel.categorySelect
            + '&cpoColorDescSelect=' + this.searchFormModel.cpoColorDescSelect
            + '&cpoDeptSelect=' + this.searchFormModel.cpoDeptSelect
            + '&initFlowSelect=' + this.searchFormModel.initFlowSelect
            + '&authentication=' + localStorage.getItem('authentication')
            + '&loginUserId=' + localStorage.getItem('loginUserId')
            + '&brUserId=' + localStorage.getItem('brUserId')
            + '&roleName=' + localStorage.getItem('roleName')
            + '&pageNo=' + page;

        return searchFieldData;
    }

    onFormSubmit(page: number): void {
        this.getCellValueChanged = null;
        this.allowCheckStatus = false;
        if (this.availableViews.length > 0) {
            this.gridStatus = true;
            const testArray: any[] = new Array();
            let milstoneList = new Array();
            this.scrollList = []
            for (let index = 1; index < this.temporaryColumn.length; index++) {

                if (this.temporaryColumn[index].columngroup) {

                    if (milstoneList.indexOf(this.temporaryColumn[index].columngroup) == -1) {
                        milstoneList.push(this.temporaryColumn[index].columngroup);
                        testArray.push({ label: this.temporaryColumn[index].columngroup, value: this.temporaryColumn[index].columngroup, milestone: true });
                        this.scrollList.push(this.temporaryColumn[index].columngroup);
                    }
                } else {
                    if (this.roleName == 'PSR_VENDOR') {

                        if (!this.temporaryColumn[index].customhidden && this.temporaryColumn[index].datafield != 'ndcDate' && this.temporaryColumn[index].datafield != 'vpoNltGacDate' && this.temporaryColumn[index].datafield != 'salesAmount' && this.temporaryColumn[index].datafield != 'margin' && this.temporaryColumn[index].datafield != 'grossSellPrice') {
                            testArray.push({ label: this.temporaryColumn[index].text, value: this.temporaryColumn[index].datafield });
                            this.scrollList.push(this.temporaryColumn[index].text);
                        }

                    } else {
                        if (!this.temporaryColumn[index].customhidden
                        ) {
                            testArray.push({ label: this.temporaryColumn[index].text, value: this.temporaryColumn[index].datafield });
                            this.scrollList.push(this.temporaryColumn[index].text);
                        }
                    }

                }
            }
            this.holdlistOriginal = testArray;
           this.listBoxSource = testArray;
           //this.holdlistOriginal = this.listBoxSource;
           // this.listBoxSource.sort(this.compareFields);
            if (this.isValidCheck === true) {
                if ((this.searchFormModel.vpo === '') && (this.searchFormModel.factoryId === '') &&
                    (this.searchFormModel.factoryName === '') && (this.searchFormModel.vendorId === '') &&
                    (this.searchFormModel.ocCommit === '') && (this.searchFormModel.style === '') &&
                    (this.searchFormModel.styleDescription === '') && (this.searchFormModel.productionOffice === '') &&
                    (this.searchFormModel.cpo === '') && (this.searchFormModel.season === '') &&
                    (this.searchFormModel.dept === '') && (this.searchFormModel.ndcdate === '') &&
                    (this.searchFormModel.vpoNltGacDate === '') && (this.searchFormModel.vpoFtyGacDate === '') &&
                    (this.searchFormModel.colorWashName === '') && (this.searchFormModel.countryOrigin === '') &&
                    (this.searchFormModel.brand === '') && (this.searchFormModel.ndcWk === '')  &&
                    (this.searchFormModel.vpoStatus === '') && (this.searchFormModel.vpoLineStatus === '') && 
                    (this.searchFormModel.cpoDept === '') && (this.searchFormModel.initFlow === '')) {
                    this.toastr.infoToastr('Please enter search criteria.', 'Info!', { dismiss: 'click', showCloseButton: true });
                } else {
                    this.showHideHeader = true;
                    this.source.url = this.returnSearchFieldsList(page);
                    this.psrGrid.updatebounddata();
                }
            } else {
                this.showHideHeader = true;
                this.source.url = this.returnSearchFieldsList(page);
                this.psrGrid.updatebounddata();
            }
            let queryName = localStorage.getItem('queryName');
            if (queryName !== undefined && queryName !== '' && queryName !== null && queryName !== "null" && this.publicviewdropdown.length == 0) {
                this.publicviewdropdown = [{ text: queryName }];
                const defaultView = (document.getElementById("default-view")) as HTMLInputElement;
                defaultView.checked = true;
                this.onChangeViews(queryName, 'search_event');
            } else {
                if ((queryName == 'null' || queryName == null || queryName == '') && (this.publicviewdropdown.length == 0)) {
                    this.onChangeViews('', 'search_event');
                } else {
                    this.onChangeViews(this.publicviewdropdown[0].text, 'search_event');
                }

            }
        } else {
            this.toastr.infoToastr('Available View is not loaded, Please wait for a moment', 'Info!');
        }

    };

    onChangeViews(value, flag): void {
        this.reorderFlag = false;
        this.selectedCheckbox = [];
        // this.holdCheckedColumns = [];
        // this.holdUncheckedColumns = [];
        this.status = 'All';
        this.getCellValueChanged = null;
        let queryName = localStorage.getItem('queryName');
        let getAvailableviewindex = this.availableViewlablelist.indexOf(value);
        this.availableviewid = this.availableViewidlist[getAvailableviewindex];
        const defaultView = (document.getElementById("default-view")) as HTMLInputElement;
        let flag_value;
        let psr_grid_length = this.psrGrid.getrows().length;
        if (flag == 'search_event') {
            flag_value = false;
        } else if (flag == 'change_view') {
            if (psr_grid_length === 0) {
                flag_value = true;
            }
            else {
                flag_value = false;
            }
        }
        if (flag_value) {
            this.toastr.infoToastr('Please click on search, then change the view', 'Info!', { dismiss: 'click', showCloseButton: true });
            this.null_increment_counter();
            return;
        } else {
            if (queryName != '') {
                if (queryName == value) {
                    defaultView.checked = true;
                } else {
                    defaultView.checked = false;
                }
            }
            this.query.queryName = value;
            this.query.checkboxValue = false;
            if (this.checkpublicviewstatus[getAvailableviewindex] == '1') {
                this.query.checkboxValue = true;
                this.StatusPublicView = true;
                if (this.roleName != 'PSR_ADMIN') {
                    this.hideUpdateDelete = true;
                }

            } else {
                this.StatusPublicView = false;
                if (this.roleName != 'PSR_ADMIN') {
                    this.hideUpdateDelete = false;
                }
            }
            if (flag == 'change_view') {
                this.progress.start();
                this.columns = this.temporaryColumn;
                //this.listBoxSource = this.holdlistOriginal;
            }

            this.reArrangeColumn(value);
            // this._global.elementDisabled = true;
            this.api.getSelectedColumn(value).subscribe(
                res => {

                    
                    //  this._global.elementDisabled = false;
                    this.psrCheckBox.refresh();
                    this.psrGrid.beginupdate();
                    if (flag == 'change_view') {
                        this.getReorder();
                        this.progress.complete();
                    }
                    let temp_data = [];
                    if (this.roleName === 'PSR_VENDOR') {
                        this.psrGrid.hidecolumn('ndcDate');
                        this.psrGrid.hidecolumn('vpoNltGacDate');
                        this.psrGrid.hidecolumn('salesAmount');
                        this.psrGrid.hidecolumn('margin');
                        this.psrGrid.hidecolumn('grossSellPrice');
                    }
                    this.listBoxSource.forEach(function (item, index) {
                        if (!item.milestone) {
                            temp_data.push(item.value);
                        }

                    });
                    this.showhideColumns(this.psrGrid, temp_data, true);
                    for (let index_new = 0; index_new < this.getmilestonedata.length; index_new++) {
                        this.psrGrid.showcolumn(this.getmilestonedata[index_new] + 'PlannedDate');
                        this.psrGrid.showcolumn(this.getmilestonedata[index_new] + 'RevisedDate');
                        this.psrGrid.showcolumn(this.getmilestonedata[index_new] + 'ActualDate');
                    }

                    this.checkitemshowhide(this.psrCheckBox, res);
                    this.reorderFlag = true;
                    this.holdCheckednUnchecked();
                    this.psrGrid.endupdate();
                }, error => {
                    this.psrGrid.beginupdate();
                    if (this.roleName === 'PSR_VENDOR') {
                        this.psrGrid.hidecolumn('ndcDate');
                        this.psrGrid.hidecolumn('vpoNltGacDate');
                        this.psrGrid.hidecolumn('salesAmount');
                        this.psrGrid.hidecolumn('margin');
                        this.psrGrid.hidecolumn('grossSellPrice');
                    }
                    this.reorderFlag = true;
                    this.psrGrid.endupdate();
                });

        }
    }

    changeDefaultView(): void {
        let newQueryName;
        this.getCellValueChanged = null;
        if (this.publicviewdropdown.length > 0) {
            newQueryName = this.publicviewdropdown[0].text;
        } else {
            newQueryName = 'Available Views';
        }
        let existingQueryName = localStorage.getItem('queryName');
        const defaultView = (document.getElementById("default-view")) as HTMLInputElement;
        if ((existingQueryName !== newQueryName) && (newQueryName !== "Available Views")) {
            let checked = defaultView.checked;
            if (checked === true) {
                this.progress.start();
                this._global.elementDisabled = true;
                this.api.updateDefaultView(newQueryName, existingQueryName).subscribe(
                    res => {
                        this.psrRes = res;
                        if (this.psrRes !== undefined && this.psrRes !== '' && this.psrRes !== null && this.psrRes !== "null" && this.psrRes.type === 'success') {
                            localStorage.setItem('queryName', newQueryName);
                            this.progress.complete();
                            this._global.elementDisabled = false;
                            this.toastr.successToastr(this.psrRes.message, 'Success!');

                        } else {
                            this.progress.complete();
                            this._global.elementDisabled = false;
                            this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                        }
                    });

            }

        } else {
            if (newQueryName !== "Available Views") {
                defaultView.checked = true;
                this.toastr.warningToastr("This is already your default view so please select different view to make it as default.", "Alert!", { dismiss: 'click', showCloseButton: true });
            } else {
                defaultView.checked = false;
                this.toastr.infoToastr(' Please select the view..! ', 'Info!', { dismiss: 'click', showCloseButton: true });
            }
        }


    }

    changeWidth3(): void {
        this.psrGrid.width('99.3%');
    };



    checkitemshowhide(psrt: any, col_array: any): void {
        this.flagCheck = false;
        col_array.forEach(function (item, index) {
            psrt.checkItem(item);
        });
    }
    autoPopulateViews(): void {
        let availableviewdata = [];
        this.availableViews = [];
        let t = this.api.getCustomisedViews(localStorage.getItem('loginUserId'));
        t.subscribe(res => {
            for (let i = 0; i < res.length; i++) {
                this.availableViewlablelist.push(res[i][1]);
                availableviewdata.push(res[i][1]);
                this.availableViewidlist.push(res[i][0]);
                this.checkpublicviewstatus.push(res[i][2]);
            }
            this.availableViews = availableviewdata;
            //this.availableViews = availableviewdata.slice(0);
        });
    }

    checkboxlist(): void {
        this.getCellValueChanged = null;
        let disable_status = this.hideUpdateDelete;
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot hide any column", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            const queryname = this.query.queryName;
            if (queryname === undefined || queryname === '') {
                this.toastr.infoToastr('Please Enter View Name', 'Info!', { dismiss: 'click', showCloseButton: true });
            } else {
                this.progress.start();
                this._global.elementDisabled = true;
                if (disable_status == false) {
                    this.hideUpdateDelete = true;
                }
                let checkboxValue = this.query.checkboxValue;
                if (this.roleName == 'PSR_ADMIN') {
                    if (checkboxValue === undefined || checkboxValue === false) {
                        checkboxValue = 0;
                    } else {
                        checkboxValue = 1
                    }
                } else {
                    checkboxValue = 0;
                }

                let orderColumn:any = this.arrangeUpdatenSaveView();

                let t = this.api.addCustView(queryname, this.selectedCheckbox, checkboxValue,orderColumn.toString());
                t.subscribe(res => {
                    this.psrRes = res;

                    if (this.psrRes.type === 'error') {
                        this.autoPopulateViews();
                        this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                    } else {
                        this.autoPopulateViews();
                        this.toastr.successToastr(this.psrRes.message, 'Success!');
                    }
                    this.showHideClose();
                    this.progress.complete();
                    this._global.elementDisabled = false;
                    if (disable_status == false) {
                        this.hideUpdateDelete = false;
                    }
                    if (this.isIE == true) {
                        setTimeout(function () { location.reload(); }, 2000);
                    }
                }, error => {
                    this.toastr.errorToastr("Error", "Error", { dismiss: 'click', showCloseButton: true });
                    this.progress.complete();
                    this._global.elementDisabled = false;
                    if (disable_status == false) {
                        this.hideUpdateDelete = false;
                    }
                });
            }

        }
    }
    updateAvailableView(): void {
        this.getCellValueChanged = null;
        const queryname = this.query.queryName;
        let checkboxValue = this.query.checkboxValue;
        if (queryname === undefined || queryname === '') {
            this.toastr.infoToastr('Please Enter View Name', 'Info!', { dismiss: 'click', showCloseButton: true });
        } else {
            if ((checkboxValue == false) && (this.StatusPublicView == true)) {
                this.toastr.warningToastr("You cannot make public view to normal view, where this view may be assigned as default view to others", "Alert!", { dismiss: 'click', showCloseButton: true });
                const publicView = (document.getElementById("checkboxValue")) as HTMLInputElement;
                publicView.checked = true;
                this.query.checkboxValue = true;
            } else {
                this.progress.start();
                this._global.elementDisabled = true;
                this.hideUpdateDelete = true;
                let checkboxValue = this.query.checkboxValue;
                if (this.roleName == 'PSR_ADMIN') {
                    if (checkboxValue === undefined || checkboxValue === false) {
                        checkboxValue = 0;
                    } else {
                        checkboxValue = 1
                    }
                } else {
                    checkboxValue = 0;
                }

               let orderColumn:any = this.arrangeUpdatenSaveView();
               
                let t = this.api.updateCustView(this.availableviewid, this.selectedCheckbox, checkboxValue, orderColumn.toString());
                t.subscribe(res => {
                    this.psrRes = res;
                    if (this.psrRes.type == "ERROR") {
                        this.autoPopulateViews();
                        this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                        this.progress.complete();
                        this._global.elementDisabled = false;
                        this.hideUpdateDelete = false;
                    } else {
                        this.autoPopulateViews();
                        if (queryname == localStorage.getItem('queryName')) {
                            localStorage.setItem('queryName', this.psrRes.queryName)
                            this.publicviewdropdown = [{ text: this.psrRes.queryName }];
                            this.query.queryName = this.psrRes.queryName;
                        } else {
                            this.publicviewdropdown = [{ text: this.psrRes.queryName }];
                            this.query.queryName = this.psrRes.queryName;
                        }
                        this.toastr.successToastr(this.psrRes.message, 'Success!');
                        this.getReorder();
                        if (this.isIE == true) {
                            setTimeout(function () { location.reload(); }, 2000);
                        }

                    }
                    this.showHideClose();
                    this.progress.complete();
                    this._global.elementDisabled = false;
                    this.hideUpdateDelete = false;
                }, error => {
                    this.toastr.errorToastr("Error", "Error", { dismiss: 'click', showCloseButton: true });
                    this.progress.complete();
                    this._global.elementDisabled = false;
                    this.hideUpdateDelete = false;
                });

            }

        }

    }
    deleteAvailableView(): void {
        this.getCellValueChanged = null;
        const queryname = this.query.queryName;
        if (queryname === undefined || queryname === '') {
            this.toastr.infoToastr('Please Enter View Name', 'Alert!', { dismiss: 'click', showCloseButton: true });
        } else {
            if (this.StatusPublicView == true && this.roleName !== 'PSR_ADMIN') {
                this.toastr.errorToastr('You cannot delete public view!', 'Error!', { dismiss: 'click', showCloseButton: true });
            } else {
                this.progress.start();
                this._global.elementDisabled = true;
                this.hideUpdateDelete = true;
                let t = this.api.deleteCustView(this.availableviewid);
                t.subscribe(res => {
                    this.psrRes = res;
                    if (this.psrRes.type === 'error') {
                        this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                        this.progress.complete();
                        this._global.elementDisabled = false;
                        this.hideUpdateDelete = false;
                    } else {
                        this.autoPopulateViews();
                        localStorage.setItem('queryName', '');
                        this.toastr.successToastr(this.psrRes.message, 'Success!');

                    }
                    setTimeout(function () { location.reload(); }, 2000);
                }, error => {
                    this.toastr.errorToastr("Error", "Error", { dismiss: 'click', showCloseButton: true });
                    this.progress.complete();
                    this._global.elementDisabled = false;
                    this.hideUpdateDelete = false;
                });
            }
        }
    }

    showHideClose(): void {
        const close_btn: HTMLElement = document.getElementsByClassName('dialog__close-btn')[0] as HTMLElement;
        close_btn.click();
    }

    setPage(page: number): void {

        let length = parseInt(localStorage.getItem('allItems'));
        this.pager = this.pagerService.getPager(length, page);
        this.pager.currentPage = parseInt(this.pager.currentPage, 10);
        let data = [];
        let counter = 1;
        for (; counter <= this.pager.totalPages;) {
            data[counter++] = {};
        }
        this.pagedItems = data;
        const pageNumberId = <HTMLInputElement>document.getElementById('page-number-list');
        if (pageNumberId !== null) {
            pageNumberId.value = page.toString();
        }
    }

    togglePanel(): void {
        this.panelOpenState = !this.panelOpenState;
    }

    onKeyDown(event) {
        if (event.target.id === 'ndcdate' || event.target.id === 'vpoNltGacDate' || event.target.id === 'vpoFtyGacDate') {
            //this.panelOpenState = !this.panelOpenState;
            return false;
        } else if (event.keyCode === 13) {
            //this.panelOpenState = !this.panelOpenState;
            this.onFormSubmit(1);
        }
    }

    since: any=[{text:'',id:''}];
    searchKeyword: String="";
    sinceValues = ['Today','Yesterday','Last Week','Last Month','All','Custom'];
    fromDate: any="";
    toDate: any="";
    changeTracking(value) {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot perform the Action", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            const orderIds = new Array();
            let flag = false;
            for (let i = 0; i < this.psrGrid.getrows().length; i++) {
                const row = this.psrGrid.getrowdata(i);
                if (row.select !== undefined && row.select !== null && row.select !== '' && row.select === true) {
                    orderIds.push(row.psrOrderId);
                    flag = true;
                }
            }
            if (!flag) {
                this.toastr.infoToastr('Please select row', 'Info!', { dismiss: 'click', showCloseButton: true });
                return;
            }
            let t :any;
            if(value=='changeTrack'){
                this.searchKeyword ="";
                this.fromDate="";
                this.toDate="";
                
                t = this.api.getChangeTracking(localStorage.getItem('loginUserId'), orderIds, 'All', this.searchKeyword, this.fromDate
                , this.toDate);
            }
            else{
                // console.log(this.since[0].text);
                // console.log(this.searchKeyword);
                // console.log(this.fromDate);
                if(this.since[0].text=="Custom"){
                    if(this.fromDate == null || this.fromDate=="" || this.toDate==null || this.toDate ==""){
                        alert("Date fields are mandatory for Custom filter");
                        return;
                    }
                    if(this.fromDate.formatted!=undefined){
                        this.fromDate= this.fromDate.formatted;
                    }
                    if(this.toDate.formatted!=undefined){
                        this.toDate = this.toDate.formatted;
                    }
                    
                    // console.log(this.fromDate);
                    // console.log(this.toDate);
                }
                else{
                    this.fromDate="";
                    this.toDate="";
                }
                
               t = this.api.getChangeTracking(localStorage.getItem('loginUserId'), orderIds, this.since[0].text, this.searchKeyword, this.fromDate , this.toDate);
            }
            this.progress.start();
            t.subscribe(res => {
                this.changeTrackList = res;
                this.fileChangeTrackList =[];
                this.fileChangeTrackFinalList=[];
                this.sum=20;
                this.fileChangeTrackList =Object.keys(res).map((key) => [key, res[key]]);
                console.log(this.fileChangeTrackList)
                this.appendItems(0, this.sum);
                this.progress.complete();
                this.showPopup = true;
            }, error => {
                this.toastr.errorToastr("Please contact admin, An error has been occured in the system.", "Error", { dismiss: 'click', showCloseButton: true });
                this.progress.complete();
            }
            );
        }
        // this.router.navigateByUrl('/changetracking/' + orderIds, { skipLocationChange: true });
    }

    showDate:boolean=false;
    showDateFilter(value){
        console.log(value);
        if(value.text=="Custom"){
            this.showDate = true;
        }
        else{
            this.showDate=false;
        }
    }

    resetSearchFilter(){
        this.searchKeyword ="";
        this.since=[{text:'',id:''}];
        this.fromDate="";
        this.toDate="";
        this.showDateFilter("");
    }

    // start on scroll down for file track
    sum = 20;
  throttle = 100;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = "";
  fileChangeTrackFinalList=[];

    addItems(startIndex, endIndex, _method) {
        
        for (let i = startIndex; i < endIndex; ++i) {
            if(this.fileChangeTrackList[i]!=null){
                this.fileChangeTrackFinalList[_method](this.fileChangeTrackList[i]);
            }
        }
      }

    appendItems(startIndex, endIndex) {
        this.addItems(startIndex, endIndex, "push");
      }
    
      onScrollDown(ev) {
        console.log("scrolled down!!", ev);
    
        // add another 20 items
        const start = this.sum;
        this.sum += 20;
        this.appendItems(start, this.sum);
    
        this.direction = "down";
      }
      //end

      
    //export excel from PSR Grid incase required for future
    // exportAsXLSX(): void {
    //     if (this.psrGrid.getrows().length === 0) {
    //         this.toastr.warningToastr("Grid is empty, Cannot perform the Action", "Alert!", { dismiss: 'click', showCloseButton: true });
    //     }
    //     else {
    //         if (this.publicviewdropdown.length > 0) {
    //             this.progress.start();
    //             this.source.url =
    //                 'vpo=' + this.searchFormModel.vpo
    //                 + '&productionOffice=' + this.searchFormModel.productionOffice
    //                 + '&vendorId=' + this.searchFormModel.vendorId
    //                 + '&ocCommit=' + this.searchFormModel.ocCommit
    //                 + '&factoryId=' + this.searchFormModel.factoryId
    //                 + '&factoryName=' + this.searchFormModel.factoryName
    //                 + '&countryOrigin=' + this.searchFormModel.countryOrigin
    //                 + '&brand=' + this.searchFormModel.brand
    //                 + '&style=' + this.searchFormModel.style
    //                 + '&styleDescription=' + this.searchFormModel.styleDescription
    //                 + '&cpo=' + this.searchFormModel.cpo
    //                 + '&dept=' + this.searchFormModel.dept
    //                 + '&season=' + this.searchFormModel.season
    //                 + '&colorWashName=' + this.searchFormModel.colorWashName
    //                 + '&ndcdate=' + this.searchFormModel.ndcdate
    //                 + '&vpoNltGacDate=' + this.searchFormModel.vpoNltGacDate
    //                 + '&vpoFtyGacDate=' + this.searchFormModel.vpoFtyGacDate
    //                 + '&ndcWk=' + this.searchFormModel.ndcWk
    //                 + '&vpoStatus=' + this.searchFormModel.vpoStatus
    //                 + '&vpoLineStatus=' + this.searchFormModel.vpoLineStatus
    //                 + '&category=' + this.searchFormModel.category
    //                 + '&cpoColorDesc=' + this.searchFormModel.cpoColorDesc
    //                 + '&vposelect=' + this.searchFormModel.vpoSelect
    //                 + '&vendorIdSelect=' + this.searchFormModel.vendorIdSelect
    //                 + '&ocCommitSelect=' + this.searchFormModel.ocCommitSelect
    //                 + '&factoryIdSelect=' + this.searchFormModel.factoryIdSelect
    //                 + '&factoryNameSelect=' + this.searchFormModel.factoryNameSelect
    //                 + '&brandSelect=' + this.searchFormModel.brandSelect
    //                 + '&styleSelect=' + this.searchFormModel.styleSelect
    //                 + '&styleDescSelect=' + this.searchFormModel.styleDescSelect
    //                 + '&prodOfficeSelect=' + this.searchFormModel.prodOfficeSelect
    //                 + '&countryOriginSelect=' + this.searchFormModel.countryOriginSelect
    //                 + '&milestoneselect=' + this.searchFormModel.milestoneselect
    //                 + '&cpoSelect=' + this.searchFormModel.cpoSelect
    //                 + '&deptSelect=' + this.searchFormModel.deptSelect
    //                 + '&seasonSelect=' + this.searchFormModel.seasonSelect
    //                 + '&colorWashNameSelect=' + this.searchFormModel.colorWashNameSelect
    //                 + '&ndcdateSelect=' + this.searchFormModel.ndcdateSelect
    //                 + '&vpoNltGacDateSelect=' + this.searchFormModel.vpoNltGacDateSelect
    //                 + '&vpoFtyGacDateSelect=' + this.searchFormModel.vpoFtyGacDateSelect
    //                 + '&ndcWkSelect=' + this.searchFormModel.ndcWkSelect
    //                 + '&vpoStatusSelect=' + this.searchFormModel.vpoStatusSelect
    //                 + '&vpoLineStatusSelect=' + this.searchFormModel.vpoLineStatusSelect
    //                 + '&categorySelect=' + this.searchFormModel.categorySelect
    //                 + '&cpoColorDescSelect=' + this.searchFormModel.cpoColorDescSelect
    //                 + '&loginUserId=' + localStorage.getItem('loginUserId')
    //                 + '&brUserId=' + localStorage.getItem('brUserId')
    //                 + '&roleName=' + localStorage.getItem('roleName')
    //                 + '&authentication=' + localStorage.getItem('authentication');

    //             this.api.getAllGriddataforExcel(localStorage.getItem('loginUserId'), this.source.url, this.availableviewid).subscribe(
    //                 res => {

    //                     let exportExcelData = new Array();
    //                     for (let i = 0; i < res.length; i++) {
    //                         exportExcelData.push(res[i]);
    //                     }
    //                     if (exportExcelData.length > 0) {
    //                         this.excelService.exportAsExcelFile(exportExcelData, 'psr');
    //                         this.toastr.successToastr("Downloaded Successfully", 'Success!');
    //                         this.progress.complete();
    //                     }
    //                     else {
    //                         this.toastr.warningToastr("Grid is empty, Cannot perform the Action", "Alert!", { dismiss: 'click', showCloseButton: true });
    //                         this.progress.complete();
    //                     }

    //                 }
    //             )
    //         } else {
    //             this.toastr.errorToastr('Please select the available view', 'Error!', { dismiss: 'click', showCloseButton: true });
    //         }
    //     }
    // }
    showHideBox() {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot perform the Action", "Alert!", { dismiss: 'click', showCloseButton: true });
            return this.showDialog = false;
        } else {
            this.flagCheck = true;
        }
    }

    acknowledgement(): void {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot perform the Action.", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            const psrOrderIds = new Array();
            let flag = false;
            for (let i = 0; i < this.psrGrid.getrows().length; i++) {
                const row = this.psrGrid.getrowdata(i);
                if (row.select !== undefined && row.select !== null && row.select !== '' && row.select === true) {
                    flag = true;
                    psrOrderIds.push(row.psrOrderId);
                }
            }
            if (!flag) {
                this.toastr.infoToastr('Please select row', 'Info!', { dismiss: 'click', showCloseButton: true });
                return;
            }
            this.progress.start();
            let t = this.api.updateAcknowledgement(psrOrderIds);
            t.subscribe(
                res => {
                    this.psrRes = res;
                    this.progress.complete();
                    if (this.psrRes.type === 'ERROR') {
                        this.toastr.errorToastr(this.psrRes.message, 'Error!', { dismiss: 'click', showCloseButton: true });
                    } else {
                        this.toastr.successToastr(this.psrRes.message, 'Success!');
                        this.onFormSubmit(1);
                    }
                }, error => {
                    this.toastr.errorToastr('error', 'Error!', { dismiss: 'click', showCloseButton: true });
                    this.progress.complete();
                }
            );
        };
    }
    fullWidthCall() {
        let x = document.getElementById("doFullWidth");
        x.click();
    }

    doFullWidth(): void {
        this.psrGrid.beginupdate();
        this.psrGrid.setcolumnproperty('psrOrderId', 'width', '8%');
        this.psrGrid.endupdate();
    }

    /* import excel functionality starts */

    callInput() {
        document.getElementById("inputFile").click();
    }
   

    selectedFiles: FileList;
    currentFile: File;
    // data:any;
    checkExcelError:any = [];
    selectFile(event) {
      
        this.selectedFiles = event.target.files;
        if (this.selectedFiles.length > 0) {
           // this.checkExcelError = true;
            this.readExcelData(event);
            this.fileName = this.selectedFiles.item(0).name;
            this.onUpload = false;
        }
        else {
            this.fileName = "";
        }
       
    }

    orderAvailableFields = new Array();
    mischieviousExcel:boolean;
    duplicatePsrOrderFound:boolean;
    readExcelData(event) {
        this.dataExcel = [];
        this.rowValues =  [];
        this.checkExcelError = [];
        this.orderAvailableFields = [];
        let wrongExcelUpload: Boolean = false;
        this.duplicatePsrOrderFound = false;
        this.mischieviousExcel = false;

        const target: DataTransfer = <DataTransfer>(event.target);
        //if (target.files.length !== 1) throw new Error('Cannot use multiple files');
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
          /* read workbook */
          const bstr: string = e.target.result;
          const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
    
          /* grab first sheet */
          let indexOfsheet:number;
          wb.SheetNames.forEach((value,index)=> {
              if(value.includes('PSR_')) {
                indexOfsheet = index;
              } 
              if(value.toString().toLocaleUpperCase() === 'VALIDATIONS') {
                wrongExcelUpload = true;
              }
          });
          
           if(wrongExcelUpload){
                const validationSheet:XLSX.WorkSheet = wb.Sheets['validations'];
                if(this.validateSheet(validationSheet)) {
                    this.mainSheetName = validationSheet['G4'].v;

                    const wsname: string = wb.SheetNames[indexOfsheet];
                    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
                    /* save data */
                    this.dataExcel = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
                    this.validateExcelData(this.dataExcel);
                }  else {
                    console.log("Either Enable Code False Or  Secret code Missed");
                    this.mischieviousExcel = true;
                }
           } else {
               console.log("Validation Sheet is not available");
               this.mischieviousExcel = true;
           }
        
           // this.checkExcelError = false;

        };
        reader.readAsBinaryString(target.files[0]);
        //return true;

    }

    validateSheet(sheet:any):boolean {

        // 1- check weather validation sheet is available or not
        // 2- check enable content is true or not
        // 3- check secret code is available or not


        //check enable content
        if(sheet['G3'].v == false) {
            console.log('Enable content is false');
            return false
        }
        
        // check secret code
        if(sheet['G2'].v != 'Mgf123!') {
            console.log('Secret code Missed');
            return false
        }
        // //validate Duplicate PsrOrderId Flag 
        // if(sheet['G5'].v === 'DUPLICATE') {
        //     console.log('Duplicate PSR Order ID Found');
        //     return false
        // }

        //validate valid sheet
        return true;
    }
    
   
    validateExcelData(data:any) {

       
        let order:any  =[];
        let events:any = [];
        let psrOrderId;
        let holdPsrId:any = [];
        for(let i = 1; i<data.length; i++) {
            order[i-1] = {};
            for(let j= 0; j<data[i].length; j++) {
    
                if(data[0][j] ==='Psr Order Id') {
                    psrOrderId = data[i][j]
                   
                    if(holdPsrId.indexOf(psrOrderId)>-1) {
                        console.log("Duplicate Psr OrderId Found");
                        this.duplicatePsrOrderFound = true;
                    } 
                    holdPsrId.push(psrOrderId);
                    order[i-1]["psrOrderId"] = psrOrderId;
                }
                for(let k = 0; k<this.columns.length; k++) {
                    if(this.columns[k].editable === true && this.columns[k].text === data[0][j]) {
                        for(let l = 0; l<this.datafields.length; l++) {
                            if(this.datafields[l].text===data[0][j]) {
                                this.validateDataType(this.datafields[l].type, this.datafields[l].length, data[i][j], this.datafields[l].text,i);
                                if(this.datafields[l].text === "Knitting %" || this.datafields[l].text === "Linking %") {
                                    order[i-1][this.datafields[l].name] = data[i][j]*100;
                                } else {
                                    order[i-1][this.datafields[l].name] = data[i][j];
                                }
                                
                            }
    
                        }
                    }
                }
    
                
                if(data[0][j].includes('-RevisedDate') || data[0][j].includes('-ActualDate')) {
                    //console.log(data[0][j]+"===="+ data[i][j]);
                    this.validateDataType('date','', data[i][j], data[0][j], i);
                }
    
                if(data[0][j].includes('-eventCode') && !!data[i][j]) {
                    events.push({
                        "psrOrderId": psrOrderId,
                        "eventCode": data[i][j],
                        "actualDateStr": !!data[i][j+3] == true?data[i][j+3]:null,
                        "revisedDateStr": !!data[i][j+2] == true?data[i][j+2]:null
                    })
                }
    
    
            }
    
        }
        //this.progress.start();
        this.rowValues.push({ "orders": order, "events": events });
        this.orderAvailableFields = Object.getOwnPropertyNames(order[0]);
        
    }

    validateDataType(type, length, value, columnname, rowNumber) {

        if(value !== '' && value !== undefined && value !== null) {
            let expRes:(string|number);
            if (type=='string') {
                if(value.length> length) {
                    expRes = "Type: Text, Length:"+ length;
                    this.checkExcelError.push({columnName:columnname,rownumber:rowNumber+1, result:value, expectedResult:expRes});
                }
               
            } else if(type === 'int' || type === 'BigDecimal') {
                expRes = "Type:"+ type + ", Length:"+ length;
                if(columnname === "Knitting %" || columnname === "Linking %") {
                    expRes = "Type: Percentage, Length: "+ length;
                }
                if(isNaN(value) || value.length>length) {
                    this.checkExcelError.push({columnName:columnname,rownumber:rowNumber+1, result:value, expectedResult:expRes});
                }
            } else if(type === 'bool') {
                expRes = "Type: Boolean, Accept: Yes/No";
                if(/\d/.test(value)) {
                    this.checkExcelError.push({columnName:columnname,rownumber:rowNumber+1, result:value, expectedResult:expRes});
                } else if(value.toUpperCase() != 'YES' && value.toUpperCase() != 'NO') {
                    this.checkExcelError.push({columnName:columnname,rownumber:rowNumber+1, result:value, expectedResult:expRes});
                } 
            } else {
                expRes = "Type: Date, Format: MM/DD/YYYY";
                if(!moment(value, 'MM/DD/YYYY',true).isValid()) {
                    if(!moment(value, 'M/DD/YYYY',true).isValid() && !moment(value, 'MM/D/YYYY',true).isValid() && !moment(value, 'M/D/YYYY',true).isValid()) {
                        this.checkExcelError.push({columnName:columnname,rownumber:rowNumber+1, result:value, expectedResult:expRes});
                    }
                    
                }
            }
        }
        //return true;
    }



    
    fileName: String = "";
    @ViewChild('fileInput')
    fileInput: ElementRef;
    onUpload: boolean = false;
    upload() {
      
        if(this.checkExcelError.length>0) {
            //console.log(this.checkExcelError);
            //this.toastr.errorToastr('Mistmatch Data, <a style="color:yellow" href="javascript:void(0)" onclick="showExcelValPop()"> Click Here</a> for more details.', "Error", { dismiss: 'click', showCloseButton: true, enableHTML: true });
            this.excelUivaliPopoup = true;
            this.showUploadPopup = false;
            this.fileName = "";
            this.fileInput.nativeElement.value = "";
            return;
        } else if( this.rowValues.length<=0 || this.mischieviousExcel == true) {
            this.toastr.errorToastr("Please contact admin, An error has been occured in the system.", "Error", { dismiss: 'click', showCloseButton: true });
            return;
        } else if(this.duplicatePsrOrderFound == true) {
            this.toastr.errorToastr("Duplicate PSR ORDER ID found, Please Export another excel file", "Error", { dismiss: 'click', showCloseButton: true });
            return;
        }
        //this.currentFile = this.selectedFiles.item(0);
        //console.log('  -------  ', this.currentFile);
        this.progress.start();
        this.onUpload = true;
        // this.api.uploadFile(this.currentFile).subscribe(
        this.api.updateOrdersByImportExcel(localStorage.getItem('loginUserId'), localStorage.getItem('roleName'), this.mainSheetName,this.orderAvailableFields.toString(), this.rowValues).subscribe(
            res => {
                this.psrRes = res;
                this.fileName = "";
                this.fileInput.nativeElement.value = "";
                console.log('File is completely uploaded!', res);
                if (this.psrRes.type === 'error') {
                    this.showUploadPopup = false;
                    if (this.psrRes.message != null) {
                        this.toastr.errorToastr(this.psrRes.message, "Error", { dismiss: 'click', showCloseButton: true });
                    }
                    if (this.psrRes.message == null) {
                        this.toastr.errorToastr("Please contact admin, an Arror has been occured in the system.", "Error", { dismiss: 'click', showCloseButton: true });
                    }
                    this.progress.complete();
                } else {
                    // refresh the grid with latest update
                    this.showUploadPopup = false;
                    this.onFormSubmit(1);
                    this.toastr.successToastr(this.psrRes.message, 'Success!');
                    this.progress.complete();
                }
            },
            error => {
                this.toastr.errorToastr("Please contact admin, an Arror has been occured in the system.", "Error", { dismiss: 'click', showCloseButton: true });
                this.progress.complete();
            });
    }



    callFileInput() {
        document.getElementById("fileInput").click();
    }

    fileUpload(event) {
        this.x = event.target.files;
        console.log(this.x);
        this.fileUploadModelInput = this.x.length + ' Files';
        // this.toastr.errorToastr('Error encountered. Click on Resolve error button to check', 'Error!', { dismiss: 'click', showCloseButton: true });
    }

    /* export excel with macros enabled starts here */
    headerFileName: string = "";
    downloadXlsm() {
        let holdCheckedOrderId:any = [];
        for (let i = 0; i < this.psrGrid.getrows().length; i++) {
            if(this.psrGrid.getcellvalue(i,'select')) {
                holdCheckedOrderId.push(this.psrGrid.getcellvalue(i,'psrOrderId'));
            }
        }

        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot perform the Action", "Alert!", { dismiss: 'click', showCloseButton: true });
        }
        else if (parseInt(localStorage.getItem('allItems')) > (parseInt(localStorage.getItem('gridRowsLimit'))) && holdCheckedOrderId.length ==0) {
            this.toastr.warningToastr("Number of records cannot be more than " + localStorage.getItem('gridRowsLimit') + ", Please refine your search.", "Alert!", { dismiss: 'click', showCloseButton: true });
        }
        else {
            if (this.publicviewdropdown.length > 0) {
                this.progress.start();
                this.source.url =
                    'vpo=' + this.searchFormModel.vpo
                    + '&productionOffice=' + this.searchFormModel.productionOffice
                    + '&vendorId=' + this.searchFormModel.vendorId
                    + '&ocCommit=' + this.searchFormModel.ocCommit
                    + '&factoryId=' + this.searchFormModel.factoryId
                    + '&factoryName=' + this.searchFormModel.factoryName
                    + '&countryOrigin=' + this.searchFormModel.countryOrigin
                    + '&brand=' + this.searchFormModel.brand
                    + '&style=' + this.searchFormModel.style
                    + '&styleDescription=' + this.searchFormModel.styleDescription
                    + '&cpo=' + this.searchFormModel.cpo
                    + '&dept=' + this.searchFormModel.dept
                    + '&season=' + this.searchFormModel.season
                    + '&colorWashName=' + this.searchFormModel.colorWashName
                    + '&ndcdate=' + this.searchFormModel.ndcdate
                    + '&vpoNltGacDate=' + this.searchFormModel.vpoNltGacDate
                    + '&vpoFtyGacDate=' + this.searchFormModel.vpoFtyGacDate
                    + '&ndcWk=' + this.searchFormModel.ndcWk
                    + '&vpoStatus=' + this.searchFormModel.vpoStatus
                    + '&vpoLineStatus=' + this.searchFormModel.vpoLineStatus
                    + '&category=' + this.searchFormModel.category
                    + '&cpoColorDesc=' + this.searchFormModel.cpoColorDesc
                    + '&cpoDept=' + this.searchFormModel.cpoDept
                    + '&initFlow=' + this.searchFormModel.initFlow
                    + '&vposelect=' + this.searchFormModel.vpoSelect
                    + '&vendorIdSelect=' + this.searchFormModel.vendorIdSelect
                    + '&ocCommitSelect=' + this.searchFormModel.ocCommitSelect
                    + '&factoryIdSelect=' + this.searchFormModel.factoryIdSelect
                    + '&factoryNameSelect=' + this.searchFormModel.factoryNameSelect
                    + '&brandSelect=' + this.searchFormModel.brandSelect
                    + '&styleSelect=' + this.searchFormModel.styleSelect
                    + '&styleDescSelect=' + this.searchFormModel.styleDescSelect
                    + '&prodOfficeSelect=' + this.searchFormModel.prodOfficeSelect
                    + '&countryOriginSelect=' + this.searchFormModel.countryOriginSelect
                    + '&milestoneselect=' + this.searchFormModel.milestoneselect
                    + '&cpoSelect=' + this.searchFormModel.cpoSelect
                    + '&deptSelect=' + this.searchFormModel.deptSelect
                    + '&seasonSelect=' + this.searchFormModel.seasonSelect
                    + '&colorWashNameSelect=' + this.searchFormModel.colorWashNameSelect
                    + '&ndcdateSelect=' + this.searchFormModel.ndcdateSelect
                    + '&vpoNltGacDateSelect=' + this.searchFormModel.vpoNltGacDateSelect
                    + '&vpoFtyGacDateSelect=' + this.searchFormModel.vpoFtyGacDateSelect
                    + '&ndcWkSelect=' + this.searchFormModel.ndcWkSelect
                    + '&vpoStatusSelect=' + this.searchFormModel.vpoStatusSelect
                    + '&vpoLineStatusSelect=' + this.searchFormModel.vpoLineStatusSelect
                    + '&categorySelect=' + this.searchFormModel.categorySelect
                    + '&cpoColorDescSelect=' + this.searchFormModel.cpoColorDescSelect
                    + '&cpoDeptSelect=' + this.searchFormModel.cpoDeptSelect
                    + '&initFlowSelect=' + this.searchFormModel.initFlowSelect
                    + '&loginUserId=' + localStorage.getItem('loginUserId')
                    + '&brUserId=' + localStorage.getItem('brUserId')
                    + '&roleName=' + localStorage.getItem('roleName')
                    + '&authentication=' + localStorage.getItem('authentication');

                   


                    if(holdCheckedOrderId.length>0) {
                        holdCheckedOrderId = holdCheckedOrderId.toString();
                    } else {
                        holdCheckedOrderId = '';
                    }

                this.api.getAllGriddataforExcel(localStorage.getItem('loginUserId'), this.source.url, this.availableviewid,localStorage.getItem('milestone'), holdCheckedOrderId).subscribe(
                    
                    res => {
                        console.log(' jtsw ------ > ', res);
                        console.log(res.headers.get('content-disposition'));
                        var contentDisposition = res.headers.get('content-disposition');
                        this.headerFileName = contentDisposition.split(';')[1].split('filename')[1].split('=')[1].trim() + EXCEL_EXTENSION;
                        console.log(this.headerFileName);
                        if (res.body.size > 0) {
                            this.excelService.saveAsExcelFile(res.body, this.headerFileName);
                            this.toastr.successToastr("Download Successfully", "Success");
                            this.progress.complete();
                        } else {
                            this.toastr.errorToastr("Unable to download file, No data exists based on search!", "Error!");
                            this.progress.complete();
                        }
                    }, error => {
                        console.error(' An error has been occured ', error);
                        this.progress.complete();
                        if (error.status == 417) {
                            this.toastr.warningToastr("Number of records cannot be more than " + localStorage.getItem('gridRowsLimit') + ", Please refine your search.", "Alert!", { dismiss: 'click', showCloseButton: true });
                        }
                        else if (error.status == 103) {
                            this.toastr.warningToastr("The process cannot access the file because it is being used by another process. Please try again!", "Alert!", { dismiss: 'click', showCloseButton: true });
                        }
                        else {
                            this.toastr.errorToastr("Something went wrong. Please try again!", "Error!");
                        }

                    }
                )
            } else {
                this.toastr.errorToastr('Please select the available view', 'Error!', { dismiss: 'click', showCloseButton: true });
            }
        }
    }
    /* export excel with macros enabled ends here */

    fileTrack(value) {

        let t :any;
            if(value=='fileTrack'){
                this.fromDate="";
                this.toDate="";
                
                t = this.api.getFileTrackList( 'All', this.fromDate
                , this.toDate);
            }
            else{
                // console.log(this.since[0].text);
                // console.log(this.searchKeyword);
                // console.log(this.fromDate);
                if(this.since[0].text=="Custom"){
                    if(this.fromDate == null || this.fromDate=="" || this.toDate==null || this.toDate ==""){
                        alert("Date fields are mandatory for Custom filter");
                        return;
                    }
                    if(this.fromDate.formatted!=undefined){
                        this.fromDate= this.fromDate.formatted;
                    }
                    if(this.toDate.formatted!=undefined){
                        this.toDate = this.toDate.formatted;
                    }
                    
                    // console.log(this.fromDate);
                    // console.log(this.toDate);
                }
                else{
                    this.fromDate="";
                    this.toDate="";
                }
                
               t = this.api.getFileTrackList(this.since[0].text, this.fromDate , this.toDate);
            }
        this.progress.start();
        t.subscribe(
            (response) => {
                console.log("response data ", response);
                this.userId = localStorage.getItem('loginUserId');
                this.fileChangeTrackList =[];
                this.fileChangeTrackFinalList=[];
                this.sum=20;
                this.fileChangeTrackList =Object.keys(response).map((key) => [key, response[key]]);
                console.log(this.fileChangeTrackList)
                this.appendItems(0, this.sum);
                this.progress.complete();
                this.showFileTrackPopup = true;
            },
            error => {
                console.log("error------>", error);
            })
    }
    showUploadPopup: boolean = false;
    showFileUpload() {
        this.showUploadPopup = true;
    }
	
    saveSearch() {
        if (this.psrGrid.getrows().length === 0) {
            this.toastr.warningToastr("Grid is empty, Cannot save search", "Alert!", { dismiss: 'click', showCloseButton: true });
        } else {
            let result = this.api.saveSearchData(this.searchFormModel, localStorage.getItem('loginUserId'));
            result.subscribe(res => {
                this.psrRes = res;
                if (this.psrRes.type === 'ERROR') {
                    this.toastr.warningToastr("Cannot save search", "Alert!", { dismiss: 'click', showCloseButton: true });
                } else {
                    this.toastr.successToastr("Saved Successfully", "Success");
                }
            });
        }
    }

    loadSearch() {
        this.api.savedSearchData(localStorage.getItem('loginUserId')).subscribe(
            (response) => {
                console.log("response data ", response);
                var savedSearch = response;
                this.searchFormModel.vpo = savedSearch.vpo;
                this.searchFormModel.productionOffice = savedSearch.productionOffice;
                this.searchFormModel.vendorId = savedSearch.vendorId;
                this.searchFormModel.ocCommit = savedSearch.ocCommit;
                this.searchFormModel.factoryId = savedSearch.factoryId;
                this.searchFormModel.factoryName = savedSearch.factoryName;
                this.searchFormModel.countryOrigin = savedSearch.countryOrigin;
                this.searchFormModel.brand = savedSearch.brand;
                this.searchFormModel.style = savedSearch.style;
                this.searchFormModel.styleDescription = savedSearch.styleDescription;
                this.searchFormModel.cpo = savedSearch.cpo;
                this.searchFormModel.dept = savedSearch.dept;
                this.searchFormModel.season = savedSearch.season;
                this.searchFormModel.colorWashName = savedSearch.colorWashName;
                //CHG0016311 Rupan Start
                if (savedSearch.ndcdateSelect == 'Between') {
                    this.searchFormModel.ndcdate = savedSearch.ndcdate;
                    var splits = savedSearch.ndcdate.split(" AND ")
                    this.ndcDateValue = [new Date(splits[0]), new Date(splits[1])];
                } else {
                    this.searchFormModel.ndcdate = savedSearch.ndcdate;
                    this.cndcdate = {formatted: savedSearch.ndcdate};
                }
                if (savedSearch.vpoNltGacDateSelect == 'Between') {
                    this.searchFormModel.vpoNltGacDate = savedSearch.vpoNltGacDate;
                    var splits = savedSearch.vpoNltGacDate.split(" AND ")
                    this.vpoNltGacDateValue = [new Date(splits[0]), new Date(splits[1])];
                } else {
                    this.searchFormModel.vpoNltGacDate = savedSearch.vpoNltGacDate;
                    this.cvpoNltGacDate = {formatted: savedSearch.vpoNltGacDate};
                }
                if (savedSearch.vpoFtyGacDateSelect == 'Between') {
                    this.searchFormModel.vpoFtyGacDate = savedSearch.vpoFtyGacDate;
                    var splits = savedSearch.vpoFtyGacDate.split(" AND ")
                    this.vpoFtyGacDateValue = [new Date(splits[0]), new Date(splits[1])];
                } else {
                    this.searchFormModel.vpoFtyGacDate = savedSearch.vpoFtyGacDate;
                    this.cvpoFtyGacDate = {formatted: savedSearch.vpoFtyGacDate};
                }
                //CHG0016311 Rupan End
				this.searchFormModel.ndcWk = savedSearch.ndcWk;
				this.searchFormModel.vpoStatus = savedSearch.vpoStatus;
				this.searchFormModel.vpoLineStatus = savedSearch.vpoLineStatus;
				this.searchFormModel.category = savedSearch.category;
				this.searchFormModel.cpoColorDesc = savedSearch.cpoColorDesc;
				this.searchFormModel.cpoDept = savedSearch.cpoDept;
				this.searchFormModel.initFlow = savedSearch.initFlow;
				this.searchFormModel.vpoSelect = savedSearch.vpoSelect;
				this.searchFormModel.vendorIdSelect = savedSearch.vendorIdSelect;
				this.searchFormModel.ocCommitSelect = savedSearch.ocCommitSelect;
				this.searchFormModel.factoryIdSelect = savedSearch.factoryIdSelect;
				this.searchFormModel.factoryNameSelect = savedSearch.factoryNameSelect;
				this.searchFormModel.brandSelect = savedSearch.brandSelect;
				this.searchFormModel.styleSelect = savedSearch.styleSelect;
				this.searchFormModel.styleDescSelect = savedSearch.styleDescSelect;
				this.searchFormModel.prodOfficeSelect = savedSearch.prodOfficeSelect;
				this.searchFormModel.countryOriginSelect = savedSearch.countryOriginSelect;
				this.searchFormModel.milestoneselect = savedSearch.milestoneselect;
				this.searchFormModel.cpoSelect = savedSearch.cpoSelect;
				this.searchFormModel.deptSelect = savedSearch.deptSelect;
				this.searchFormModel.seasonSelect = savedSearch.seasonSelect;
				this.searchFormModel.colorWashNameSelect = savedSearch.colorWashNameSelect;
				this.searchFormModel.ndcdateSelect = savedSearch.ndcdateSelect;
				this.searchFormModel.vpoNltGacDateSelect = savedSearch.vpoNltGacDateSelect;
				this.searchFormModel.vpoFtyGacDateSelect = savedSearch.vpoFtyGacDateSelect;
				this.searchFormModel.ndcWkSelect = savedSearch.ndcWkSelect;
				this.searchFormModel.vpoStatusSelect = savedSearch.vpoStatusSelect;
				this.searchFormModel.vpoLineStatusSelect = savedSearch.vpoLineStatusSelect;
				this.searchFormModel.categorySelect = savedSearch.categorySelect;
				this.searchFormModel.cpoColorDescSelect = savedSearch.cpoColorDescSelect;
				this.searchFormModel.cpoDeptSelect = savedSearch.cpoDeptSelect;
				this.searchFormModel.initFlowSelect = savedSearch.initFlowSelect;
            },
            error => {
                console.log("error------>", error);
            })
    }

    //holdReorderColumns:any;

    getReorder():void {


        let holdReorderColumns = [];
        let holdListBoxItems = this.psrCheckBox.getItems();
        for (let index = 0; index < holdListBoxItems.length; index++) {
            holdReorderColumns.push(holdListBoxItems[index].label);
        }

        let getColorder:any = holdReorderColumns;


        let resCOl = [{ text: ' ', datafield: 'select', align: "center", columntype: 'checkbox', width: '50px',pinned:true, editoptions: { value: "1:0" }, formatoptions: { disabled: false } }];
         for(let i=0; i<getColorder.length; i++) {
             this.temporaryColumn.find((o, t) => {
                 if (o.text === getColorder[i]) {
                     resCOl.push(this.temporaryColumn[t]);
                     return true; // stop searching
                 } else if(getColorder[i]+'PlannedDate' === o.datafield) {
                    resCOl.push(this.temporaryColumn[t]);
                    resCOl.push(this.temporaryColumn[t+1]);
                    resCOl.push(this.temporaryColumn[t+2]);
                    return true; // stop searchi
                 } 
             });
         }
         
         for (let index = 0; index < this.temporaryColumn.length; index++) {
                let kpt:any = -1;
                for (let kmt = 0; kmt < resCOl.length; kmt++) {
                    if(this.temporaryColumn[index].text == resCOl[kmt].text) {
                        kpt = index;
                        break;
                    }    
                }

                if(kpt ==-1 && this.temporaryColumn[index].datafield !='select') {
                    resCOl.push(this.temporaryColumn[index]);
                }
         }

         this.columns = resCOl;

     }

     changeListStatus(value) {


       if(value == 'All') {
            this.listBoxSource = this.holdUncheckedColumns.concat(this.holdCheckedColumns);
        } else if(value == 'Visible') {
            this.listBoxSource = this.holdUncheckedColumns;
        } else {
            this.listBoxSource = this.holdCheckedColumns;
        }

        
     }

     reArrangeColumn(value) {

          this.api.getOrderColumn(value).subscribe(
                res=>{
                    
                   let result = res;

                   if((Object.keys(result).length === 0)) {   
                    this.listBoxSource  = this.holdlistOriginal;
                    //this.holdUncheckedColumns 
                    return;
                   }
                   let x = [];
                   let y=[];

                
                  for (let index = 0; index < result.length; index++) {

                    this.temporaryColumn.filter((resposeR)=>{

                            if(resposeR.columngroup ==undefined && resposeR.text == result[index] && y.indexOf(resposeR.text)==-1) {
                                x.push({ label: resposeR.text, value: resposeR.datafield }); 
                                y.push(resposeR.text)
                            } else if(resposeR.columngroup == result[index] && y.indexOf(resposeR.columngroup)==-1) {
                                x.push({ label: resposeR.columngroup, value: resposeR.columngroup, milestone: true });
                                y.push(resposeR.columngroup)
                            }

                    })
                  }
                  this.listBoxSource = x;
                }

            )
     }

     dragEnd(item) {

        if(this.status == 'All') {
            this.holdCheckedColumns = [];
            this.holdUncheckedColumns = [];    
        } else if(this.status == 'Visible') {
            this.holdUncheckedColumns = [];
        } else {
            this.holdCheckedColumns = [];
        }

        let holdListBoxItems = this.psrCheckBox.getItems();


        for (let index = 0; index < holdListBoxItems.length; index++) {

            if(holdListBoxItems[index].checked == true) {
                    this.holdCheckedColumns.push(holdListBoxItems[index]);
            } else {
                    this.holdUncheckedColumns.push(holdListBoxItems[index]);
            }
        }

     }

     arrangeUpdatenSaveView() {

         let availableColumn = [];
         let hiddenColumn = [];

         this.holdUncheckedColumns.filter((res)=>availableColumn.push(res.label));
         this.holdCheckedColumns.filter((res)=>hiddenColumn.push(res.label));

         let orderColumn:any = availableColumn.concat(hiddenColumn);
         return orderColumn;
         
     }

     holdCheckednUnchecked() {

        this.holdCheckedColumns = [];
        this.holdUncheckedColumns = [];
        let holdListBoxItems = this.psrCheckBox.getItems();

        for (let index = 0; index < holdListBoxItems.length; index++) {

            if(holdListBoxItems[index].checked == true) {
                    this.holdCheckedColumns.push(holdListBoxItems[index]);
            } else {
                    this.holdUncheckedColumns.push(holdListBoxItems[index]);
            }
        }


        this.secondScrollList = this.holdUncheckedColumns.concat(this.holdCheckedColumns);

     }







    allowCheckStatus:boolean = false;
    getGridMainCheckStatus():void {
  
      this.psrGrid.beginupdate();
      for (let i = 0; i < this.psrGrid.getrows().length; i++) {
       this.psrGrid.setcellvalue(i,'select',this.allowCheckStatus);
      }
      this.psrGrid.endupdate();
  
    }
    
}

