import { Injectable,  Output, EventEmitter } from '@angular/core';
import { StaticpsrComponent } from './staticpsr.component';
import {ApiService} from '../../api.service';
import {SearchService} from '../search/search.service';
@Injectable({
  providedIn: 'root'
})
export class PsrService {
  // @Output() change: EventEmitter<void> = new EventEmitter();
  staticpsrComp = new StaticpsrComponent();
  constructor() { }
  changeWidth() {
    this.staticpsrComp.changeWidth3();
  }
  
}
