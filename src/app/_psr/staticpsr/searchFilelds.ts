
export class searchFields  {

    modelOperators = [
        { value: 'Like', label: 'Like' },
        { value: 'Not like', label: 'Not like' },
        { value: 'Equal to', label: 'Equal to' },
        { value: 'Not equal to', label: 'Not equal to' },
        { value: 'In the list', label: 'In the list' },
        { value: 'Not in the list', label: 'Not in the list' },
        { value: 'Starts with', label: 'Starts with' },
        { value: 'Ends with', label: 'Ends with' },
        { value: 'Equal to null', label: 'Equal to null' },
        { value: 'Is not null', label: 'Is not null' },
    ];
    modelOperatorsfordate = [
        { value: 'Greater', label: 'Greater' },
        { value: 'Equal to', label: 'Equal to' },
        { value: 'Less', label: 'Less' },
        { value: 'Not equal to', label: 'Not equal to' },
        { value: 'In the list', label: 'In the list' },
        { value: 'Not in the list', label: 'Not in the list' },
        { value: 'Equal to null', label: 'Equal to null' },
        { value: 'Is not null', label: 'Is not null' },
        //CHG0016311 Rupan Start
        { value: 'Between', label: 'Between' },
        //CHG0016311 Rupan End
    ];
    modelOperatorsforstatus = [
        { value: 'Not in the list', label: 'Not in the list' },
        { value: 'Like', label: 'Like' },
        { value: 'Not like', label: 'Not like' },
        { value: 'Equal to', label: 'Equal to' },
        { value: 'Not equal to', label: 'Not equal to' },
        { value: 'In the list', label: 'In the list' },
        { value: 'Starts with', label: 'Starts with' },
        { value: 'Ends with', label: 'Ends with' },
        { value: 'Equal to null', label: 'Equal to null' },
        { value: 'Is not null', label: 'Is not null' },
    ];
    
   
    

   

}