import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  apiUrl: string;
  imagePath: string;
  excelPath: string;
  excelDownloadPath: string;
  psrui: string;
  login: string;
  baseUrl: string;
  constructor() { }
}